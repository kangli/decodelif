#ifndef MASS_HELPER_H
#define MASS_HELPER_H

#include <set>
#include <random>
#include <tuple>
#include <algorithm>

/*
 * Helper functions.
 */

namespace mass {


/**
 * \brief Struct to store rescaled spike train.
 *
 * Spike train is rescaled using the time rescaling theorem.
 */
struct RescaledSpikeTrain {
    double start;               ///< rescaled starting time.
    double end;                 ///< rescaled ending time.
    std::vector<double> st;     ///< spike train.
};

/**
 * \brief Combine a vector of RescaledSpikeTrain.
 */
RescaledSpikeTrain combine_rsts( const std::vector<RescaledSpikeTrain>& rsts);

/**
 * \brief Contatenate two vectors
 */
template <class T>
std::vector<T> cont_two_vectors( const std::vector<T>& v1, 
        const std::vector<T>& v2 ) {
    std::vector<T> r = v1;
    for (const T& v : v2) {
        r.push_back(v);
    }
    return r;
}


/**
 * \brief Convert vector to set
 */
template <class T>
std::set<T> get_set_from_vector(const std::vector<T>& v) {
    std::set<T> s;
    for (auto vv : v) s.insert(vv);
    return s;
}

/**
 * \brief Get minimum value.
 */
template <class T>
T get_min(const std::vector<T>& v) {
    T min = v[0];
    for (auto& i : v) {
        if (i < min) {
            min = i;
        }
    }
    return min;
}

/**
 * \brief Get maximum value.
 */
template <class T>
T get_max(const std::vector<T>& v) {
    T max = v[0];
    for (auto& i : v) {
        if (i > max) {
            max = i;
        }
    }
    return max;
}

/**
 * \brief Get maximum index.
 */
template <class T>
int get_max_index(const std::vector<T>& v) {
    T max = v[0];
    int index = 0;
    for (size_t i = 0; i < v.size(); ++i) {
        if (v[i] > max) {
            max = v[i];
            index = i;
        }
    }
    return index;
}

/**
 * takes degree as input
 */
template <class T>
double calc_circ_gauss(T input, double g1, double g2, double g3) {
    double x = input * M_PI / 180.0;
    if (x > g2 + M_PI) {
        x = 2 * g2 + M_PI*2 - x;
    }
    return g1 * exp(-(x-g2)*(x-g2)/2/g3/g3);
}

template <class T>
double dnorm(T x, T mu, T sigma) {
    return 1.0/sqrt(2*M_PI)/sigma * exp(-(mu-x)*(mu-x)/sigma/sigma/2.0);
}

template <class T>
double pnorm(T x, T mu, T sigma)  {
    // constants
    double a1 =  0.254829592;
    double a2 = -0.284496736;
    double a3 =  1.421413741;
    double a4 = -1.453152027;
    double a5 =  1.061405429;
    double p  =  0.3275911;

    x = (x - mu) / sigma;

    // Save the sign of x
    int sign = 1;
    if (x < 0)
        sign = -1;
    x = fabs(x)/sqrt(2.0);

    // A&S formula 7.1.26
    double t = 1.0/(1.0 + p*x);
    double y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

    return 0.5*(1.0 + sign*y);
}

// prepare data for cross validation
template <class S, class T>
void prepare_cross_validation(
        int k, const std::vector<S>& stims, const std::vector<T>& ss,
        std::vector< std::vector<S> >& train_stims,
        std::vector< std::vector<T> >& train_ss,
        std::vector< std::vector<S> >& test_stims, 
        std::vector< std::vector<T> >& test_ss ) {
    std::vector<size_t> index;
    for (size_t i = 0; i < stims.size(); ++i) index.push_back(i);
    std::shuffle(std::begin(index), std::end(index), 
                 std::default_random_engine());

    int num = stims.size() / k;

    for (int i = 0; i < k; ++i) {
        auto itend = index.begin() + (i*num+num);
        if (i == k-1) {
            itend = index.end();
        }
        std::vector<size_t> testid(index.begin() + i*num, itend);
        std::vector<size_t> trainid = index;
        auto itend2 = trainid.begin() + (i*num+num);
        if (i == k-1) {
            itend2 = trainid.end();
        }
        trainid.erase(trainid.begin() + i*num, itend2);
        std::vector<S> stim_train, stim_test;
        std::vector<T> ss_train, ss_test;
        for (auto i : testid) {
            ss_test.push_back(ss[i]);
            stim_test.push_back(stims[i]);
        }
        for (auto i : trainid) {
            ss_train.push_back(ss[i]);
            stim_train.push_back(stims[i]);
        }
        train_stims.push_back(stim_train);
        train_ss.push_back(ss_train);
        test_stims.push_back(stim_test);
        test_ss.push_back(ss_test);
    }
}

// get distance
double calc_dist(const std::vector<double>& v1, const std::vector<double>& v2);

double calc_dist(double v1, double v2);

// get mean
std::vector<double> calc_mean(const std::vector<std::vector<double>>& v);

// get median
template <class T>
T calc_median(std::vector<T>& v) {
    std::sort(v.begin(), v.end());
    int size = v.size();
    T r;
    if (size % 2 == 0) {
        r = v[size/2 - 1] + v[size/2];
        r *= 0.5;
    } else {
        r = v[size/2];
    }
    return r;
}

template <class T>
T log_sum_exp(const std::vector<T>& logs) {
    T log_max = logs[0];
    for (auto& i:logs) {
        if (i > log_max) log_max = i;
    }
    T sum = 0;
    for (auto& i:logs) {
        sum += exp(i - log_max);
    }
    return log_max + log(sum);
}

// calc mean 
template <class T>
T calc_mean(const std::vector<T>& v) {
    T result = 0;
    for (auto i : v) {
        if (std::isfinite(i))
                result += i;
    }
    return result / v.size();
}

// get standard deviation
template <class T>
T calc_sd(const std::vector<T>& v) {
    double mean = calc_mean(v);
    double result = 0;
    for (auto i : v) {
        double diff = i - mean;
        result += diff*diff;
    }
    result /= v.size();
    return sqrt(result);
}


// k means cluster
template <class T>
void kmeans(const std::vector<T>& d, int k, 
            std::vector<std::vector<size_t>>& c) {
    if (k == 1) {
        c.push_back( {} );
        for (size_t i = 0; i < d.size(); ++i) {
            c[0].push_back(i);
        }
        return;
    }
    std::default_random_engine gen;
    std::uniform_int_distribution<int> distri(0,d.size()-1);
    double var_min = HUGE_VAL;
    std::vector<std::vector<size_t>> c_min;
    for (size_t count = 0; count < 50 * d.size(); ++count) {
        // initialize the means
        std::vector<T> m;
        for (int i = 0; i < k; ++i) {
            m.push_back(d[distri(gen)]);
        }
        double dis = 1000;

        while (dis > 0.001) {
            c.clear();
            c.resize(k);
            // assign class
            for (size_t i = 0; i < d.size(); ++i) {
                T dd = d[i];
                int index_min = 0;
                double v_min = calc_dist(dd, m[0]);
                for (int j = 1; j < k; ++j) {
                    double v_new = calc_dist(dd, m[j]);
                    if (v_new < v_min) {
                        v_min = v_new;
                        index_min = j;
                    }
                }
                c[index_min].push_back(i);
            }

            // new mean
            std::vector<T> nm;
            for (int j = 0; j < k; ++j) {
                std::vector<T> tmp;
                for (size_t cc : c[j]) {
                    tmp.push_back(d[cc]);
                }
                if (tmp.size() == 0) {
                    nm.push_back(m[j]);
                } else {
                    nm.push_back(calc_mean(tmp));
                }
            }
            // distance
            dis = 0;
            for (int j = 0; j < k; ++j) {
                dis += calc_dist(nm[j], m[j]);
            }
            m = nm;
        }
        //variance
        double var = 0;
        for (int j = 0; j < k; ++j) {
            for (size_t cc : c[j]) {
                double tmp = calc_dist(d[cc], m[j]);
                var += tmp * tmp;
            }
        }
        if (var < var_min) {
            var_min = var;
            c_min = c;
        }
    }
    c = c_min;
}

template <class T>
T calc_medoid(const std::vector<T>& d) {
    double min=1e100, sum;
    T min_medoid = d.size() > 0 ? d[0] : 1e-10;
    for (auto& dd : d) {
        sum = 0;
        for (auto& di : d) {
            sum += sqrt(calc_dist(dd, di));
        }
        if (sum < min) {
            min = sum;
            min_medoid = dd;
        }
    }
    return min_medoid;
}

// k medoids cluster
template <class T>
void kmedoids(const std::vector<T>& d, int k, 
            std::vector<std::vector<size_t>>& c) {
    if (k == 1) {
        c.push_back( {} );
        for (size_t i = 0; i < d.size(); ++i) {
            c[0].push_back(i);
        }
        return;
    }
    std::default_random_engine gen;
    std::uniform_int_distribution<int> distri(0,d.size()-1);
    double var_min = HUGE_VAL;
    std::vector<std::vector<size_t>> c_min;
    for (size_t count = 0; count < 10 * d.size(); ++count) {
        // initialize the medoids
        std::vector<T> m;
        for (int i = 0; i < k; ++i) {
            m.push_back(d[distri(gen)]);
        }
        double dis = 1000;

        while (dis > 0.001) {
            c.clear();
            c.resize(k);
            // assign class
            for (size_t i = 0; i < d.size(); ++i) {
                T dd = d[i];
                int index_min = 0;
                double v_min = sqrt(calc_dist(dd, m[0]));
                for (int j = 1; j < k; ++j) {
                    double v_new = sqrt(calc_dist(dd, m[j]));
                    if (v_new < v_min) {
                        v_min = v_new;
                        index_min = j;
                    }
                }
                c[index_min].push_back(i);
            }

            // new medoids
            std::vector<T> nm;
            for (int j = 0; j < k; ++j) {
                std::vector<T> tmp;
                for (size_t cc : c[j]) {
                    tmp.push_back(d[cc]);
                }
                nm.push_back(calc_medoid(tmp));
            }
            // distance
            dis = 0;
            for (int j = 0; j < k; ++j) {
                dis += sqrt(calc_dist(nm[j], m[j]));
            }
            m = nm;
        }
        //dissimilarity
        double var = 0;
        for (int j = 0; j < k; ++j) {
            for (size_t cc : c[j]) {
                double tmp = sqrt(calc_dist(d[cc], m[j]));
                var += tmp;
            }
        }
        if (var < var_min) {
            var_min = var;
            c_min = c;
        }
    }
    c = c_min;
}


/**
 * \brief Calculate distance between two time series using dynamic time warping.
 */
template <class T>
double calc_dynamic_time_warping_dist(
        const std::vector<T>& obs, 
        const std::vector<T>& obj, double q) {
    int n = obs.size() + 1;
    int m = obj.size() + 1;
    std::vector<std::vector<double>> mat(n, std::vector<double>(m));
    for (int i = 0; i < m; ++i) {
        mat[0][i] = i;
    }
    for (int i = 0; i < n; ++i) {
        mat[i][0] = i;
    }
    for (int i = 1; i < n; ++i) {
        for (int j = 1; j < m; ++j) {
            std::vector<double> vals = {
                mat[i-1][j-1] + q * fabs(obs[i-1] - obj[j-1]),
                mat[i-1][j] + 1,
                mat[i][j-1] + 1 };
            mat[i][j] = get_min( vals );
        }
    }
    return mat[n-1][m-1];
}


#define SQARE_2PI 2.506628
/**
 * \brief Standard normal pdf.
 */
template <class T>
double standard_normal_pdf(T x) {
    return exp(-x*x/2) / SQARE_2PI;
}

/**
 * \brief Calculate kernel density.
 */
template <class T>
double calc_kernel_density(double x, double h, const std::vector<T>& v) {
    double result = 0;
    for (auto i : v) {
        result += standard_normal_pdf( (x-i) / h );
    }
    return result / h / v.size();
}

/**
 * \brief Calculate bandwidth using Silverman's rule of thumb.
 */
template <class T>
T calc_bandwidth_silverman(const std::vector<T>& v) {
    double r = (v[0]+1) / 10;
    if (v.size() > 1) {
        r = 1.06 * calc_sd(v) * pow(v.size(), -0.2);
    }
    if (r == 0) r = (v[0]+1) / 10;
    return r;
}

template <class T>
void calc_kernel_density_function (double min, double max, int n,
        int cut, std::vector<T>& xs, std::vector<T>& density,
        const std::vector<T>& v) {
    double bw = calc_bandwidth_silverman<T>(v);
    xs.clear();
    density.clear();
    double from = min - cut * bw;
    double to = max + cut * bw;
    double dx = (to - from) / n;
    for (int i = 0; i <= n; ++i) {
        double x = from + i * dx;
        xs.push_back(x);
        density.push_back( calc_kernel_density(x, bw, v) );
    }
}

/**
 * \brief Calculate L-K divergence.
 */
template <class T>
double calc_KL_divergence(const std::vector<T>& v1,
                          const std::vector<T>& v2, double dx) {
    double result = 0;
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (size_t i = 0; i < v1.size(); ++i) {
        double val1 = v1[i];
        double val2 = v2[i];
        if (val1 <= 1e-100) {
            val1 = 1e-100;
        }
        if (val2 <= 1e-100) {
            val2 = 1e-100;
        }
        double tmp = val1 * log(val1/val2);
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
        result += tmp;
    }
    return result * dx;
}

/**
 * \brief Sort vector of pairs.
 */
template <class F, class S>
void sort_vector_of_pairs(std::vector<std::pair<F,S>>& v) {
    struct {
        bool operator()(std::pair<F,S> a, std::pair<F,S> b) {   
            if (!std::isfinite(a.first)) {
                return false;
            } else if (!std::isfinite(b.first)) {
                return true;
            } else {
                return a.first < b.first;
            }
        }   
    } ComparePairs;
    std::sort(v.begin(), v.end(), ComparePairs);
}


/**
 * \brief Check if two real values are euqal.
 */
template <class T>
bool check_equal(T v1, T v2) {
    if (fabs( v1-v2 ) < 1e-7
     || fabs( (v1-v2) / v1 ) < 1e-7) {
        return true;
    } else {
        return false;
    }
}


/**
 * \brief Multi-nomial distribution 
 * \param p vector containing all probabilities except the last one.
 */
int sample_multinomial(const std::vector<double>& p);

template<class T>
std::tuple<double, double> calc_mean_sd_posterior(const std::vector<T>& v, const std::vector<T>& w) {
    double mean = 0;
    size_t sz = v.size();
    std::vector<double> wn = w;
    double wsum = 0;
    for (size_t i = 0; i < sz; ++i) {
        wsum += w[i];
    }
    for (size_t i = 0; i < sz; ++i) {
        wn[i] /= wsum;
    }
    for (size_t i = 0; i < sz; ++i) {
        mean += v[i] * wn[i];
    }
    double sd = 0;
    for (size_t i = 0; i < sz; ++i) {
        double tmp = v[i] - mean;
        sd += wn[i] * tmp * tmp;
    }
    return std::make_tuple(mean, sqrt(sd));
}

template<class T>
std::vector<std::tuple<double, double>> calc_mean_sd_posterior(const std::vector<T>& v, const std::vector<T>& w, int num_stimuli) {
    std::vector<std::tuple<double, double>> res;
    double mean = 0;
    size_t sz = w.size();
    std::vector<double> wn = w;
    double wsum = 0;
    for (size_t i = 0; i < sz; ++i) {
        wsum += w[i];
    }
    for (size_t i = 0; i < sz; ++i) {
        wn[i] /= wsum;
    }
    for (int j = 0; j < num_stimuli; ++j) {
        for (size_t i = 0; i < sz; ++i) {
            int id = i * num_stimuli + j;
            mean += v[id] * wn[i];
        }
        double sd = 0;
        for (size_t i = 0; i < sz; ++i) {
            int id = i * num_stimuli + j;
            double tmp = v[id] - mean;
            sd += wn[i] * tmp * tmp;
        }
        res.push_back(std::make_tuple(mean, sqrt(sd)));
    }
    return res;
}

template<class T>
std::tuple<double, double> calc_mean_sd_posterior_with_flag(const std::vector<T>& v, const std::vector<T>& w, const std::vector<int>& c, int cf) {
    size_t sz = v.size();
    std::vector<double> wn, vn;
    for (size_t i = 0; i < sz; ++i) {
        if (c[i] == cf) {
            wn.push_back(w[i]);
            vn.push_back(v[i]);
        }
    }
    return calc_mean_sd_posterior<double>(vn, wn);
}


} // namespace mass

#endif // HELPER_H
