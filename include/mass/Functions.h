#ifndef MASS_FUNCTIONS_H
#define MASS_FUNCTIONS_H

#include <mass/Helper.h>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/distributions/inverse_gaussian.hpp>

#include <vector>
#include <unordered_map>
#include <iostream>
#include <cmath>
#include <cstring>
#include <memory>
#include <omp.h>


namespace mass {

/**
 * \brief Enum of all types of functions.
 *
 * All types of functions are included.
 * - RK: ResponseKernel
 * - SK: StimulusKernel
 * - PK: PerceptionKernel
 * - ISI: Interspike Interval (ISI) distribution
 */
enum FuncType {
    RK_NON,             ///< no response kernel
    RK_UNB,             ///< unbiased response kernel
    RK_EXP,             ///< exponential response kernel
    RK_LGA,             ///< log gamma response kernel

    SK_EMP,             ///< empty stimulus kernel
    SK_PW,              ///< piece-wise constant stimulus kernel
    SK_INPUT,           ///< input stimulus kernel
    SK_SMP,             ///< simple stimulus kernel
    SK_SIN,             ///< sinusoidal stimulus kernel

    PK_GAUSS,           ///< gaussian tuning curve, perception kernel
    PK_GAUSS_INT_MAP,   ///< gaussian tuning curve, perception kernel

    ISI_EXP,            ///< exponential ISI distribution
    ISI_GAM,            ///< Gamma ISI distribution
    ISI_IG,             ///< Inverse Gaussian ISI distribution

    DIST_DTW_ST,        ///< dynamic time warping on spike times
    DIST_DTW_ISI,       ///< dynamic time warping on ISIs
    DIST_ISI_DENS,      ///< dynamic time warping on ISIs
    DIST_VM,            ///< dynamic time warping on ISIs

};

/******************************************************************************/
/**
 * \brief The base class for Function.
 */
class Function {
public:
    Function(int n) : m_num(n) {
        m_params.resize(n);
    }
    ~Function() {}

    /// Set number of parameters.
    void set_num(int n) {
        m_num = n;
        m_params.resize(n);
    }
    virtual int set_parameters(int s, const std::vector<double>& params) {
        for (int i = 0; i < m_num; ++i) {
            m_params[i] = params[i+s];
        }
        return s+m_num;
    }

    std::vector<double> get_parameters() const {
        return m_params;
    }

    virtual void set_parameter_names() {}

    std::vector<std::string> get_parameter_names() {
        return m_param_names;
    }

    int get_num_parameters() const {
        return m_num;
    }

    virtual void set_properties(void* p) {}

protected:
    int m_num;
    std::vector<double> m_params;
    std::vector<std::string> m_param_names;
}; // class Function


/******************************************************************************/
/**
 * \brief Customized function (not used) TODO.
 */
class CustomFunction : public Function {
public:
    CustomFunction(int n) : Function(n) {}
protected:

}; // class CustomFunction

/******************************************************************************/
/**
 * \brief Method to calculate distance between two spike trains.
 */
class Distance {

public:
    /// spike trains type.
    typedef std::vector<std::vector<double>> STS;
    typedef std::vector<double> ST;

    Distance() {}

    /**
     * \brief Calculate the distance of *a* given spike train to objective spike
     * trains.
     */
    virtual double calc(const ST& st) const = 0;

    /**
     * \brief Calculate the distance of given *spike trains* to objective spike
     * trains.
     */
    virtual double calc(const STS& sts) const = 0;

    /**
     * \brief Get objective (observed) spike trains.
     */
    STS* get_obj_st() const { return mp_obj_st; }
    /**
     * \brief Set objective (observed) spike trains.
     */
    virtual void set_obj_st(STS* p) { mp_obj_st = p; }

    /**
     * \brief Set tuning parameters for calculating distances.
     */
    virtual void set_tuning_parameters( const std::vector<double>& p) = 0;

private:
    STS* mp_obj_st;         ///< pointer to objective spike trains data.

}; // class Distance

/******************************************************************************/
/**
 * \brief The base class for ResponseKernel.
 *
 * Response kernel calculates the effect of post spikes on the current time.
 */
class ResponseKernel : public Function {
public:
    ResponseKernel(int n) : Function(n) {}
    virtual double calc(double, double, const std::vector<double>&) = 0;

}; // class ResponseKernel

/******************************************************************************/
/**
 * \brief The base class for StimulusKernel.
 *
 * Stimulus kernel generates a stimulus signal from input parameters, and 
 * calculates the effective stimulus value to the neuron.
 */
class StimulusKernel : public Function {
public:
    StimulusKernel(int n) : Function(n) {}
    virtual double calc(double t, const std::vector<double>& stim) = 0;
    /**
     * \brief Get the number of parameters to describe a single stimulus.
     */
    virtual int stim_des_num() { return 1; }

}; // class StimulusKernel

/******************************************************************************/
/**
 * \brief The base class for PerceptionKernel.
 *
 * Perception kernel calculates how the neuron perceives the effective stimulus 
 * value. It takes different forms depending on different models. For example 
 * it could return a stimulus-driven current, or return the expected firing
 * rate.
 */
class PerceptionKernel : public Function {
public:
    PerceptionKernel(int n) : Function(n) {}
    /**
     * \brief Calculate the result for integer-type input value.
     */
    virtual double calc(int v) = 0;
    /**
     * \brief Calculate the result for double-type input value.
     */
    virtual double calc(double v) = 0;

}; // class PerceptionKernel


/******************************************************************************/
/**
 * \brief The base class for ISIDistribution.
 *
 * The proposed distribution for ISIs inside a renewal process model.
 */
class ISIDistribution : public Function {
public:
    ISIDistribution(int n) : Function(n) {}
    /**
     * \brief Calculate the PDF value.
     *
     * Calculate the probability density function value for given parameters.
     *
     * \param isi length of an interspike Interval
     * \param lam firing rate
     */
    virtual double calc_pdf(double isi, double lam) = 0;
    /**
     * \brief Calculate the CDF value.
     *
     * Calculate the cumulative distribution function value for given 
     * parameters.
     */
    virtual double calc_cdf(double isi, double lam) = 0;
    /**
     * \brief Calculate the logarithm of PDF.
     */
    virtual double calc_log_pdf(double isi, double lam) = 0;
    /**
     * \brief Calculate the log-likelihood of a spike train.
     *
     * \param s Starting time of spike train.
     * \param e Ending time of spike train.
     * \param st Spike train.
     * \param lam Firing rate.
     */
    virtual double calc_log_spike_train(double s, double e, 
            const std::vector<double>& st, double lam) = 0;
    /**
     * \brief Calculate the CDF values of all ISIs and two boundaries (if any)
     * of a spike train.
     *
     * \param st Spike train.
     * \param start Starting time of spike train.
     * \param end Ending time of spike train.
     * \param lam Firing rate.
     * \param result Stores the result values.
     */
    virtual void calc_cdf_values(const std::vector<double>& st, double start, 
            double end, double lam, std::vector<double>& result) = 0;
    /**
     * \brief Calculate the CDF of the number of spikes in a spike train.
     *
     * The CDF of count can obtained by convolution of all ISI distributions.
     *
     * \param st Spike train.
     * \param start Starting time of spike train.
     * \param end Ending time of spike train.
     * \param lam Firing rate.
     */
    virtual double calc_count_cdf(const std::vector<double>& st, 
            double start, double end, double lam) = 0;
    /**
     * \brief Calculate the *adjusted* CDF of the number of spikes in a spike 
     * train.
     *
     * Because the CDF of count number is discrete, the adjustment approximates
     * a continuous distribution from the available discrete distribution. 
     * The CDF of count number, n,  can obtained by convolution of all ISI 
     * distributions, using adjustment that calculates the average of CDF(n) and
     * CDF(n-1).
     *
     * \param st Spike train.
     * \param start Starting time of spike train.
     * \param end Ending time of spike train.
     * \param lam Firing rate.
     */
    virtual double calc_count_cdf_adjust(const std::vector<double>& st, 
            double start, double end, double lam) = 0;
    /**
     * \brief Calculate the logarithm of survival probability.
     *
     * The survival probability S = 1 - CDF.
     */
    virtual double log_1_minus_cdf(double lam, double x) { return 0; }

}; // class ISIDistribution


/******************************************************************************/
/**
 * \brief Kernel with no response from post spikes.
 */
class NoneResponseKernel : public ResponseKernel {
public:
    /// constructor
    NoneResponseKernel() : ResponseKernel(0) {}
    NoneResponseKernel(int n) : ResponseKernel(0) {}

    virtual double calc(double start, double now,
            const std::vector<double>& st) {
        return 0;
    }
}; // class NoneResponseKernel

/******************************************************************************/
/**
 * \brief Kernel with no bias from assumption.
 *
 * The kernel uses a linear combination from past spikes with weight values. 
 * No distribution assumption is used. All weight parameters are estimated.
 * The kernel is a piece-wise constant function, with each segamentaion being
 * 0.001s: sum(w_i * h_i) for i = 1, 2, ... , n. Here n by default is 10, and
 * it can also be set to any integer value.
 */
class UnbiasedResponseKernel : public ResponseKernel {
public:
    /**
     * \brief Constructor
     *
     * Take no argument. Use 10 as number of weight values.
     */
    UnbiasedResponseKernel() : ResponseKernel(10) {}
    /**
     * \brief Constructor taking one argument.
     *
     * \param n Number of past miliseconds considered for response effects.
     */
    UnbiasedResponseKernel(int n) : ResponseKernel(n) {}
    /**
     * \brief Calculate response value.
     *
     * \param start Starting time of the currently considered ISI, which is also
     * the last spike time. It should be the last spike time of all considered
     * post spikes.
     * \param now Current time.
     * \param st Spike train.
     */
    virtual double calc(double start, double now, 
                        const std::vector<double>& st);

    virtual void set_parameter_names() {
        for (int i = 0; i < m_num; ++i) {
            m_param_names.push_back("beta_" + std::to_string(i+1));
        }
    }

}; // class ResponseKernel

/******************************************************************************/
/**
 * \brief Response kernel using exponential functions.
 *
 * The kernel is the sum of two decaying exponential functions, one positive
 * and one negative:
 * - f(x) = a * exp(b * x) + c * exp(d * x),
 * where a > 0, b < 0, c < 0, d < 0.
 *
 * For fast computation, discretization is used. The kernel function is
 * approximated by piece-wise constants with segamentaion defined by m_s, and
 * the values are stored in m_vals.
 */
class ExponentialResponseKernel : public ResponseKernel {
public:
    /// constructor
    ExponentialResponseKernel() : ResponseKernel(4) {}
    /// constructor
    ExponentialResponseKernel(int n) : ResponseKernel(4) {}
    /// Calculate response value.
    virtual double calc(double start, double now, 
                        const std::vector<double>& st);

    virtual void set_parameter_names() {
        for (int i = 0; i < m_num; ++i) {
            m_param_names.push_back("kernel_" + std::to_string(i+1));
        }
    }

    virtual int set_parameters(int s, const std::vector<double>& params);

protected:
    std::vector<double> m_vals;     //< Stores the piece-wice constant values.
    size_t m_s;                     //< Discretization segamentaion size.
};


/******************************************************************************/
/**
 * \brief Response kernel using a log-gamma function.
 */
class LogGammaResponseKernel : public ResponseKernel {
public:
    // constructor
    // default number of hist is 10
    LogGammaResponseKernel() : ResponseKernel(5) {}
    LogGammaResponseKernel(int n) : ResponseKernel(5) {}

    virtual double calc(double start, double now, 
                        const std::vector<double>& st);

    virtual void set_parameter_names() {
        for (auto& i : {"alpha", "beta", "a", "sx", "sy"}) {
            m_param_names.push_back(i);
        }
    }
    virtual int set_parameters(int s, const std::vector<double>& params) {
        for (int i = 0; i < m_num; ++i) {
            m_params[i] = params[i+s];
        }
        double alpha = m_params[0];
        double beta = m_params[1];
        double a = m_params[2];
        m_c = a * pow(beta, alpha) / tgamma(alpha);
        return s+m_num;
    }

    std::vector<double> test(double from, double to, int n);

protected:
    double m_c;
};


/******************************************************************************/
/**
 * \brief Exponential distribution for ISIs.
 */
class ISIDistributionExponential : public ISIDistribution {
public:
    ISIDistributionExponential() : ISIDistribution(0) {}

    virtual double calc_pdf(double isi, double lam) {
        return lam * exp(-lam * isi);
    }

    virtual double calc_cdf(double isi, double lam) {
        return 1 - exp(-lam * isi);
    }

    virtual void calc_cdf_values(const std::vector<double>& st, double start, 
                                 double end,double lam, 
                                 std::vector<double>& result);

    virtual double calc_log_pdf(double isi, double lam) {
        return log(lam) - lam * isi;
    }

    virtual double calc_log_spike_train(
            double s, double e, const std::vector<double>& st, double lam);

    virtual void set_parameter_names() {
        m_param_names = {};
    }

    virtual double calc_count_cdf(const std::vector<double>& st, 
            double start, double end, double lam);

    virtual double calc_count_cdf_adjust(const std::vector<double>& st, 
            double start, double end, double lam);

}; // class ISIDistributionExponential

/******************************************************************************/
/**
 * \brief Gamma distribution for ISI.
 *
 * Takes a shape parameter.
 */
class ISIDistributionGamma : public ISIDistribution {
public:
    /// Constructor.
    ISIDistributionGamma() : ISIDistribution(1) {}

    virtual int set_parameters(int s, const std::vector<double>& params) {
        int i = ISIDistribution::set_parameters(s, params);
        m_gamma = m_params[0];
        return i;
    }

    virtual double calc_pdf(double isi, double lam) {
        double beta = m_gamma * lam;
        return pow(beta, m_gamma) / ::tgamma(m_gamma) * pow(isi, m_gamma-1)
            * exp(-beta * isi);
    }

    virtual double calc_cdf(double isi, double lam) {
        if (isi <= 0) return 0;
        return boost::math::gamma_p(m_gamma, m_gamma*lam*isi);
    }

    virtual void calc_cdf_values(const std::vector<double>& st, double start, 
                                 double end, double lam, 
                                 std::vector<double>& result);

    virtual double calc_log_pdf(double isi, double lam) {
        return log(calc_pdf(isi, lam));
    }

    virtual double calc_log_spike_train(
            double s, double e, const std::vector<double>& st, double lam);

    virtual double log_1_minus_cdf(double lam, double x) {
        long double a = static_cast<long double>(m_gamma);
        long double b = static_cast<long double>(m_gamma*lam*x);
        long double s = boost::math::gamma_q(a, b);
        return static_cast<double>(std::log(s));
    }

    virtual void set_parameter_names() {
        m_param_names = {"shape"};
    }

    virtual double calc_count_cdf(const std::vector<double>& st, 
            double start, double end, double lam);

    virtual double calc_count_cdf_adjust(const std::vector<double>& st, 
            double start, double end, double lam);

private:
    double m_gamma;                 ///< shape parameter
    double m_dt = 0.001;            ///< discretization size for integration

}; // class ISIDistributionGamma


/******************************************************************************/
/**
 * \brief Inverse Gaussian distribution for ISI.
 *
 * Takes a variance parameter.
 */
class ISIDistributionInvGauss : public ISIDistribution {
public:
    ISIDistributionInvGauss() : ISIDistribution(1) {}

    virtual int set_parameters(int s, const std::vector<double>& params) {
        int i = ISIDistribution::set_parameters(s, params);
        m_var = m_params[0];
        return i;
    }

    virtual double calc_pdf(double isi, double lam) {
        double mu = 1.0 / lam;
        return sqrt(m_var / 2.0 / M_PI / isi / isi / isi) *
            exp(-m_var * (isi - mu) * (isi - mu) / 2.0 / mu / mu / isi);
    }

    virtual double calc_cdf(double isi, double lam) {
        if (isi <= 0) return 0;
        boost::math::inverse_gaussian ig(1.0/lam, m_var);
        return boost::math::cdf(ig, isi);
    }

    virtual void calc_cdf_values(const std::vector<double>& st, double start,
                                 double end, double lam, 
                                 std::vector<double>& result);

    virtual double calc_log_pdf(double isi, double lam) {
        return log(calc_pdf(isi, lam));
    }

    virtual double calc_log_spike_train(
            double s, double e, const std::vector<double>& st, double lam);

    virtual double log_1_minus_cdf(double lam, double len) {
        long double a = 1.0/lam;
        long double b = m_var;
        long double x = len;
        boost::math::inverse_gaussian_distribution<long double> ig(a, b);
        long double s = boost::math::cdf(boost::math::complement(ig, x));
        if (s == 0) {
            s = std::numeric_limits<long double>::min();
        }
        long double r = std::log(s);
        return static_cast<double>(r);
    }

    virtual void set_parameter_names() {
        m_param_names = {"variance"};
    }

    virtual double calc_count_cdf(const std::vector<double>& st, 
            double start, double end, double lam);

    virtual double calc_count_cdf_adjust(const std::vector<double>& st, 
            double start, double end, double lam);

private:
    double m_var;                   ///< variance parameter
    double m_dt = 0.001;

}; // class ISIDistributionInvGauss


/******************************************************************************/
/**
 * \brief Perception kernel using Gaussian tuning curve.
 *
 * Calculates the firing rate from the direction of random dot patterns.
 */
class GaussianPerceptionKernel : public PerceptionKernel {
public:
    GaussianPerceptionKernel(int n) : PerceptionKernel(3) {}

    virtual double calc(double v) {
        return calc_circ_gauss(v, m_params[0], m_params[1], m_params[2]);
    }

    virtual double calc(int v) {
        return calc_circ_gauss(v, m_params[0], m_params[1], m_params[2]);
    }

    virtual void set_parameter_names() {
        m_param_names = {"A", "D", "sigma"};
    }

protected:
}; // class GaussianPerceptionKernel

/******************************************************************************/
/**
 * \brief Perception kernel using Gaussian tuning curve using a map.
 *
 * Calculates the firing rate from the direction of random dot patterns. Stores
 * calculated value as direction(int)->value(double) pairs in an unordered_map 
 * for fast usage.
 */
class GaussianIntMapPerceptionKernel : public PerceptionKernel {
public:
    GaussianIntMapPerceptionKernel(int n) : PerceptionKernel(3) {}

    virtual double calc(int v) {
        return m_values[v];
    }

    virtual double calc(double v) {
        int i = std::round(v);
        return m_values[i];
    }

    virtual void set_parameter_names() {
        m_param_names = {"A", "D", "sigma"};
    }

    /**
     * \brief Clear and re-fill the map with calculated values.
     */
    virtual int set_parameters(int s, const std::vector<double>& params) {
        int r = PerceptionKernel::set_parameters(s, params);
        m_values.clear();
        for (int v = 0; v <= 330; v+=30) {
            m_values[v] = calc_circ_gauss(
                    v, m_params[0], m_params[1], m_params[2]);
        }
        return r;
    }

protected:
    std::unordered_map<int, double> m_values; ///< map of rate values
}; // class GaussianIntMapPerceptionKernel


/******************************************************************************/
/**
 * \brief Stimulus kernel that always returns one value.
 *
 * Takes one parameter and always return this parameter as the calculated
 * value on any input.
 */
class SimpleStimulusKernel : public StimulusKernel {
public:
    SimpleStimulusKernel(int n) : StimulusKernel(1) {}

    virtual double calc(double t, const std::vector<double>& stim) {
        return m_params[0];
    }

    virtual void set_parameter_names() {
        m_param_names = {"stim"};
    }

    virtual int stim_des_num() { return 0; }

}; // class SimpleStimulusKernel

/******************************************************************************/
/**
 * \brief Empty stimulus kernel that directly returns input.
 *
 * Takes no parameters. Returns whatever the input (described by one
 * value) is.
 */
class EmptyStimulusKernel : public StimulusKernel {
public:
    EmptyStimulusKernel(int n) : StimulusKernel(0) {}

    virtual double calc(double t, const std::vector<double>& stim) {
        return stim[0];
    }

    virtual int stim_des_num() { return 1; }

}; // class EmptyStimulusKernel

/******************************************************************************/
/**
 * \brief Input stimulus kernel that directly returns input.
 *
 * Takes no parameters. Returns whatever the inputs (described by many
 * values) are.
 */
class InputStimulusKernel : public StimulusKernel {
public:
    InputStimulusKernel(int n) : StimulusKernel(0) {}

    virtual double calc(double t, const std::vector<double>& stim) {
        return stim[floor(t/dt)];
    }

    virtual int stim_des_num() { return 1; }

    virtual void set_properties(void* p) {
        dt = *(static_cast<double*>(p));
    }

private:
    double dt;

}; // class InputStimulusKernel

/******************************************************************************/
/**
 * \brief Piece-wise constant stimulus kernel.
 *
 * Takes no parameters. Returns whatever the inputs (described by many
 * values) are.
 * eg., {t1, t2, t3, s0, s1, s2, s3}
 */
class PWStimulusKernel : public StimulusKernel {
public:
    PWStimulusKernel(int n) : StimulusKernel(0) {}

    virtual double calc(double t, const std::vector<double>& stim) {
        int n = stim.size() / 2;
        double s = stim.back();
        for (int i = 0; i < n; ++i) {
            if (t < stim[i]) {
                s = stim[i+n];
                break;
            }
        }
        return s;
    }

    virtual int stim_des_num() { return 1; }

private:
    double dt;

}; // class PWStimulusKernel

/******************************************************************************/
/**
 * \brief Stimulus kernel that returns sinusoidal signal.
 *
 * Takes four parameters, and return the value calculated by
 * - a * sin(b * t + c) + d.
 */
class SinusoidalStimulusKernel : public StimulusKernel {
public:
    SinusoidalStimulusKernel(int n) : StimulusKernel(0) {}

    /**
     *  a * sin(b*x + c) + d
     */
    virtual double calc(double t, const std::vector<double>& stim) {
        return stim[0] * sin(stim[1] * t + stim[2]) + stim[3];
    }

    virtual int stim_des_num() { return 4; }

}; // class SinusoidalStimulusKernel

/******************************************************************************/
/**
 * \brief Spike train distance using dynamic time warping on spike times.
 *
 * Takes one tuning parameter as the alignment panelty; Uses 1 for insert/delete
 * panelty.
 */
class DistanceDTWST : public Distance {
public:
    DistanceDTWST() {}

    virtual double calc(const ST& st) const {
        double sum = 0;
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < get_obj_st()->size(); ++i) {
            auto& obj = (*get_obj_st())[i];
            double dist = calc_dynamic_time_warping_dist(st, obj, m_q);
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
            sum += dist;
        }
        return sum / get_obj_st()->size();
    }

    virtual double calc(const STS& sts) const {
        double sum = 0;
        for (size_t i = 0; i < sts.size(); ++i) {
            sum += calc(sts[i]);
        }
        return sum / sts.size();
    }

    void print_mat(const STS& mat) const {
        for (auto& m1 : mat) {
            for (auto& m2 : m1) {
                std::cout << m2 << "\t";
            }
            std::cout << std::endl;
        }
    }

    /**
     * \brief Set one parameter.
     *
     * Take one tuning parameter for the alignment panelty:
     * - p = q * |t1 - t2|,
     * where q is the parameter.
     */
    virtual void set_tuning_parameters( const std::vector<double>& p) {
        m_q = p[0];
    }

protected:
    double m_q = 5;       ///< alignment panelty.
    int m_bound;            ///< 

}; // class DistanceDTWST

/******************************************************************************/
/**
 * \brief Spike train distance using dynamic time warping on ISIs.
 *
 * Takes one tuning parameter as the alignment panelty; Uses 1 for insert/delete
 * panelty.
 */
class DistanceDTWISI : public Distance {
public:
    DistanceDTWISI() {}

    virtual double calc(const ST& st) const {
        double sum = 0;
        auto isi = get_isi(st);
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < m_isi.size(); ++i) {
            auto& obj = m_isi[i];
            double dist = calc_dynamic_time_warping_dist(isi, obj, m_q);
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
            sum += dist;
        }
        return sum / get_obj_st()->size();
    }

    virtual double calc(const STS& sts) const {
        double sum = 0;
        for (size_t i = 0; i < sts.size(); ++i) {
            sum += calc(sts[i]);
        }
        return sum / sts.size();
    }


    void print_mat(const STS& mat) const {
        for (auto& m1 : mat) {
            for (auto& m2 : m1) {
                std::cout << m2 << "\t";
            }
            std::cout << std::endl;
        }
    }

    /**
     * \brief Set objective spike trains and calculate ISIs.
     */
    virtual void set_obj_st(STS* p) { 
        Distance::set_obj_st(p);
        m_isi.clear();
        for (auto& st : *p) {
            m_isi.push_back(get_isi(st));
        }
    }

    virtual void set_tuning_parameters( const std::vector<double>& p) {
        m_q = p[0];
    }

protected:

    std::vector<double> get_isi(const std::vector<double>& st) const {
        std::vector<double> isi;
        for (size_t i = 1; i < st.size(); ++i) {
            isi.push_back(st[i] - st[i-1]);
        }
        return isi;
    }

    double m_q = 5;
    int m_bound;
    STS m_isi;              ///< store ISIs.

}; // class DistanceDTWISI

/******************************************************************************/
/**
 * \brief ISI Kernel density estimates distance.
 *
 * Uses Gaussian kernel to evaluate the density.
 * The bandwidth is decided by Silverman's rule of thumb:
 * - bw = 1.06 * sd * n ^ (-0.2).
 * Takes two tuning parameters for calculating the ISI density function.
 * - cut: the number of bandwidths before and after the min and max values;
 * - num: the number of discretization on density function.
 */
class DistanceISIDensity : public Distance {

public:
    virtual double calc(const ST& st) const {
        std::vector<double> isi;
        for (size_t i = 1; i < st.size(); ++i) {
            isi.push_back(st[i] - st[i-1]);
        }
        return calc_density_distance(isi);
    }

    virtual double calc(const STS& sts) const {
        std::vector<double> isi;
        for (auto& st : sts) {
            for (size_t i = 1; i < st.size(); ++i) {
                isi.push_back(st[i] - st[i-1]);
            }
        }
        return calc_density_distance(isi);
    }


    virtual void set_obj_st(STS* p) { 
        Distance::set_obj_st(p);
        m_isi.clear();
        for (auto& st : *p) {
            for (size_t i = 1; i < st.size(); ++i) {
                m_isi.push_back(st[i] - st[i-1]);
            }
        }
        calc_full_obj_density();
    }

    virtual void set_tuning_parameters( const std::vector<double>& p) {
        m_cut = p[0];
        m_n = p[1];
        m_dist_method = p[2];
    }

    void print() {
        for (auto i : m_density) {
            std::cout << i << std::endl;
        }
    }

private:
    /**
     * \brief Calculate the whole (one) kernel density of all objective ISIs.
     */
    void calc_full_obj_density () {
        double bw = calc_bandwidth_silverman(m_isi);
        double min = get_min(m_isi);
        double max = get_max(m_isi);
        double from = min - m_cut * bw;
        double to = max + m_cut * bw;
        double dx = (to - from) / m_n;
        m_x.clear();
        m_density.clear();
        for (int i = 0; i <= m_n; ++i) {
            double x = from + i * dx;
            m_x.push_back(x);
            m_density.push_back( calc_kernel_density(x, bw, m_isi) );
        }
    }

    double calc_density_distance(const std::vector<double>& isi) const {
        std::vector<double> density;
        double bw = calc_bandwidth_silverman(isi);
        size_t size = m_x.size();
#ifdef ENABLE_OPENMP
        omp_lock_t writelock;
        omp_init_lock(&writelock);
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < size; ++i) {
            double dens = calc_kernel_density(m_x[i], bw, isi);
#ifdef ENABLE_OPENMP
            omp_set_lock(&writelock);
#endif
            density.push_back(dens);
#ifdef ENABLE_OPENMP
            omp_unset_lock(&writelock);
#endif
        }
#ifdef ENABLE_OPENMP
        omp_destroy_lock(&writelock);
#endif
        double dist;
        if (m_dist_method == 0) {
            dist = calc_dist(m_density, density) * sqrt(m_x[1]-m_x[0]);
        } else {
            dist = calc_KL_divergence(m_density, density, m_x[1]-m_x[0]);
        }
        return dist;
    }

    std::vector<double> m_isi;      ///< stores objective ISI.

    std::vector<double> m_x;        ///< x values of objective kernel density.
    std::vector<double> m_density;  ///< density value corresponding to m_x.

    double m_cut = 3;               ///< number of bandwidths exceeding limits.
    double m_n = 100;               ///< number of discretization of density.
    bool m_dist_method = 0;         //*< method to calculate density distance 
                                    //*< - 0: Euclidean distance
                                    //*< - 1: K-L divergence 
};

/******************************************************************************/
/**
 * \brief van Rossum distance.
 *
 * Uses Gaussian kernel to evaluate the density.
 * The bandwidth is decided by Silverman's rule of thumb:
 * - bw = 1.06 * sd * n ^ (-0.2).
 * Takes two tuning parameters for calculating the ISI density function.
 * - cut: the number of bandwidths before and after the min and max values;
 * - num: the number of discretization on density function.
 */
class DistanceVanRossum : public Distance {

public:
    virtual double calc(const ST& st) const {
        std::vector<double> density;
        double bw = calc_bandwidth_silverman(st);
        size_t size = m_x.size();
#ifdef ENABLE_OPENMP
        omp_lock_t writelock;
        omp_init_lock(&writelock);
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < size; ++i) {
            double dens = calc_kernel_density(m_x[i], bw, st);
#ifdef ENABLE_OPENMP
            omp_set_lock(&writelock);
#endif
            density.push_back(dens);
#ifdef ENABLE_OPENMP
            omp_unset_lock(&writelock);
#endif
        }
#ifdef ENABLE_OPENMP
        omp_destroy_lock(&writelock);
#endif
        double dist = calc_dist(m_density, density) * sqrt(m_x[1]-m_x[0]);
        return dist;
    }

    virtual double calc(const STS& sts) const {
        std::vector<double> onest;
        for (auto& st : sts) {
            for (auto i : st) {
                onest.push_back(i);
            }
        }
        return calc(onest);
    }

    virtual void set_obj_st(STS* p) { 
        Distance::set_obj_st(p);
        m_st.clear();
        for (auto& st : *p) {
            for (auto i : st) {
                m_st.push_back(i);
            }
        }
        calc_full_obj_density();
    }

    virtual void set_tuning_parameters( const std::vector<double>& p) {
        m_cut = p[0];
        m_n = p[1];
    }

    void print() {
        for (auto i : m_density) {
            std::cout << i << std::endl;
        }
    }
private:
    /**
     * \brief Calculate the whole (one) kernel density of all objective ISIs.
     */
    void calc_full_obj_density () {
        double bw = calc_bandwidth_silverman(m_st);
        double min = get_min(m_st);
        double max = get_max(m_st);
        double from = min - m_cut * bw;
        double to = max + m_cut * bw;
        double dx = (to - from) / m_n;
        m_x.clear();
        m_density.clear();
        for (int i = 0; i <= m_n; ++i) {
            double x = from + i * dx;
            m_x.push_back(x);
            m_density.push_back( calc_kernel_density(x, bw, m_st) );
        }
    }

    std::vector<double> m_st;       ///< stores objective all spike times.

    std::vector<double> m_x;        ///< x values of objective kernel density.
    std::vector<double> m_density;  ///< density value corresponding to m_x.

    double m_cut = 3;               ///< number of bandwidths exceeding limits.
    double m_n = 100;               ///< number of discretization of density.
};

/******************************************************************************/
/**
 * \brief Create response kernels indicated by FuncType.
 *
 * \param ft Indicate the FuncType to create.
 * \param n Number of parameters for the kernel.
 */
std::shared_ptr<ResponseKernel> create_response_kernel(FuncType ft, int n );
std::shared_ptr<StimulusKernel> create_stimulus_kernel(FuncType ft, int n );
std::shared_ptr<PerceptionKernel> create_perception_kernel(FuncType ft, int n );
std::shared_ptr<ISIDistribution> create_isi_distribution(FuncType ft);
std::shared_ptr<Distance> create_distance_method(FuncType ft);


} // namespace mass

#endif // FUNCTIONS_H
