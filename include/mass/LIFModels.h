#ifndef MASS_LIFModels_H
#define MASS_LIFModels_H

#include <mass/LIF.h>
#include <mass/Functions.h>
#include <mass/Helper.h>

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/ml.hpp"


namespace mass {

enum DecodingMethod {
    DM_MARGINAL,
    DM_EM,
    DM_KMEANS,
    DM_LL_KMEANS,
    DM_LL_EM1,
    DM_LL_EM2,
    DM_LL_EM3,
    DM_LL_KMEANS_SE,
    INIT_RAND,
    INIT_KMPP,
    INIT_LDCI,
};

class ResponseLIF : public LIF <std::vector<double>> {
    
public:
    ResponseLIF(double dt, double dx, double reset, FuncType rk, int n_rk, 
                FuncType sk, int n_sk) : LIF(dt, dx, reset) {
        mp_rk = create_response_kernel(rk, n_rk);
        mp_sk = create_stimulus_kernel(sk, n_sk);
        m_rk = rk; m_sk = sk; m_n_rk = n_rk; m_n_sk = n_sk;
        for (auto& i : {"conductance", "resting", "diffusion"}) {
            m_param_names.push_back(i);
        }
        for (auto& i : mp_rk->get_parameter_names()) {
            m_param_names.push_back(i);
        }
        for (auto& i : mp_sk->get_parameter_names()) {
            m_param_names.push_back(i);
        }
    }
    ~ResponseLIF() {}

    // should be overloaded
    virtual double get_conductance() const { return m_conductance; }

    std::shared_ptr<StimulusKernel> get_stimulus_kernel() {
        return mp_sk;
    }

    std::shared_ptr<ResponseKernel> get_response_kernel() {
        return mp_rk;
    }

    
    // calc rk
    virtual double calc_rk(double start, double t, const std::vector<double>& st) const {
        return mp_rk->calc(start, t, st);
    }
    // calc sk
    virtual double calc_sk(double t, const std::vector<double>& stim) const {
        return mp_sk->calc(t, stim);
    }

protected:
    virtual double calc_drift(double h, double stim, double x) const {
        return m_conductance*(m_x_resting - x) + stim + h;
    }
    // TODO translate substim to std vector double for stimulus kernel
    virtual double calc_drift(double start, double t, double x, 
                              const std::vector<double>& substim, 
                              const std::vector<double>& st) const {
        double h = mp_rk->calc(start, t, st);
        double stim = mp_sk->calc(t, substim);
        return m_conductance*(m_x_resting - x) + stim + h;
    }

    virtual double calc_diffusion(double t, double x) const {
        return m_diffusion;
    }

    virtual int process_parameters() {
        int i = 0;
        m_conductance = m_params[i];
        ++i;
        m_x_resting = m_params[i];
        ++i;
        m_diffusion = m_params[i];
        ++i;
        i = mp_sk->set_parameters(i, m_params);
        i = mp_rk->set_parameters(i, m_params);
        return i;
    }

    virtual void set_stimuli_from_params() {
        this->m_stimuli.clear();
        int n = this->mp_sk->stim_des_num();
        for (size_t i = 0; i < this->m_decoding_params.size(); i+=n) {
            std::vector<double> one_stim;
            for (int j = 0; j < n; ++j) {
                one_stim.push_back(this->m_decoding_params[i+j]);
            }
            this->m_stimuli.push_back(one_stim);
        }
    }

    std::shared_ptr<StimulusKernel> mp_sk;
    std::shared_ptr<ResponseKernel> mp_rk;

    FuncType m_rk, m_sk;
    int m_n_rk, m_n_sk;

    double m_conductance;
    double m_x_resting;
    double m_diffusion;

};


/**
 *  Probability-mixing response LIF model
 */
class ProbMixResponseLIF : public ResponseLIF {
public:
    ProbMixResponseLIF(double dt, double dx, double reset, FuncType rk, 
                       int n_rk, FuncType sk, int n_sk, int num_mix)
        : ResponseLIF(dt, dx, reset, rk, n_rk, sk, n_sk), 
          m_num_mixtures(num_mix) {
        m_weights.resize(m_num_mixtures, 0);
    }

    void set_num_mixtures(int n) {
        m_num_mixtures = n;
        m_weights.resize(m_num_mixtures, 0);
    }

    int get_num_mixtures() { return m_num_mixtures; }

    void set_em(bool em) {
        m_em = em;
    }

    void set_decoding_params_reuse(bool b) {
        m_decoding_params_reuse = b;
    }

    void set_init(DecodingMethod init) {
        m_init = init;
    }

    void set_opt_trial_num(int n) {
        m_opt_trial_num = n;
    }

    void set_kmpp_trial_num(int n) {
        m_kmpp_trial_num = n;
    }

    void set_ldci_trial_num(int n) {
        m_ldci_trial_num = n;
    }

    virtual void optimize() {
        if (!m_em) {
            ResponseLIF::optimize();
        } else {
            optimize_em();
        }
    }

    void optimize_em() {
        double f_min = 0;
        std::vector<double> init_params = m_params;
        if (m_maxeval_global > 0) {
            set_em(false);
            m_count = 0;
            run_opt(m_algorithm_global, m_maxeval_global, init_params, f_min);
            /*
            std::cout << "#global loop: " << m_count << std::endl;
            std::cout << "#init";
            for (auto v : init_params) {
                std::cout << "\t" << v;
            }
            std::cout << std::endl;
            */
        }
        if (m_maxeval > 0) {
            set_em(true);
            m_count = 0;
            std::vector<double> params_old = init_params;
            double f_old = 2 * f_min;
            set_parameters(init_params);
            calc_em_cond_probs(*(this->mp_stimuli));
            while (m_count == 0 || 
                   !is_em_converged(f_old, f_min, params_old, init_params)) {
                f_old = f_min;
                params_old = init_params;
                run_opt(m_algorithm, m_maxeval, init_params, f_min);

                /*
                std::cout << "*em\t" << f_min << "\t" << m_count;
                for (double v : init_params) {
                    std::cout << "\t" << v;
                }
                std::cout << std::endl;
                */
            }
            if (f_old < f_min) {
                f_min = f_old;
                init_params = params_old;
            }
        }
        m_min_f = f_min;
        set_parameters(init_params);
    }

    virtual void decode() {
        if (m_dm == DM_MARGINAL || m_dm == DM_EM) {
            decode_bayesian();
        } else if (m_dm == DM_LL_KMEANS || m_dm == DM_LL_EM1 || m_dm == DM_LL_EM2 || m_dm == DM_LL_EM3 || m_dm == DM_LL_KMEANS_SE) {
            decode_for_mixture();
        } else if (m_dm == DM_KMEANS) {
            decode_kmeans_lld();
        }
    }

    void decode_em() {
        double f_min = 100;
        std::vector<double> init_params = m_decoding_params;
        if (m_maxeval_global > 0) {
            set_em(false);
            m_count = 0;
            run_opt_decode(m_algorithm_global, m_maxeval_global, 
                           init_params, f_min);
            std::cout << "#global loop: " << m_count << std::endl;
            std::cout << "#init";
            for (auto v : init_params) {
                std::cout << "\t" << v;
            }
            std::cout << std::endl;
        }
        set_em(true);
        m_count = 0;
        int step = 0;
        std::vector<double> params_old = init_params;
        double f_old = f_min*2;
        set_decoding_parameters(init_params);
        calc_em_cond_probs(this->m_stimuli);
        estimate_em_weights();
        while ((step < 100 && !is_em_converged(f_old, f_min, params_old, init_params)) || step < 2) {
            f_old = f_min;
            params_old = init_params;
            run_opt_decode(m_algorithm, m_maxeval, init_params, f_min);
            //run_opt_decode(m_algorithm, m_maxeval/20 , init_params, f_min);
            calc_em_cond_probs(this->m_stimuli);
            estimate_em_weights();
            step++;

            /*
               std::cout << "*em\t" << f_min << "\t" << m_count;
               for (double v : init_params) {
               std::cout << "\t" << v;
               }
               std::cout << std::endl;
               */
        }
        if (f_old < f_min) {
            f_min = f_old;
            init_params = params_old;
        }
        m_min_f = f_min;
        set_decoding_parameters(init_params);
    }

    void decode_bayesian() {
        auto ori_params = m_decoding_params;
        auto ori_ub = m_ub;
        auto ori_lb = m_lb;
        int nmix = m_num_mixtures;
        double min_score = HUGE_VAL;  // used to record best decoding
        std::vector<double> opt_decode;
        std::vector<int> opt_index;
        size_t data_size = mp_spike_trains->size();
        int data_num = 0;
        for (auto& st : *mp_spike_trains) {
            data_num += st.size() - 1;
        }
        ResponseLIF rlif(m_dt, m_dx, m_x_reset, m_rk, m_n_rk, m_sk, m_n_sk);
        auto params = m_params;
        // FIXME
        if (m_num_mixtures > 1) {
            params.pop_back();
        }
        std::vector<double> d_lb, d_ub;
        for (int j = 0; j < mp_sk->stim_des_num(); ++j) {
            d_lb.push_back(m_lb[j]);
            d_ub.push_back(m_ub[j]);
        }
        rlif.set_parameters(params);
        rlif.set_maxeval(m_maxeval);
        rlif.set_maxeval_global(m_maxeval_global);
        rlif.set_lower_bounds(d_lb);
        rlif.set_upper_bounds(d_ub);
        // FIXME k
        for (int k = nmix; k < nmix+1; ++k) {
            if (m_init == INIT_LDCI && !m_decoding_params_reuse) {
                auto mdm = m_dm;
                set_decoding_method(DM_LL_EM1);
                decode_for_mixture(true);
                set_decoding_method(mdm);
            }
            set_num_mixtures(k);
            double min_f_trial = HUGE_VAL;
            for (int trial_num = 0; trial_num < m_opt_trial_num; ++trial_num) {
                int n = mp_sk->stim_des_num();
                std::vector<double> ub, lb;
                std::vector<double> decoding_params;
                auto generate_params = m_decode_cluster_params;
                if (!m_decoding_params_reuse) {
                    if (m_init == INIT_KMPP) {
                        generate_params = kmeanspp_initialize(k, rlif);
                    } else if (m_init == INIT_RAND) {
                        generate_params = rand_initialize(k, rlif);
                        //generate_params.resize(k);
                        //for (int i = 0; i < k; i+=1) {
                            //generate_params[i] = random_decoding_parameter();
                        //}
                    } else if (m_init == INIT_LDCI) {
                        generate_params = ldci_initializer(trial_num);
                    }
                }
                for (int i = 0; i < k; i+=1) {
                    for (int j = 0; j < n; ++j) {
                        decoding_params.push_back(generate_params[i][j]);
                        ub.push_back(ori_ub[j]);
                        lb.push_back(ori_lb[j]);
                    }
                }
                for (int i = 0; i < k-1; i+=1) {
                    decoding_params.push_back(1.0/k);
                    lb.push_back(-10);
                    ub.push_back(10);
                }
                set_decoding_parameters(decoding_params);
                set_upper_bounds(ub);
                set_lower_bounds(lb);
                if (m_dm == DM_EM) {
                    decode_em();
                } else {
                    ResponseLIF::decode();
                }
                set_upper_bounds(ub);
                set_lower_bounds(lb);
                double score = m_min_f * 2 + (mp_sk->stim_des_num() * k + k - 1) * log(data_num);
                if (min_score > score) {
                    min_score = score;
                    opt_decode = m_decoding_params;
                    opt_index.clear();
                    // docode state index
                    for (size_t i = 0; i < data_size; ++i) {
                        double logs = -HUGE_VAL;
                        int ind = 0;
                        for (int j = 0; j < k; ++j) {
                            double tmp = calc_spike_train_loglik( 
                                    m_stimuli[j], (*mp_spike_trains)[i], 0, 0);
                            double new_log = log(m_weights[j]) + tmp;
                            if (logs < new_log) {
                                logs = new_log;
                                ind = j;
                            }
                        }
                        opt_index.push_back(ind);
                    }
                }
                if (m_min_f < min_f_trial) {
                    min_f_trial = m_min_f;
                    m_decode_map_params = opt_decode;
                    m_decode_map_index = opt_index;
                }
                // in case min_F is nan
                if (m_decode_map_params.empty()) {
                    m_decode_map_params = opt_decode;
                }
                if (m_decode_map_index.empty()) {
                    m_decode_map_index = opt_index;
                }
            }
        }
    }

    void estimate_em_weights() {
        for (int i = 0; i < m_num_mixtures-1; ++i) {
            double s = 0;
            for (auto j : m_em_cond_probs) {
                s += j[i];
            }
            if (s == 0) s = 1e-7;
            int pnum = mp_sk->stim_des_num() * m_num_mixtures;
            double v = s / mp_spike_trains->size();
            m_decoding_params[ pnum + i] = v;
            m_lb[pnum+i] = v;
            m_ub[pnum+i] = v;
        }
    }

    std::vector<std::vector<double>> ldci_initializer(int repn) {
        if (repn == 0) {
            return m_ldci_optim_params;
        }
        std::vector<std::vector<double>> res;
        std::random_device rd;
        std::mt19937 g(rd());
        for (auto& params : m_ldci_params) {
            std::shuffle(params.begin(), params.end(), g);
            if (params.size() > 0) {
                res.push_back(params[0]);
            } else {
                auto ss = m_single_decoding_result;
                std::shuffle(ss.begin(), ss.end(), g);
                res.push_back(ss[0]);
            }
        }
        return res;
    }

    std::vector<std::vector<double>> rand_initialize(int k, ResponseLIF& rlif) {
        // decode random k spike trains
        int sz = mp_spike_trains->size();
        if (k > sz) {
            std::cout << "ERRRORRR, too many clusters" << std::endl;
        }
        std::vector<std::vector<double>> decoding_result(k);

        std::vector<int> indx(sz);
        for (int iindx = 0; iindx < sz; ++iindx) indx[iindx] = iindx;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::shuffle(indx.begin(), indx.end(), gen);

        for (int kk = 0; kk < k; ++kk) {
            double minf_dec = HUGE_VAL;
            std::vector<double> optdecparams;
            std::vector<std::vector<double>> ss = {(*mp_spike_trains)[indx[kk]]};
            rlif.set_spike_trains( &ss );
            for (int repn = 0; repn < m_kmpp_trial_num; ++repn) {
                rlif.set_decoding_parameters(random_decoding_parameter());
                rlif.decode();
                double newminf = rlif.get_min_f();
                if (newminf < minf_dec) {
                    minf_dec = newminf;
                    optdecparams = rlif.get_decoding_parameters();
                }
            }
            decoding_result[kk] = optdecparams;
        }
        return decoding_result;
    }


    std::vector<std::vector<double>> kmeanspp_initialize(int k, ResponseLIF& rlif) {
        // decode random k spike trains
        int sz = mp_spike_trains->size();
        if (k > sz) {
            std::cout << "ERRRORRR, too many clusters" << std::endl;
        }
        std::vector<int> centers(k);
        std::vector<std::vector<double>> decoding_result(k);
        std::vector<double> mllks(sz);

        std::vector<int> indx(sz);
        for (int iindx = 0; iindx < sz; ++iindx) indx[iindx] = iindx;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::shuffle(indx.begin(), indx.end(), gen);
        rlif.set_decoding_parameters(random_decoding_parameter());
        std::vector<std::vector<double>> ss = {(*mp_spike_trains)[indx[0]]};
        rlif.set_spike_trains( &ss );
        //rlif.decode();

        double minf_dec = HUGE_VAL;
        std::vector<double> optdecparams;
        for (int repn = 0; repn < m_kmpp_trial_num; ++repn) {
            rlif.set_decoding_parameters(random_decoding_parameter());
            rlif.decode();
            double newminf = rlif.get_min_f();
            if (newminf < minf_dec) {
                minf_dec = newminf;
                optdecparams = rlif.get_decoding_parameters();
            }
        }

        centers[0] = indx[0];
        decoding_result[0] = optdecparams;

        for (int i = 1; i < k; ++i) {
            // calculate likelihood
            for (int j = 0; j < sz; ++j) {
                double mllk = HUGE_VAL;
                for (int c = 0; c < i; ++c) {
                    if (centers[c] == j) continue;
                    double tmpmllk = -rlif.calc_spike_train_loglik(
                            decoding_result[c], (*mp_spike_trains)[j], 0, 0)/
                        (*mp_spike_trains)[j].size();
                    if (tmpmllk < mllk) {
                        mllk = tmpmllk;
                    }
                }
                mllks[j] = mllk;
            }
            // normalize to 0 and 1
            double minmllk = HUGE_VAL, maxmllk = -HUGE_VAL;
            for (int j = 0; j < sz; ++j) {
                bool not_center = true;
                for (int c = 0; c < i; ++c) {
                    if (centers[c] == j)
                        not_center = false;
                }
                if (not_center) {
                    if (minmllk > mllks[j])
                        minmllk = mllks[j];
                    if (maxmllk < mllks[j])
                        maxmllk = mllks[j];
                }
            }
            double dismllk = maxmllk - minmllk;
            for (int j = 0; j < sz; ++j) {
                bool not_center = true;
                for (int c = 0; c < i; ++c) {
                    if (centers[c] == j)
                        not_center = false;
                }
                if (not_center) {
                    mllks[j] = (mllks[j] - minmllk) / dismllk;
                }
            }
            for (int c = 0; c < i; ++c) {
                mllks[centers[c]] = 0;
            }
            // random sample the next center
            std::discrete_distribution<> d(mllks.begin(), mllks.end());
            int nc = d(gen);
            for (int c = 0; c < i; ++c) {
                if (nc == centers[c]) {
                    std::cout << "EEEEEEEEEEERRRRROR!!!!" << std::endl;
                    nc = d(gen);
                }
            }
            centers[i] = nc;
            rlif.set_decoding_parameters(random_decoding_parameter());
            std::vector<std::vector<double>> ss = {(*mp_spike_trains)[nc]};
            rlif.set_spike_trains( &ss );

            minf_dec = HUGE_VAL;
            for (int repn = 0; repn < m_kmpp_trial_num; ++repn) {
                rlif.set_decoding_parameters(random_decoding_parameter());
                rlif.decode();
                double newminf = rlif.get_min_f();
                if (newminf < minf_dec) {
                    minf_dec = newminf;
                    optdecparams = rlif.get_decoding_parameters();
                }
            }

            //rlif.decode();
            decoding_result[i] = optdecparams;
        }
        return decoding_result;
    }

    void decode_kmeans_lld() {
        // use a response model without probability mixtures
        ResponseLIF rlif(m_dt, m_dx, m_x_reset, m_rk, m_n_rk, m_sk, m_n_sk);
        auto params = m_params;
        // FIXME
        if (m_num_mixtures > 1) {
            params.pop_back();
        }
        std::vector<double> decoding_params, d_lb, d_ub;
        for (int j = 0; j < mp_sk->stim_des_num(); ++j) {
            decoding_params.push_back(m_decoding_params[j]);
            d_lb.push_back(m_lb[j]);
            d_ub.push_back(m_ub[j]);
        }
        rlif.set_parameters(params);
        rlif.set_maxeval(m_maxeval);
        rlif.set_maxeval_global(m_maxeval_global);
        rlif.set_lower_bounds(d_lb);
        rlif.set_upper_bounds(d_ub);

        // FIXME k
        for (int k = m_num_mixtures; k < m_num_mixtures+1; ++k) {

            if (m_init == INIT_LDCI && !m_decoding_params_reuse) {
                auto mdm = m_dm;
                set_decoding_method(DM_LL_EM1);
                decode_for_mixture(true);
                set_decoding_method(mdm);
            }
            double min_f_trial = HUGE_VAL;
            auto min_decode_class = m_decode_cluster_index;
            std::vector<std::vector<double>> decoding_result(k);
            // run multiple opt trials and use the one with smallest llk
            for (int trialn = 0; trialn < m_opt_trial_num; ++trialn) {
                set_num_mixtures(k);
                int step = 0;
                // decode random k spike trains
                int sz = mp_spike_trains->size();
                /*
                if (k > sz) {
                    std::cout << "ERRRORRR, too many clusters" << std::endl;
                }
                std::vector<int> indx(sz);
                for (int iindx = 0; iindx < sz; ++iindx) indx[iindx] = iindx;
                std::random_device rd;
                std::mt19937 gen(rd());
                std::shuffle(indx.begin(), indx.end(), gen);
                for (int i = 0; i < k; ++i) {
                    rlif.set_decoding_parameters(random_decoding_parameter());
                    std::vector<std::vector<double>> ss = {(*mp_spike_trains)[indx[i]]};
                    rlif.set_spike_trains( &ss );
                    rlif.decode();
                    decoding_result[i] = rlif.get_decoding_parameters();
                }
                */
                // assign random starting parameters
                /*
                for (int i = 0; i < k; ++i) {
                    decoding_result[i] = random_decoding_parameter();
                }
                */
                if (m_decoding_params_reuse) {
                    decoding_result = m_decode_cluster_params;
                } else {
                    if (m_init == INIT_KMPP) {
                        decoding_result = kmeanspp_initialize(k, rlif);
                    } else if (m_init == INIT_RAND) {
                        decoding_result = rand_initialize(k, rlif);
                        //for (int i = 0; i < k; i+=1) {
                            //decoding_result[i] = random_decoding_parameter();
                        //}
                    } else if (m_init == INIT_LDCI) {
                        decoding_result = ldci_initializer(trialn);
                    }
                }

                double f_trial = 1e-5;
                double f_trial_old = 2e-5;
                while ((std::abs((f_trial - f_trial_old)/f_trial_old) > m_ftol && step < 100) || step < 2) {
                    f_trial_old = f_trial;
                    f_trial = 0.0;
                    step += 1;
                    std::vector<std::vector<int>> classes(k);

                    // classify each spike train into a cluster based on likelihood
                    for (int i = 0; i < sz; ++i) {
                        std::vector<double> logs(k, 0.0);
                        for (int j = 0; j < k; ++j) {
                            logs[j] = rlif.calc_spike_train_loglik( 
                                    decoding_result[j],
                                    (*mp_spike_trains)[i],
                                    0,0);
                        }
                        int maxid = std::max_element(logs.begin(), logs.end()) - logs.begin();
                        classes[maxid].push_back(i);
                    }

                    // estimate new k clusters
                    for (int i = 0; i < k; ++i) {
                        if (classes[i].size() == 0) {
                            continue;
                        }
                        std::vector<std::vector<double>> ss;
                        for (size_t j : classes[i]) {
                            if (j > mp_spike_trains->size()) break;
                            ss.push_back( (*mp_spike_trains)[j] );
                        }
                        rlif.set_spike_trains(&ss);
                        rlif.set_decoding_parameters( decoding_result[i] );
                        rlif.decode();
                        auto newresult = rlif.get_decoding_parameters();
                        /*
                        for (size_t n = 0; n < newresult.size(); ++n) {
                            thresh += std::pow((newresult[n] - decoding_result[i][n]), 2);
                        }
                        */
                        decoding_result[i] = rlif.get_decoding_parameters();
                        f_trial += rlif.get_min_f();
                    }
                    min_decode_class = classes;
                }
                if (f_trial < min_f_trial) {
                    min_f_trial = f_trial;
                    m_decode_cluster_index = min_decode_class;
                    m_decode_cluster_params  = decoding_result;
                }
            }
        }
    }


    std::vector<std::vector<double>> ldctest(int psize) {
        std::vector<std::vector<double>> decoding_result;
        std::vector<double> init_params = m_decoding_params;
        // use a response model without probability mixtures
        ResponseLIF rlif(m_dt, m_dx, m_x_reset, m_rk, m_n_rk, m_sk, m_n_sk);
        auto params = m_params;
        // FIXME
        if (m_num_mixtures > 1) {
            params.pop_back();
        }
        /*
        for (int j = 1; j < m_num_mixtures; ++j) {
            params.pop_back();
        }
        */
        std::vector<double> decoding_params, d_lb, d_ub;
        for (int j = 0; j < mp_sk->stim_des_num(); ++j) {
            decoding_params.push_back(m_decoding_params[j]);
            d_lb.push_back(m_lb[j]);
            d_ub.push_back(m_ub[j]);
        }
        rlif.set_parameters(params);
        rlif.set_maxeval(m_maxeval);
        rlif.set_maxeval_global(m_maxeval_global);
        rlif.set_lower_bounds(d_lb);
        rlif.set_upper_bounds(d_ub);

        // decode each spike train
        size_t data_size = mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            std::vector<std::vector<double>> ss = {(*mp_spike_trains)[i]};
            rlif.set_spike_trains( &ss );
            double minf_dec = HUGE_VAL;
            std::vector<double> optdecparams;
            for (int repn = 0; repn < m_opt_trial_num; ++repn) {
                rlif.set_decoding_parameters(random_decoding_parameter());
                rlif.decode();
                double newminf = rlif.get_min_f();
                if (newminf < minf_dec) {
                    minf_dec = newminf;
                    optdecparams = rlif.get_decoding_parameters();
                }
            }
            decoding_result.push_back(optdecparams);
        }

        // make spike train vectors of size n (number of spike trains)
        // consisting of log-likelihood values of all decoded stimuli
        std::vector<std::vector<double>> ldm(data_size);
        rlif.set_keep_inf(false); // to get stable log likelihood values
        for (size_t i = 0; i < data_size; ++i) {
            //std::vector<std::vector<double>> ss = {(*mp_spike_trains)[i]};
            //rlif.set_spike_trains( &ss );
            std::vector<double> st_vec;
            for (auto& j : decoding_result) {
                //rlif.set_decoding_parameters(j);
                //st_vec.push_back(-rlif.calc_obj_func_decode());
                double res = -rlif.calc_spike_train_loglik(j, (*mp_spike_trains)[i], 0, 0) / (*mp_spike_trains)[i].size();
                st_vec.push_back(res);
            }
            ldm[i] = st_vec;
        }
        rlif.set_keep_inf(true); 

        return ldm;

        /*
        for (auto& i : ldm) {
            for (auto j : i )
                std::cout << j << "\t";
            std::cout << std::endl;
        }
        */

        // modify
        //for (auto& tmp1 : ldm) {
        //    for (auto& tmp2 : tmp1) {
        //        if ( tmp2 < 0) tmp2 = exp(tmp2) - 1;
        //    }
        //}


        // augment vectors, combining rows and columns
        auto ldm_aug = ldm;
        int sz = ldm.size();
        for (int i = 0; i < sz; ++i) {
            for (int j = 0; j < sz; ++j) {
                ldm_aug[i].push_back(ldm_aug[j][i]);
            }
        }

        // reverse
        auto ldm_t = ldm;
        for (int i = 1; i < sz; ++i) {
            for (int j = 0; j < i; ++j) {
                std::swap(ldm_t[i][j], ldm_t[j][i]);
            }
        }

        // symmetric matrix
        auto ldm_sym = ldm;
        for (int i = 0; i < sz; ++i) {
            for (int j = 0; j < sz; ++j) {
                ldm_sym[i][j] = 0.5 * (ldm[i][j] + ldm_t[i][j]);
            }
        }

        // partial
        std::vector<std::vector<double>> ldm_p;
        std::vector<int> id_v(sz);
        std::iota(id_v.begin(), id_v.end(), 0);
        std::shuffle(id_v.begin(), id_v.end(), 
                std::mt19937{std::random_device{}()});
        if (psize > sz) psize = sz;
        for (int j = 0; j < sz; ++j) {
            ldm_p.push_back(std::vector<double>());
            for (int i = 0; i < psize; ++i) {
                ldm_p.back().push_back(ldm[j][id_v[i]]);
            }
        }

        // opencv matrix
        std::vector<double> datamat;
        for (auto& i : ldm) {
            for (auto j : i) {
                datamat.push_back(j);
            }
        }
        cv::Mat samples( sz, sz, cv::DataType<double>::type, datamat.data() );
        samples.convertTo(samples, CV_32F);
        cv::Mat labels;


        std::vector<double> datamat_aug;
        for (auto& i : ldm_aug) {
            for (auto j : i) {
                datamat_aug.push_back(j);
            }
        }
        cv::Mat samples_aug( sz, 2*sz, cv::DataType<double>::type, datamat_aug.data() );
        samples_aug.convertTo(samples_aug, CV_32F);
        cv::Mat labels_aug;

        std::vector<double> datamat_t;
        for (auto& i : ldm_t) {
            for (auto j : i) {
                datamat_t.push_back(j);
            }
        }
        cv::Mat samples_t( sz, sz, cv::DataType<double>::type, datamat_t.data() );
        samples_t.convertTo(samples_t, CV_32F);
        cv::Mat labels_t;

        std::vector<double> datamat_sym;
        for (auto& i : ldm_sym) {
            for (auto j : i) {
                datamat_sym.push_back(j);
            }
        }
        cv::Mat samples_sym( sz, sz, cv::DataType<double>::type, datamat_sym.data() );
        samples_sym.convertTo(samples_sym, CV_32F);
        cv::Mat labels_sym;


        std::vector<double> datamat_p;
        for (auto& i : ldm_p) {
            for (auto j : i) {
                datamat_p.push_back(j);
            }
        }
        cv::Mat samples_p( sz, psize, cv::DataType<double>::type, datamat_p.data() );
        samples_p.convertTo(samples_p, CV_32F);
        cv::Mat labels_p;

        cv::Ptr<cv::ml::EM> em_model = cv::ml::EM::create();
        em_model->setClustersNumber(m_num_mixtures);
        em_model->setTermCriteria(cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 100, 1e-5));
        std::vector<std::vector<int>> results;

        em_model->setCovarianceMatrixType(cv::ml::EM::COV_MAT_SPHERICAL);
        em_model->trainEM( samples, cv::noArray(), labels, cv::noArray() );
        std::vector<int> classes(sz);
        for (int i = 0; i < sz; ++i)
            classes[i] = labels.at<int>(i,0);
        results.push_back(classes);


        em_model->trainEM( samples_aug, cv::noArray(), labels_aug, cv::noArray() );
        std::vector<int> classes_aug(sz);
        for (int i = 0; i < sz; ++i)
            classes_aug[i] = labels_aug.at<int>(i,0);
        results.push_back(classes_aug);


        em_model->trainEM( samples_t, cv::noArray(), labels_t, cv::noArray() );
        std::vector<int> classes_t(sz);
        for (int i = 0; i < sz; ++i)
            classes_t[i] = labels_t.at<int>(i,0);
        results.push_back(classes_t);

        em_model->trainEM( samples_sym, cv::noArray(), labels_sym, cv::noArray() );
        std::vector<int> classes_sym(sz);
        for (int i = 0; i < sz; ++i)
            classes_sym[i] = labels_sym.at<int>(i,0);
        results.push_back(classes_sym);

        em_model->setCovarianceMatrixType(cv::ml::EM::COV_MAT_DIAGONAL);
        em_model->trainEM( samples_p, cv::noArray(), labels_p, cv::noArray() );
        std::vector<int> classes_p(sz);
        for (int i = 0; i < sz; ++i)
            classes_p[i] = labels_p.at<int>(i,0);
        results.push_back(classes_p);

        //return results;
    }


    void decode_for_mixture(bool as_init = false) {
        std::vector<std::vector<double>> decoding_result;
        std::vector<double> init_params = m_decoding_params;
        // use a response model without probability mixtures
        ResponseLIF rlif(m_dt, m_dx, m_x_reset, m_rk, m_n_rk, m_sk, m_n_sk);
        auto params = m_params;
        // FIXME
        if (m_num_mixtures > 1) {
            params.pop_back();
        }
        /*
        for (int j = 1; j < m_num_mixtures; ++j) {
            params.pop_back();
        }
        */
        std::vector<double> decoding_params, d_lb, d_ub;
        for (int j = 0; j < mp_sk->stim_des_num(); ++j) {
            decoding_params.push_back(m_decoding_params[j]);
            d_lb.push_back(m_lb[j]);
            d_ub.push_back(m_ub[j]);
        }
        rlif.set_parameters(params);
        rlif.set_maxeval(m_maxeval);
        rlif.set_maxeval_global(m_maxeval_global);
        rlif.set_lower_bounds(d_lb);
        rlif.set_upper_bounds(d_ub);

        // decode each spike train
        size_t data_size = mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            std::vector<std::vector<double>> ss = {(*mp_spike_trains)[i]};
            rlif.set_spike_trains( &ss );
            double minf_dec = HUGE_VAL;
            std::vector<double> optdecparams;
            int trial_num = as_init ? m_ldci_trial_num : m_opt_trial_num;
            for (int repn = 0; repn < trial_num; ++repn) {
                rlif.set_decoding_parameters(random_decoding_parameter());
                rlif.decode();
                double newminf = rlif.get_min_f();
                if (newminf < minf_dec) {
                    minf_dec = newminf;
                    optdecparams = rlif.get_decoding_parameters();
                }
            }
            decoding_result.push_back(optdecparams);
        }

        // make spike train vectors of size n (number of spike trains)
        // consisting of log-likelihood values of all decoded stimuli
        std::vector<std::vector<double>> spike_train_vectors(data_size);
        rlif.set_keep_inf(false); // to get stable log likelihood values
        for (size_t i = 0; i < data_size; ++i) {
            //std::vector<std::vector<double>> ss = {(*mp_spike_trains)[i]};
            //rlif.set_spike_trains( &ss );
            std::vector<double> st_vec;
            for (auto& j : decoding_result) {
                //rlif.set_decoding_parameters(j);
                //st_vec.push_back(-rlif.calc_obj_func_decode());
                double res = -rlif.calc_spike_train_loglik(j, (*mp_spike_trains)[i], 0, 0) / (*mp_spike_trains)[i].size();
                st_vec.push_back(res);
            }
            spike_train_vectors[i] = st_vec;
        }
        rlif.set_keep_inf(true); 

        /*
        for (auto& i : spike_train_vectors) {
            for (auto j : i )
                std::cout << j << "\t";
            std::cout << std::endl;
        }
        */

        // modify
        //for (auto& tmp1 : spike_train_vectors) {
        //    for (auto& tmp2 : tmp1) {
        //        if ( tmp2 < 0) tmp2 = exp(tmp2) - 1;
        //    }
        //}

        int sz = spike_train_vectors.size();
        // augment vectors, combining rows and columns
        //for (int i = 0; i < sz; ++i) {
        //    for (int j = 0; j < sz; ++j) {
        //        spike_train_vectors[i].push_back(spike_train_vectors[j][i]);
        //    }
        //}

        // use clustered spike trains to decode 
        int total_num = m_num_mixtures + 1;
        int data_num = 0;
        for (auto& st : *mp_spike_trains) {
            data_num += st.size() - 1;
        }
        std::vector<std::vector<std::vector<double>>> decoding_result_sum(total_num);
        std::vector<std::vector<std::vector<int>>> decoding_classes;
        std::vector<double> scores;

        std::vector<double> datamat;
        for (auto& i : spike_train_vectors) {
            for (auto j : i) {
                datamat.push_back(j);
            }
        }

        cv::Mat samples( sz, sz, cv::DataType<double>::type, datamat.data() );
        samples.convertTo(samples, CV_32F);
        cv::Mat labels;

        // FIXME k
        for (int k = m_num_mixtures; k < total_num; ++k) {
            m_ldci_params = std::vector<std::vector<std::vector<double>>>(
                    k, std::vector<std::vector<double>>(0));
            set_num_mixtures(k);
            std::vector<std::vector<int>> classes(k);
            //kmeans(spike_train_vectors, k, classes);

            if (m_dm == DM_LL_KMEANS || m_dm == DM_LL_KMEANS_SE) {
                cv::kmeans(samples, k, labels,
                        cv::TermCriteria( cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 100, 0.01),
                        10, cv::KMEANS_PP_CENTERS, cv::noArray());
            } else {
                cv::Ptr<cv::ml::EM> em_model = cv::ml::EM::create();
                em_model->setClustersNumber(k);

                auto emmethod = cv::ml::EM::COV_MAT_SPHERICAL;
                if (m_dm == DM_LL_EM1) {
                    emmethod = cv::ml::EM::COV_MAT_SPHERICAL;
                } else if (m_dm == DM_LL_EM2) {
                    emmethod = cv::ml::EM::COV_MAT_DIAGONAL;
                } else if (m_dm == DM_LL_EM3) {
                    emmethod = cv::ml::EM::COV_MAT_GENERIC;
                }
                em_model->setCovarianceMatrixType(emmethod);
                em_model->setTermCriteria(cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 100, 0.01));
                em_model->trainEM( samples, cv::noArray(), labels, cv::noArray() );
            }


            for (int i = 0; i < labels.rows; ++i) {
                classes[labels.at<int>(i,0)].push_back(i);
            }

            decoding_classes.push_back(classes);

            double score = 0;
            for (int i = 0; i < k; ++i) {
                if (classes[i].size() == 0) {
                    std::cout << "kmeans error; cluster has 0 size" << std::endl;
                    continue;
                }
                /*
                if (classes[i][0] > 9) {
                    continue;
                }
                */
                std::vector<std::vector<double>> ss;
                for (size_t j : classes[i]) {
                    if (j > mp_spike_trains->size()) break;
                    ss.push_back( (*mp_spike_trains)[j] );
                }
                rlif.set_spike_trains(&ss);

                // average the estimates from sigle observations
                std::vector<double> avg_params(4, 0.0);

                for (auto j : classes[i]) {
                    for (int nn = 0; nn < 4; ++nn) {
                        avg_params[nn] += decoding_result[j][nn];
                    }
                }
                for (auto& j : avg_params) {
                    j /= classes[i].size();
                    std::cout << j << std::endl;
                }

                rlif.set_decoding_parameters( avg_params );
                rlif.decode();
                auto tmpdecodeparams = rlif.get_decoding_parameters();
                double tmp_minf = rlif.get_min_f();

                if (as_init) {
                    m_ldci_params[i].push_back(tmpdecodeparams);
                }

                if (m_dm != DM_LL_KMEANS_SE) {
                    std::random_device tmprd;
                    std::mt19937 tmpg(tmprd());
                    for (int tmprep = 0; tmprep < m_opt_trial_num-1; ++tmprep) {
                        if (tmprep >= static_cast<int>(classes[i].size())) {
                            rlif.set_decoding_parameters( random_decoding_parameter() );
                        } else {
                            rlif.set_decoding_parameters( decoding_result[classes[i][tmprep]] );
                        }
                        rlif.decode();
                        double tmp_f =rlif.get_min_f();
                        if (tmp_minf > tmp_f) {
                            tmp_minf = tmp_f;
                            tmpdecodeparams = rlif.get_decoding_parameters();
                        }
                        if (as_init) {
                            m_ldci_params[i].push_back(rlif.get_decoding_parameters());
                        }
                    }
                }
                /*
                for (int tmprep = 0; tmprep < std::ceil(0.49*m_opt_trial_num) - 1; ++tmprep) {
                    rlif.set_decoding_parameters( random_decoding_parameter() );
                    rlif.decode();
                    double tmp_f =rlif.get_min_f();
                    if (tmp_minf > tmp_f) {
                        tmp_minf = tmp_f;
                        tmpdecodeparams = rlif.get_decoding_parameters();
                    }
                }
                */

                decoding_result_sum[k-1].push_back(tmpdecodeparams);
                score += tmp_minf;
            }
            // BIC
            score = score * 2 + mp_sk->stim_des_num() * k * log(data_num);
            scores.push_back(score);
        }

        if (0) {
            std::cout << "Decoding result of each spike train:\n";
            for (auto i : decoding_result) {
                for (auto j : i) {
                    std::cout << "\t" << j;
                }
                std::cout << std::endl;
            }
            for (size_t i = 0; i < decoding_result_sum.size(); ++i) {
                std::cout << "Cluster size: " << i+1 << ", score: " 
                    << scores[i] << "\n";
                std::cout << " -\tclass of index:\n";
                for (auto& cls : decoding_classes[i]) {
                    for (auto v : cls) {
                        std::cout << "\t" << v;
                    }
                    std::cout << std::endl;
                }
                std::cout << " -\tdecoded stimuli:\n";
                for (auto& res : decoding_result_sum[i]) {
                    for (auto v : res) {
                        std::cout << "\t" << v;
                    }
                    std::cout << std::endl;
                }
            }
        }

        // output optim
        int ind = 0;
        double min_bic = scores[0];
        for (size_t i = 1; i < scores.size(); ++i) {
            if (scores[i] < min_bic) {
                ind = i;
                min_bic = scores[i];
            }
        }
        if (as_init) {
            m_single_decoding_result = decoding_result;
            m_ldci_cluster_index = decoding_classes[ind];
            m_ldci_optim_params = decoding_result_sum[m_num_mixtures-1];
        }
        m_decode_cluster_index = decoding_classes[ind];
        m_decode_cluster_params  = decoding_result_sum[m_num_mixtures-1];
    }

    std::pair<bool, double> calc_performance(const std::vector<std::vector<int>>& ind) {
        if (m_dm == DM_MARGINAL || m_dm == DM_EM) {
            double res = 0.0;
            set_decoding_parameters(m_decode_map_params);
            std::vector<double> logs(m_num_mixtures);
            for (size_t i = 0; i < mp_spike_trains->size(); ++i) {
                for (int j = 0; j < m_num_mixtures; ++j) {
                    double logprob = log(m_weights[j]) + calc_spike_train_loglik(m_stimuli[j], (*mp_spike_trains)[i], 0, 0);
                    logs[j] = logprob;
                }
                res += log_sum_exp(logs);
            }
            for (size_t i = 0; i < ind.size(); ++i) {
                int first = m_decode_map_index[ind[i][0]];
                for (size_t j = 1; j < ind[i].size(); ++j) {
                    if (m_decode_map_index[ind[i][j]] != first) {
                        return std::make_pair(false, res);
                    }
                }
                if (i >= 1) {
                    for (size_t k = 0; k < i; ++k) {
                        if (first == m_decode_map_index[ind[k][0]]) {
                            return std::make_pair(false, res);
                        }
                    }
                }
            }
            return std::make_pair(true, res);
        } else if (m_dm == DM_KMEANS || m_dm == DM_LL_KMEANS || 
                m_dm == DM_LL_EM1 || m_dm == DM_LL_EM2 || 
                m_dm == DM_LL_EM3 || m_dm == DM_LL_KMEANS_SE) {
            std::vector<int> index(mp_spike_trains->size());
            for (size_t i = 0; i < m_decode_cluster_index.size(); ++i) {
                for (size_t j = 0; j < m_decode_cluster_index[i].size(); ++j) {
                    index[m_decode_cluster_index[i][j]] = i;
                }
            }
            double res = 0.0;
            for (size_t i = 0; i < mp_spike_trains->size(); ++i) {
                res += calc_spike_train_loglik(
                        m_decode_cluster_params[index[i]],
                        (*mp_spike_trains)[i], 0, 0);
            }
            for (size_t i = 0; i < ind.size(); ++i) {
                int first = index[ind[i][0]];
                for (size_t j = 1; j < ind[i].size(); ++j) {
                    if (index[ind[i][j]] != first) {
                        return std::make_pair(false, res);
                    }
                }
                if (i >= 1) {
                    for (size_t k = 0; k < i; ++k) {
                        if (first == index[ind[k][0]]) {
                            return std::make_pair(false, res);
                        }
                    }
                }
            }
            return std::make_pair(true, res);
        }
        return std::make_pair(false, 0.0);
    }

    std::pair<int, double> calc_optimal_bayes(
            const std::vector<std::vector<double>>& ss,
            const std::vector<std::vector<int>>& ind, 
            const std::vector<std::vector<double>>& stims) {
        auto tmpstim = m_decode_cluster_params;
        m_decode_cluster_params = stims;
        auto res = predict(ss, ind);
        m_decode_cluster_params = tmpstim;
        return res;
    }

    std::pair<int, double> predict(const std::vector<double>& ss) {
        double max_logprob = -HUGE_VAL;
        int ind = 0;
        if (m_dm == DM_MARGINAL || m_dm == DM_EM) {
            std::vector<double> logs(m_num_mixtures);
            set_decoding_parameters(m_decode_map_params);
            for (int i = 0; i < m_num_mixtures; ++i) {
                double logprob = log(m_weights[i]) + calc_spike_train_loglik(m_stimuli[i], ss, 0, 0);
                logs[i] = logprob;
                if (max_logprob < logprob) {
                    ind = i;
                    max_logprob = logprob;
                }
            }
            max_logprob = log_sum_exp(logs);
        } else if (m_dm == DM_KMEANS || m_dm == DM_LL_KMEANS || m_dm == DM_LL_EM1 ||
                   m_dm == DM_LL_EM2 || m_dm == DM_LL_EM3 || m_dm == DM_LL_KMEANS_SE) {
            m_stimuli = m_decode_cluster_params;
            for (int i = 0; i < m_num_mixtures; ++i) {
                double logprob = calc_spike_train_loglik(m_stimuli[i], ss, 0, 0);
                if (max_logprob < logprob) {
                    ind = i;
                    max_logprob = logprob;
                }
            }
        }
        return std::make_pair(ind, max_logprob);
    }

    std::pair<bool, double> predict(const std::vector<std::vector<double>>& ss, const std::vector<std::vector<int>>& ind) {
        double f_val = 0;
        std::vector<int> ind_pred(ss.size());
        for (size_t i = 0; i < ss.size(); ++i) {
            auto res = predict(ss[i]);
            f_val += res.second;
            ind_pred[i] = res.first;
        }
        for (size_t i = 0; i < ind.size(); ++i) {
            int first = ind_pred[ind[i][0]];
            for (size_t j = 1; j < ind[i].size(); ++j) {
                if (ind_pred[ind[i][j]] != first) {
                    return std::make_pair(false, f_val);
                }
            }
            if (i >= 1) {
                for (size_t k = 0; k < i; ++k) {
                    if (first == ind_pred[ind[k][0]]) {
                        return std::make_pair(false, f_val);
                    }
                }
            }
        }
        return std::make_pair(true, f_val);
    }

    void monte_carlo_matrix(std::ostream& of) {
        std::vector<std::vector<double>> decoding_result;
        std::vector<double> init_params = m_decoding_params;
        // use a response model without probability mixtures
        ResponseLIF rlif(m_dt, m_dx, m_x_reset, m_rk, m_n_rk, m_sk, m_n_sk);
        auto params = m_params;
        // FIXME
        if (m_num_mixtures > 1) {
            params.pop_back();
        }
        /*
        for (int j = 1; j < m_num_mixtures; ++j) {
            params.pop_back();
        }
        */
        std::vector<double> decoding_params, d_lb, d_ub;
        for (int j = 0; j < mp_sk->stim_des_num(); ++j) {
            decoding_params.push_back(m_decoding_params[j]);
            d_lb.push_back(m_lb[j]);
            d_ub.push_back(m_ub[j]);
        }
        rlif.set_parameters(params);
        rlif.set_maxeval(m_maxeval);
        rlif.set_maxeval_global(m_maxeval_global);
        rlif.set_lower_bounds(d_lb);
        rlif.set_upper_bounds(d_ub);

        // decode each spike train
        size_t data_size = mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            std::cout << i << std::endl;
            std::vector<std::vector<double>> ss = {(*mp_spike_trains)[i]};
            rlif.set_spike_trains( &ss );
            double minf_dec = HUGE_VAL;
            std::vector<double> optdecparams;
            for (int repn = 0; repn < m_opt_trial_num; ++repn) {
                rlif.set_decoding_parameters(random_decoding_parameter());
                rlif.decode();
                double newminf = rlif.get_min_f();
                if (newminf < minf_dec) {
                    minf_dec = newminf;
                    optdecparams = rlif.get_decoding_parameters();
                }
            }
            decoding_result.push_back(optdecparams);
        }

        // make spike train vectors of size n (number of spike trains)
        // consisting of minus-log-likelihood values of all decoded stimuli
        std::vector<std::vector<double>> spike_train_vectors(data_size);
        rlif.set_keep_inf(false); // to get stable log likelihood values
        for (size_t i = 0; i < data_size; ++i) {
            //std::vector<std::vector<double>> ss = {(*mp_spike_trains)[i]};
            //rlif.set_spike_trains( &ss );
            std::vector<double> st_vec;
            for (auto& j : decoding_result) {
                //rlif.set_decoding_parameters(j);
                //st_vec.push_back(-rlif.calc_obj_func_decode());
                double res = rlif.calc_spike_train_loglik(j, (*mp_spike_trains)[i], 0, 0) / (*mp_spike_trains)[i].size();
                st_vec.push_back(res);
            }
            spike_train_vectors[i] = st_vec;
        }
        rlif.set_keep_inf(true); 

        for (auto& i : spike_train_vectors) {
            for (auto j : i )
                of << j << "\t";
            of << std::endl;
        }
    }

    void print_decode_map(std::ostream& of) {
        // output results
        for (auto i : m_decode_map_params) {
            of << i << "\t";
        }
        of << std::endl;
        for (auto i : m_decode_map_index) {
            of << i << "\t";
        }
        of << std::endl;
    }


    void print_decode_cluster(std::ostream& of) {
        for (auto& res : m_decode_cluster_params) {
            for (auto v : res) {
                of << v << "\t";
            }
        }
        of << std::endl;
        std::vector<int> index(mp_spike_trains->size());
        for (size_t i = 0; i < m_decode_cluster_index.size(); ++i) {
            for (size_t j = 0; j < m_decode_cluster_index[i].size(); ++j) {
                index[m_decode_cluster_index[i][j]] = i;
            }
        }
        for (auto i : index) {
            of << i << "\t";
        }
        of << std::endl;
    }

    void print_decode_result(std::ostream& of) {
        if (m_dm == DM_MARGINAL || m_dm == DM_EM) {
            print_decode_map(of);
        } else if (m_dm == DM_LL_KMEANS || m_dm == DM_LL_EM1 ||
                m_dm == DM_LL_EM2 || m_dm == DM_LL_EM3 || 
                m_dm == DM_KMEANS || m_dm == DM_LL_KMEANS_SE ) {
            print_decode_cluster(of);
        }
    }

    virtual double calc_obj_func() const {
        double r = 0;
        size_t data_size = mp_spike_trains->size();
        if (m_em) {
            for (size_t i = 0; i < data_size; ++i) {
                std::vector<double> vec;
                double sum = 0;
                for (int j = 0; j < m_num_mixtures; ++j) {
                    double tmp = calc_spike_train_loglik( 
                            (*mp_stimuli)[j], (*mp_spike_trains)[i], 0, 0);
                    double p = m_em_cond_probs[i][j];
                    r += p * (log(m_weights[j]) + tmp);
                    double tmp2 = m_weights[j] * exp(tmp);
                    vec.push_back(tmp2);
                    sum += tmp2;
                }
                for (int j = 0; j < m_num_mixtures; ++j) {
                    m_em_cond_probs[i][j] = vec[j] / sum;
                }
            }
        } else {
            for (size_t i = 0; i < data_size; ++i) {
                std::vector<double> logs;
                for (int j = 0; j < m_num_mixtures; ++j) {
                    double tmp = calc_spike_train_loglik( 
                            (*mp_stimuli)[j], (*mp_spike_trains)[i], 0, 0);
                    logs.push_back(log(m_weights[j]) + tmp);
                }
                r += log_sum_exp(logs);
            }
        }
        return -r;
    }

    virtual double calc_obj_func_decode() const {
        double r = 0;
        size_t data_size = mp_spike_trains->size();
        if (m_dm == DM_EM) {
            for (size_t i = 0; i < data_size; ++i) {
                //std::vector<double> vec;
                //double sum = 0;
                for (int j = 0; j < m_num_mixtures; ++j) {
                    double tmp = calc_spike_train_loglik( 
                            m_stimuli[j], (*mp_spike_trains)[i], 0, 0);
                    double p = m_em_cond_probs[i][j];
                    /*
                    r += p * (log(m_weights[j]) + tmp);
                    double tmp2 = m_weights[j] * exp(tmp);
                    vec.push_back(tmp2);
                    sum += tmp2;
                    */
                    r += p * tmp;
                }
                for (int j = 0; j < m_num_mixtures; ++j) {
                    //m_em_cond_probs[i][j] = vec[j] / sum;
                }
            }
        } else {
            for (size_t i = 0; i < data_size; ++i) {
                std::vector<double> logs;
                for (int j = 0; j < m_num_mixtures; ++j) {
                    double tmp = calc_spike_train_loglik( 
                            m_stimuli[j], (*mp_spike_trains)[i], 0, 0);
                    logs.push_back(log(m_weights[j]) + tmp);
                }
                r += log_sum_exp(logs);
            }
        }
        return -r;
    }

    int get_optimal_stimulus_index(
            const std::vector<double>& st, 
            const std::vector<std::vector<double>>& stims, 
            const std::vector<double>& probs) const {
        double max = -HUGE_VAL;
        int mi = 0;
        for (size_t i = 0; i < stims.size(); ++i) {
            double r = calc_spike_train_loglik( stims[i], st, 0, 0 );
            if (m_decoding_prior) {
                r += log(probs[i]);
            }
            if (r > max) {
                max = r;
                mi = i;
            }
        }
        return mi;
    }

    std::vector<double> get_optimal_stimulus(
            const std::vector<double>& st, 
            const std::vector<std::vector<double>>& stims, 
            const std::vector<double>& probs) const {
        int mi = get_optimal_stimulus_index(st, stims, probs);
        return {stims[mi]};
    }

    virtual void calc_uniform_residuals() const {
        std::string filename;
        if (m_lif_method == FK_PDF) {
            filename = this->get_folder()+"/fp_unif.out";
        } else if (m_lif_method == FK_CDF) {
            filename = this->get_folder()+"/fc_unif.out";
        } else if (m_lif_method == VOL_FIRST) {
            filename = this->get_folder()+"/vf_unif.out";
        } else if (m_lif_method == VOL_SECOND) {
            filename = this->get_folder()+"/vs_unif.out";
        }
        size_t data_size = this->mp_spike_trains->size();
        std::vector<double> res;
        for (size_t i = 0; i < data_size; ++i) {
            std::vector<double> st = (*(this->mp_spike_trains))[i];
            auto substim = get_optimal_stimulus(st, *mp_stimuli, m_weights);
            if (m_lif_method == FK_PDF) {
                calc_uniform_residuals_spike_train_FK_PDF(res, substim, st);
            } else if (m_lif_method == FK_CDF) {
                calc_uniform_residuals_spike_train_FK_CDF(res, substim, st);
            } else if (m_lif_method == VOL_FIRST) {
                calc_uniform_residuals_spike_train_VOL_FIRST(res, substim, st);
            } else if (m_lif_method == VOL_SECOND) {
                calc_uniform_residuals_spike_train_VOL_SECOND(res, substim, st);
            }
        }
        std::ofstream of(filename);
        for (auto& i : res) {
            of << i << "\n";
        }
    }

    virtual void print_cdf_pdf_isipdf() const {
        //size_t data_size = mp_spike_trains->size();
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        // only first two ones
        for (size_t i = 0; i < 2; ++i) {
            std::vector<double> st = (*mp_spike_trains)[i];
            auto substim = get_optimal_stimulus(st, *mp_stimuli, m_weights);
            print_cdf_pdf_isipdf_spike_train(i, substim, st);
        }
    }

    virtual void perf_test() {
        std::ofstream of(this->get_folder()+"/decode.out");
        for (auto i : (*mp_spike_trains)) {
            double rate = (i.size() - 1) * 1.0 / (i.back() - i.front());
            int ind = get_optimal_stimulus_index(i, *mp_stimuli, m_weights);
            of << rate << "\t" << ind << "\n";
        }
        //print_cdf_pdf_isipdf();
        calc_uniform_residuals();
    }

    std::vector<double> random_decoding_parameter() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(0, 1);
        return { 1 + dis(gen)*6, 2+dis(gen)*2, dis(gen)*2, 60+dis(gen)*5};
    }

    virtual void set_decoding_parameters(const std::vector<double>& p) {
        m_decoding_params = p;
        set_stimuli_from_params();

        if (m_dm == DM_EM) {
            double s = 0;
            for (int k = 0; k < m_num_mixtures-1; ++k) {
                m_weights[k] = p[ mp_sk->stim_des_num() * m_num_mixtures + k ];
                s += m_weights[k];
            }
            if (s >= 0.99999) s = 0.99999;
            m_weights[m_num_mixtures-1] = 1-s;
        } else {
            // softmax
            std::vector<double> ex(m_num_mixtures, 0);

            for (int k = 0; k < m_num_mixtures-1; ++k) {
                ex[k] = std::exp(p[p.size()-1-k]);
            }
            ex[m_num_mixtures-1] = std::exp(1.0/m_num_mixtures);

            double sum = 0;
            for (int k = 0; k < m_num_mixtures; ++k) {
                sum += ex[k];
            }

            for (int k = 0; k < m_num_mixtures; ++k) {
                m_weights[k] = ex[k] / sum;
            }
        }
    }

    void set_decoding_prior(bool b) {
        m_decoding_prior = b;
    }

    void set_decoding_for_mixture(bool b) {
        m_decoding_for_mixture = b;
    }

    void set_decoding_method(DecodingMethod dm) {
        m_dm = dm;
    }

protected:

    double log_sum_exp(const std::vector<double>& logs) const {
        double log_max = logs[0];
        for (auto& i:logs) {
            if (i > log_max) log_max = i;
        }
        double sum = 0;
        for (auto& i:logs) {
            sum += exp(i - log_max);
        }
        return log_max + log(sum);
    }

    virtual int process_parameters() {
        int i = ResponseLIF::process_parameters();
        double sum = 0;
        for (int k = 0; k < m_num_mixtures-1; ++k) {
            m_weights[k] = m_params[i+k];
            sum += m_params[i+k];
        }
        m_weights[m_num_mixtures-1] = 1-sum;
        if (sum > 1) {
            m_weights = std::vector<double>(m_num_mixtures, 1e-8);
        }
        return i+m_num_mixtures;
    }

    void calc_em_cond_probs(
            const std::vector<std::vector<double>>& stimuli) {
        m_em_cond_probs.clear();
        size_t data_size = mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            std::vector<double> v, probs;
            double s = 0;
            for (int j = 0; j < m_num_mixtures; ++j) {
                double tmp = m_weights[j] 
                    * exp(calc_spike_train_loglik(stimuli[j],
                                                  (*mp_spike_trains)[i], 0, 0));
                v.push_back(tmp);
                s += tmp;
            }
            for (auto val : v) {
                probs.push_back(val / s);
            }
            m_em_cond_probs.push_back(probs);
        }
    }

    bool is_em_converged(double fold, double fnew, 
                         const std::vector<double>& pold, 
                         const std::vector<double>& pnew) {
        if (fnew > fold) {
            return true;
        }
        double t = std::abs( (fold - fnew) / fold );
        if (t > m_ftol) {
            return false;
        } else {
            return true;
        }
        /*
        bool r = true;
        for (size_t i = 0; i < pold.size(); ++i) {
            double t = fabs( (pold[i] - pnew[i]) * 1.0 / pold[i] );
            if ( t > 1e-2) {
                r = false;
                break;
            }
        }
        return r;
        */
    }

    int m_num_mixtures;
    std::vector<double> m_weights;

    bool m_decoding_prior = true;
    bool m_decoding_for_mixture = true;
    bool m_em = false;
    DecodingMethod m_dm = DM_MARGINAL;
    int m_opt_trial_num = 5;
    int m_kmpp_trial_num = 5;
    int m_ldci_trial_num = 5;
    bool m_decoding_params_reuse = false;
    DecodingMethod m_init = INIT_KMPP;

    mutable std::vector<std::vector<double>> m_em_cond_probs;

    std::vector<std::vector<double>> m_decode_cluster_params;
    std::vector<std::vector<int>> m_decode_cluster_index;
    std::vector<std::vector<int>> m_ldci_cluster_index;
    std::vector<std::vector<std::vector<double>>> m_ldci_params;
    std::vector<std::vector<double>> m_ldci_optim_params;
    std::vector<double> m_decode_map_params;
    std::vector<int> m_decode_map_index;
    std::vector<std::vector<double>> m_single_decoding_result;
};


/**
 *  Response-averaging response LIF model
 */
class RespAvgResponseLIF : public ResponseLIF {
public:
    RespAvgResponseLIF(double dt, double dx, double reset, FuncType rk, 
                       int n_rk, FuncType sk, int n_sk, int num_mix)
        : ResponseLIF(dt, dx, reset, rk, n_rk, sk, n_sk), 
          m_num_mixtures(num_mix) {
        m_weights.resize(m_num_mixtures, 0);
    }

    void set_num_mixtures(int n) {
        m_num_mixtures = n;
        m_weights.resize(m_num_mixtures, 0);
    }

    int get_num_mixtures() { return m_num_mixtures; }

    virtual double calc_obj_func() const {
        double r = 0;
        size_t data_size = mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            r += calc_spike_train_loglik((*mp_stimuli)[0], 
                                         (*mp_spike_trains)[i], 0, 0);
        }
        return -r;
    }

    virtual void calc_uniform_residuals() const {
        std::string filename;
        if (m_lif_method == FK_PDF) {
            filename = this->get_folder()+"/fp_unif.out";
        } else if (m_lif_method == FK_CDF) {
            filename = this->get_folder()+"/fc_unif.out";
        } else if (m_lif_method == VOL_FIRST) {
            filename = this->get_folder()+"/vf_unif.out";
        } else if (m_lif_method == VOL_SECOND) {
            filename = this->get_folder()+"/vs_unif.out";
        }
        size_t data_size = this->mp_spike_trains->size();
        std::vector<double> res;
        for (size_t i = 0; i < data_size; ++i) {
            std::vector<double> st = (*(this->mp_spike_trains))[i];
            auto substim = (*mp_stimuli)[0];
            if (m_lif_method == FK_PDF) {
                calc_uniform_residuals_spike_train_FK_PDF(res, substim, st);
            } else if (m_lif_method == FK_CDF) {
                calc_uniform_residuals_spike_train_FK_CDF(res, substim, st);
            } else if (m_lif_method == VOL_FIRST) {
                calc_uniform_residuals_spike_train_VOL_FIRST(res, substim, st);
            } else if (m_lif_method == VOL_SECOND) {
                calc_uniform_residuals_spike_train_VOL_SECOND(res, substim, st);
            }
        }
        std::ofstream of(filename);
        for (auto& i : res) {
            of << i << "\n";
        }
    }

    virtual void perf_test() {
        calc_uniform_residuals();
    }

    // calc sk
    virtual double calc_sk(double t, const std::vector<double>& stim) const {
        double stim_val = 0;
        for (int i = 0; i < m_num_mixtures; ++i) {
            stim_val += m_weights[i] * mp_sk->calc(t, (*mp_stimuli)[i]);
        }
        return stim_val;
    }

protected:

    virtual double calc_drift(double h, double stim, double x) const {
        return m_conductance*(m_x_resting - x) + stim + h;
    }

    virtual double calc_drift(double start, double t, double x, 
                              const std::vector<double>& stim, 
                              const std::vector<double>& st) const {
        double h = mp_rk->calc(start, t, st);
        double stim_val = 0;
        for (int i = 0; i < m_num_mixtures; ++i) {
            stim_val += m_weights[i] * mp_sk->calc(t, (*mp_stimuli)[i]);
        }
        return m_conductance*(m_x_resting - x) + stim_val + h;
    }

    virtual int process_parameters() {
        int i = ResponseLIF::process_parameters();
        double sum = 0;
        for (int k = 0; k < m_num_mixtures-1; ++k) {
            m_weights[k] = m_params[i+k];
            sum += m_params[i+k];
        }
        m_weights[m_num_mixtures-1] = 1-sum;
        if (sum > 1) {
            m_weights = std::vector<double>(m_num_mixtures, 1e-8);
        }
        return i+m_num_mixtures;
    }

    int m_num_mixtures;
    std::vector<double> m_weights;
};


/**
 *  Response-averaging response LIF model
 *
class RespAvgResponseLIF : public ResponseLIF {
public:
    RespAvgResponseLIF(double dt, double dx, double reset, FuncType rk, int n_rk, FuncType sk, int n_sk) : ResponseLIF(dt, dx, reset, rk, n_rk, sk, n_sk) {
    }

    virtual std::vector<double> calc_weighted_stim() const {
        return {m_weight1 * m_stim1 + (1-m_weight1) * m_stim1};
    }

    virtual double calc_obj_func() const {
        double r = 0;
        size_t data_size = this->mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            // TODO {m_stim} is actually substimulus
            r += calc_spike_train_loglik( calc_weighted_stim(), (*(this->mp_spike_trains))[i], 0, 0);
        }
        return -r;
    }

    virtual void calc_uniform_residuals(std::vector<double>& res) const {
        size_t data_size = this->mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            std::vector<int> st = (*(this->mp_spike_trains))[i];
            calc_uniform_residuals_spike_train(res, calc_weighted_stim(), st);
        }
    }

    virtual void perf_test() {
        print_cdf_pdf_isipdf();
    }

    virtual std::vector<double> calc_stim_for_simulation(int a) {
        return calc_weighted_stim();
    }

protected:

    virtual int process_parameters() {
        int i = ResponseLIF::process_parameters();
        m_stim1 = m_params[i];
        ++i;
        m_stim2 = m_params[i];
        ++i;
        m_weight1 = m_params[i];
        ++i;
        return i;
    }
    double m_stim1;
    double m_stim2;
    double m_weight1;
};
*/

} // namespace mass

#endif // LIFModels_H
