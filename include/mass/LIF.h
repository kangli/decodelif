#ifndef MASS_LIF_H
#define MASS_LIF_H

#include <mass/Base.h>
#include <fstream>
#include <cstring>
#include <cmath>
#include <random>

namespace mass {

/**
 * \brief Enum of LIF likelihood methods.
 *
 * The methods to calculate the ISI probability density in LIF models.
 * - FK_PDF: Solving Fokker-Planck equation of PDF(x, t) of voltage x at time t.
 * - FK_CDF: Solving Fokker-Planck euqation of CDF(x, t) of voltage no more than
 *   x at time t.
 * - VOL_FIRST: Solving first-kind Volterra equation to get ISI PDF.
 * - VOL_SECOND: Solving second-kind Volterra equation to get ISI PDF.
 */
enum LIFMethod {
    FK_PDF = 0,
    FK_CDF,
    VOL_FIRST,
    VOL_SECOND,
};

/**
 * \brief Overload << to print enum LIFMethod as string.
 */
std::ostream& operator<<(std::ostream& out, const LIFMethod value){
    const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
    switch(value){
        PROCESS_VAL(FK_PDF);     
        PROCESS_VAL(FK_CDF);     
        PROCESS_VAL(VOL_FIRST);
        PROCESS_VAL(VOL_SECOND);
    }
#undef PROCESS_VAL
    return out << s;
}

/******************************************************************************/
/**
 * \brief The leaky integrate-and-fire model.
 */
template <class T>
class LIF : public Base<T> {

public:

    /**
     * \brief Constructor.
     *
     * \param dt time discretization for numerical differentiation/integration.
     * \param dm voltage discretization.
     * \param reset the reset voltage.
     *
     * For convenience, the fire threshold (of models using constant threshold)
     * is 1 and the lowest voltage for numerical solution of equations is 0. The
     * reset voltage can be any value between 0 and 1.
     */
    LIF(double dt, double dx, double reset)
        : m_dt(dt), m_dx(dx), m_x_reset(reset) {
    }
    ~LIF() {}

    virtual void set_dt(double t) { this->m_dt = t; }
    virtual void set_dx(double x) { this->m_dx = x; }

    virtual double get_conductance() const = 0;

    void set_lif_method(int m) {
        switch (m) {
            case 0 :
                m_lif_method = FK_PDF;
                break;
            case 1:
                m_lif_method = FK_CDF;
                break;
            case 2:
                m_lif_method = VOL_FIRST;
                break;
            case 3:
                m_lif_method = VOL_SECOND;
                break;
            default:
                m_lif_method = FK_CDF;
        }
    }

    void set_lif_method(LIFMethod m) {
        m_lif_method = m;
    }

    /**
     * \brief Get the stimulus given the index. (TODO Really Needed?)
     */
    virtual T calc_stim_for_simulation(int stimindex) {
        return (*(this->mp_stimuli))[stimindex];
    }

    double simulate_one_isi(int stimindex, double dt, double start, const std::vector<double>& st,
            std::mt19937& gen, std::normal_distribution<double>& gauss) {
        T stim = calc_stim_for_simulation(stimindex);
        double x = m_x_reset;
        double t = start + dt;
        double sqrtdt = sqrt(dt);
        // t < 10 to prevent too long isi
        while(x < 1.0 && t < start+1 ) {
            double mu = calc_drift(start, t, x, stim, st);
            double diff = gauss(gen) * sqrtdt * calc_diffusion(t, x);;
            x += mu * dt + diff;
            t += dt;
        }
        return t;
    }

    /**
     * \brief Simulate a spike train given the stimulus.
     *
     * \param stimindex index of stimulus to use 
     * \param dt time discretization for simulation
     * \param len maximal length of spike train 
     * \param num maximal number of spikes
     * \param st vector to store spike times
     */
    void simulate(int stimindex, double dt, double len, size_t num, 
                  std::vector<double>& st) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<> d(0, sqrt(dt));
        T stim = calc_stim_for_simulation(stimindex);
        double x = m_x_reset;
        double t = 0;
        st.push_back(t);
        while (t < len && st.size() < num) {
            while(x < 1.0 && t < len && st.size() < num) {
                double mu = calc_drift(st.back(), t, x, stim, st);
                double diff = d(gen) * calc_diffusion(t, x);;
                x += mu * dt + diff;
                t += dt;
            }
            if ( x >= 1.0 ) {
                x = m_x_reset;
                st.push_back(t);
            }
        }
    }

    /**
     * \brief Simulate a spike train given the stimulus.
     *
     * \param stimindex index of stimulus to use 
     * \param dt time discretization for simulation
     * \param len maximal length of spike train 
     * \param num maximal number of spikes
     * \param x vector to store voltage trajectory
     * \param st vector to store spike times
     */
    void simulate(int stimindex, double dt, double len, size_t num, 
                  std::vector<double>& x, std::vector<double>& st) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<> d(0, sqrt(dt));
        T stim = calc_stim_for_simulation(stimindex);
        x.push_back(m_x_reset);
        double t = 0;
        st.push_back(t);
        while (t < len && st.size() < num) {
            while(x.back() < 1.0 && t < len && st.size() < num) {
                double mu = calc_drift(st.back(), t, x.back(), stim, st);
                double diff = d(gen) * calc_diffusion(t, x.back());;
                x.push_back(x.back() + mu * dt + diff);
                t += dt;
            }
            if ( x.back() >= 1.0 ) {
                x.push_back(m_x_reset);
                st.push_back(t);
            }
        }
    }

    /**
     * \brief Simulate a spike train given the stimulus, and write into files.
     *
     * \param stimindex index of stimulus to use 
     * \param dt time discretization for simulation
     * \param len maximal length of spike train 
     * \param num maximal number of spikes
     * \param x vector to store voltage trajectory
     * \param st vector to store spike times
     * \param simx file name to store voltage trajectory
     * \param sims file name to store spike times
     */
    void simulate(int stimindex, double dt, double len, size_t num, 
                  std::vector<double>& x, std::vector<double>& st, 
                  const std::string simx, const std::string sims, bool outputx = true) {
        simulate(stimindex, dt, len, num, x, st);
        std::ofstream ofx(simx);
        std::ofstream ofs(sims);
        if (outputx) {
            for (auto i : x) {
                ofx << i << "\n";
            }
        }
        for (auto i : st) {
            ofs << i << "\n";
        }
    }

    /**
     * \brief Calculate uniform residuals using Fokker-Planck CDF method.
     */
    void calc_uniform_residuals_spike_train_FK_CDF(
            std::vector<double>& res, 
            const T& stim, 
            const std::vector<double>& st) const {
        for (size_t i = 0; i < st.size()-1; ++i) {
            std::vector<std::vector<double>> cdf_mat;
            solve_time_evolving_cdf(st[i], (st[i+1]-st[i]), 
                                    stim, st, cdf_mat);
            int l = cdf_mat.back().size();
            res.push_back( (cdf_mat.back()[l-1] + cdf_mat.back()[l-2]) / 2.0 );
        }
    }

    /**
     * \brief Calculate uniform residuals using Fokker-Planck PDF method.
     */
    void calc_uniform_residuals_spike_train_FK_PDF(
            std::vector<double>& res, 
            const T& stim, 
            const std::vector<double>& st) const {
        for (size_t i = 0; i < st.size()-1; ++i) {
            std::vector<std::vector<double>> pdf_mat;
            solve_time_evolving_pdf(st[i], (st[i+1]-st[i]), 
                                    stim, st, pdf_mat);
            double sum = 0;
            auto v = pdf_mat.back();
            for (size_t j = 1; j < v.size()-1; ++j) {
                sum += (v[j] + v[j-1])/2.0*m_dx;
            }
            res.push_back(sum);
        }
    }

    /**
     * \brief Calculate uniform residuals using First Volterra method.
     */
    void calc_uniform_residuals_spike_train_VOL_FIRST(
            std::vector<double>& res, 
            const T& stim, 
            const std::vector<double>& st) const {
        for (size_t i = 0; i < st.size()-1; ++i) {
            std::vector<double> p;
            solve_volterra_first(st[i], (st[i+1]-st[i]), stim, st, p);
            double sum = 0;
            for (double v : p) {
                sum += v*m_dt;
            }
            res.push_back(sum);
        }
    }

    /**
     * \brief Calculate uniform residuals using Second Volterra method.
     */
    void calc_uniform_residuals_spike_train_VOL_SECOND(
            std::vector<double>& res, 
            const T& stim, 
            const std::vector<double>& st) const {
        for (size_t i = 0; i < st.size()-1; ++i) {
            std::vector<double> p;
            solve_volterra_second(st[i], (st[i+1]-st[i]), stim, st, p);
            double sum = 0;
            for (double v : p) {
                sum += v*m_dt;
            }
            res.push_back(sum);
        }
    }

    /**
     * \brief Calculate uniform residuals.
     *
     * Uniform residuals are from the ISI CDF using observed data.
     * - z_i = F(x_i).
     * If the model is correct, z_i follows standard uniform distribution.
     */
    virtual void calc_uniform_residuals() const {
        std::string filename;
        if (m_lif_method == FK_PDF) {
            filename = this->get_folder()+"/fp_unif.out";
        } else if (m_lif_method == FK_CDF) {
            filename = this->get_folder()+"/fc_unif.out";
        } else if (m_lif_method == VOL_FIRST) {
            filename = this->get_folder()+"/vf_unif.out";
        } else if (m_lif_method == VOL_SECOND) {
            filename = this->get_folder()+"/vs_unif.out";
        }
        size_t data_size = this->mp_spike_trains->size();
        std::vector<double> res;
        for (size_t i = 0; i < data_size; ++i) {
            T stim = (*(this->mp_stimuli))[i];
            std::vector<double> st = (*(this->mp_spike_trains))[i];
            if (m_lif_method == FK_PDF) {
                calc_uniform_residuals_spike_train_FK_PDF(res, stim, st);
            } else if (m_lif_method == FK_CDF) {
                calc_uniform_residuals_spike_train_FK_CDF(res, stim, st);
            } else if (m_lif_method == VOL_FIRST) {
                calc_uniform_residuals_spike_train_VOL_FIRST(res, stim, st);
            } else if (m_lif_method == VOL_SECOND) {
                calc_uniform_residuals_spike_train_VOL_SECOND(res, stim, st);
            }
        }
        std::ofstream of(filename);
        for (auto& i : res) {
            of << i << "\n";
        }
    }

    /**
     * \brief Print time evolving CDF and PDF (Fokker-Planck methods), and the 
     * ISI distribution PDF.
     */
    virtual void print_cdf_pdf_isipdf() const {
        size_t data_size = this->mp_spike_trains->size();
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = 0; i < data_size; ++i) {
            T stim = (*(this->mp_stimuli))[i];
            std::vector<double> st = (*(this->mp_spike_trains))[i];
            print_cdf_pdf_isipdf_spike_train(i, stim, st);
        }
    }

    virtual void perf_test() {
        //print_cdf_pdf_isipdf();
        calc_uniform_residuals();
    }

    virtual double calc_obj_func_decode() const {
        double r = 0;
        size_t data_size = this->mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            auto stim = (this->m_stimuli)[0];
            double tmp = calc_spike_train_loglik( 
                    stim, (*(this->mp_spike_trains))[i], 0, 0);
            r += tmp;
        }
        return -r;
    }

    virtual double calc_obj_func() const {
        double r = 0;
        size_t data_size = this->mp_spike_trains->size();
        for (size_t i = 0; i < data_size; ++i) {
            // TODO translate mp_stimuli[i] to stimulus
            auto stim = (*(this->mp_stimuli))[i];
            double tmp = calc_spike_train_loglik( 
                    stim, (*(this->mp_spike_trains))[i], 0, 0);
            r += tmp;
        }
        return -r;
    }

    // TODO zero length of st
    virtual double calc_spike_train_loglik(
            const T& stim,
            const std::vector<double>& st,
            double start, double end) const {
        double sum = 0;
        size_t from = 0, to = st.size()-1;
        if (start != 0 || end != 0) {
            get_from_to(st, start, end, from, to);
            // ending boundary
            if (st[to] == end) {
            } else if (st[to] < end && st[to] >= start) {
                if (end-st[to] > m_dt) {
                    double r = solve_time_evolving_cdf_survival(
                            st[to], end-st[to], stim, st);
                    if (r < 1e-300) r = m_keep_inf ? 1e-300 : 1;
                    sum += log(r);
                }
            } else if (st[to] < start) {
                double r = solve_time_evolving_cdf_survival(
                        st[to], end-st[to], stim, st);
                if (r < 1e-300) r = m_keep_inf ? 1e-300 : 1;
                sum += log(r);
            }

            // beginning boundary
#if 0
            if (start - st[from] > m_dt) {
                double r = solve_time_evolving_cdf_survival(
                        st[from], start-st[from], stim, st);
                if (r < 1e-300) r = m_keep_inf ? 1e-300 : 1;
                sum -= log(r);
            }
#endif

            /*
            if (to - from > 0) {
                if (st[from] < start) {
                    if ( ( start - st[from] ) / ( st[from+1] - start) > 10 )
                        from += 1;
                }
            }
            */
        }
#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
        for (size_t i = from; i < to; ++i) {
            double r = 0;
            switch (m_lif_method) {
                case FK_CDF:
                    r = solve_time_evolving_cdf(st[i], (st[i+1]-st[i]),
                                                stim, st);
                    break;
                case FK_PDF:
                    r = solve_time_evolving_pdf(st[i], (st[i+1]-st[i]), 
                                                stim, st);
                    break;
                case VOL_FIRST:
                    r = solve_volterra_first(st[i], (st[i+1]-st[i]), 
                                             stim, st);
                    break;
                case VOL_SECOND:
                    r = solve_volterra_second(st[i], (st[i+1]-st[i]), 
                                              stim, st);
                    break;
            }
            // FIXME
            if (r < 1e-300) r = m_keep_inf ? 1e-300 : 1;
            double lr = log(r);
#ifdef ENABLE_OPENMP
#pragma omp atomic
#endif
            sum += lr;
        }
        return sum;
    }

    virtual double calc_rk(double start, double t, const std::vector<double>& st) const { return 0; }

    virtual double calc_sk(double t, const std::vector<double>& stim) const { return 0; }

    void set_keep_inf(bool b) { m_keep_inf = b; }

protected:

    // get from and to indexes depending on the starting and ending time
    void get_from_to(const std::vector<double>& st, double start, double end,
            size_t& from, size_t& to) const {
        for (size_t i = 0; i < st.size()-1; ++i) {
            if (st[i] > end) {
                to = i-1;
                break;
            }
        }
        for (int i = to; i >= 0; --i) {
            if (st[i] <= start) {
                from = i;
                break;
            }
        }
    }

    virtual double calc_drift(double h, double stim, double x) const = 0;
    virtual double calc_drift(double start, double t, double x, 
                              const T& stim, 
                              const std::vector<double>& st) const = 0;

    virtual double calc_diffusion(double t, double x) const = 0;

    double solve_time_evolving_cdf(double start, double t, const T& stim, 
                                   const std::vector<double>& st) const {
        std::vector<std::vector<double>> cdf_mat;
        return solve_time_evolving_cdf(start, t, stim, st, cdf_mat);
    }

    double solve_time_evolving_cdf_survival(double start, double t, 
            const T& stim, const std::vector<double>& st) const {
        std::vector<std::vector<double>> cdf_mat;
        solve_time_evolving_cdf(start, t, stim, st, cdf_mat);
        int len = t/m_dt;
        return cdf_mat[len].back();
    }

    double solve_time_evolving_pdf(double start, double t, const T& stim, 
                                   const std::vector<double>& st) const {
        std::vector<std::vector<double>> pdf_mat;
        return solve_time_evolving_pdf(start, t, stim, st, pdf_mat);
    }

    double solve_volterra_first(double start, double t, const T& stim, 
                                const std::vector<double>& st) const {
        std::vector<double> p;
        return solve_volterra_first(start, t, stim, st, p);
    }

    double solve_volterra_second(double start, double t, const T& stim, 
                                 const std::vector<double>& st) const {
        std::vector<double> p;
        return solve_volterra_second(start, t, stim, st, p);
    }
    double solve_time_evolving_cdf(
            double start, double t, const T& stim, 
            const std::vector<double>& st, 
            std::vector<std::vector<double>>& cdf_mat) const {
        //if t is too short than m_dt, lower m_dt
        double dt = t/5 < m_dt ? t/5 : m_dt;
        int len = t/dt+1;
        int s = 1/m_dx+1;
        double start0 = start;
        cdf_mat.resize(len, std::vector<double>(1/m_dx+1, 0));
        for (size_t m = m_x_reset/m_dx+1; m < 1/m_dx+1; ++m) {
            cdf_mat[0][m] = 1;
        }
        cdf_mat[0][m_x_reset/m_dx] = 0.5;
        std::vector<double> d(s-2), e(s-3), f(s-3), x(s-2), b(s-2);
        // pre calc rk values
        std::vector<double> rk_vec;
        std::vector<double> sk_vec;
        for (int i = 0; i < len; ++i) {
            rk_vec.push_back(calc_rk(start, start0 + i*dt, st));
            sk_vec.push_back(calc_sk(start0 + i*dt, stim));
        }
        for (int i = 1; i < len; ++i) {
            std::vector<double>& u_p = cdf_mat[i-1];
            double rk_val_i1 = rk_vec[i-1], rk_val_i = rk_vec[i];
            double sk_val_i1 = sk_vec[i-1], sk_val_i = sk_vec[i];
            for (int j = 0; j < s-2; ++j) {
                double sigmai = calc_diffusion(start0 + i*dt, (j+1)*m_dx);
                double mui = calc_drift(rk_val_i, sk_val_i, (j+1)*m_dx);
                double sigmap = calc_diffusion(start0 + (i-1)*dt, (j+1)*m_dx);
                double mup = calc_drift(rk_val_i1, sk_val_i1, (j+1)*m_dx);
                double ai = -mui/2/m_dx;
                double bi = sigmai*sigmai/2/m_dx/m_dx;
                double fp = -mup*(u_p[j+2]-u_p[j])/2/m_dx 
                    + sigmap*sigmap/2*(u_p[j+2]-2*u_p[j+1]+u_p[j])/m_dx/m_dx;
                d[j] = 2.0/dt + 2*bi;
                if (j < s-3) {
                    e[j] = -ai-bi;
                }
                if (j > 0) {
                    f[j-1] = ai-bi;
                }
                b[j] = fp + 2.0/dt*u_p[j+1];
                if (j == s-3) {
                    d[j] = d[j]-ai-bi;
                }
            }
            solve_tridiag_thomas(d, e, f, b, x);
            for (int k = 0; k < s-2; ++k) {
                cdf_mat[i][k+1] = x[k];
                
            }
            cdf_mat[i][s-1] = cdf_mat[i][s-2];
        }
        return (cdf_mat[len-2].back() - cdf_mat[len-1].back()) / dt;
    }

    /*
    double solve_time_evolving_cdf(
            double start, double t, const T& stim, 
            const std::vector<double>& st, 
            std::vector<std::vector<double>>& cdf_mat) const {
        int len = t/m_dt+1;
        int s = 1/m_dx+1;
        double start0 = start;
        cdf_mat.resize(len, std::vector<double>(1/m_dx+1, 0));
        for (size_t m = m_x_reset/m_dx+1; m < 1/m_dx+1; ++m) {
            cdf_mat[0][m] = 1;
        }
        cdf_mat[0][m_x_reset/m_dx] = 0.5;
        std::vector<double> d(s-2), e(s-3), f(s-3), x(s-2), b(s-2);
        for (int i = 1; i < len; ++i) {
            std::vector<double>& u_p = cdf_mat[i-1];
            for (int j = 0; j < s-2; ++j) {
                double sigmai = calc_diffusion(start0 + i*m_dt, (j+1)*m_dx);
                double mui = calc_drift(start, start0 + i*m_dt, (j+1)*m_dx, 
                                        stim, st);
                double sigmap = calc_diffusion(start0 + (i-1)*m_dt, (j+1)*m_dx);
                double mup = calc_drift(start, start0 + (i-1)*m_dt, (j+1)*m_dx, 
                                        stim, st);
                double ai = -mui/2/m_dx;
                double bi = sigmai*sigmai/2/m_dx/m_dx;
                d[j] = 1.0/m_dt + 2*bi;
                if (j < s-3) {
                    e[j] = -ai-bi;
                }
                if (j > 0) {
                    f[j-1] = ai-bi;
                }
                b[j] = 1.0/m_dt*u_p[j+1];
                if (j == s-3) {
                    d[j] = d[j]-ai-bi;
                }
            }
            solve_tridiag_thomas(d, e, f, b, x);
            for (int k = 0; k < s-2; ++k) {
                cdf_mat[i][k+1] = x[k];
                
            }
            cdf_mat[i][s-1] = cdf_mat[i][s-2];
        }
        return (cdf_mat[len-2].back() - cdf_mat[len-1].back()) / m_dt;
    }
    */

    double solve_time_evolving_pdf(
            double start, double t, const T& stim, 
            const std::vector<double>& st, 
            std::vector<std::vector<double>>& pdf_mat) const {
        //if t is too short than m_dt, lower m_dt
        double dt = t/5 < m_dt ? t/5 : m_dt;
        int len = t/dt+1;
        int s = 1/m_dx+1;
        double start0 = start;
        pdf_mat.resize(len, std::vector<double>(1/m_dx+1, 0));
        pdf_mat[0][m_x_reset/m_dx] = 1.0/m_dx;
        std::vector<double> d(s-2), e(s-3), f(s-3), x(s-2), b(s-2);
        // pre calc rk values
        std::vector<double> rk_vec;
        std::vector<double> sk_vec;
        for (int i = 0; i < len; ++i) {
            rk_vec.push_back(calc_rk(start, start0 + i*dt, st));
            sk_vec.push_back(calc_sk(start0 + i*dt, stim));
        }
        for (int i = 1; i < len; ++i) {
            double rk_val_i1 = rk_vec[i-1], rk_val_i = rk_vec[i];
            double sk_val_i1 = sk_vec[i-1], sk_val_i = sk_vec[i];
            std::vector<double>& u_p = pdf_mat[i-1];
            double k = 0;
            for (int j = 0; j < s-2; ++j) {
                double mu_i_jm1 = calc_drift(rk_val_i, sk_val_i, j*m_dx);
                double mu_i_jp1 = calc_drift(rk_val_i, sk_val_i, (j+2)*m_dx);
                double sigma = calc_diffusion(start0 + i*dt, (j+1)*m_dx);
                double ai = - mu_i_jm1 / 4.0 / m_dx - sigma*sigma/4.0/m_dx/m_dx;
                double bi = 1.0 / dt + sigma*sigma/2.0/m_dx/m_dx;
                double ci = mu_i_jp1 / 4.0 / m_dx - sigma*sigma/4.0/m_dx/m_dx;
                double mu_im1_jm1 = calc_drift(rk_val_i1, sk_val_i1, j*m_dx);
                double mu_im1_jp1 = calc_drift(rk_val_i1, sk_val_i1, (j+2)*m_dx);
                double fp = - (mu_im1_jp1*u_p[j+2] - mu_im1_jm1*u_p[j]) / 2.0
                    / m_dx + sigma*sigma/2.0*(u_p[j+2] - u_p[j+1]*2 + u_p[j]) 
                    / m_dx/m_dx;

                d[j] = bi;
                if (j < s-3) {
                    e[j] = ci;
                }
                if (j > 0) {
                    f[j-1] = ai;
                }
                b[j] = fp/2.0 + u_p[j+1] / dt;
                if (j == 0) {
                    k = sigma*sigma/(2.0*mu_i_jm1*m_dx+sigma*sigma);
                    d[j] = d[j] + ai*k;
                }
            }
            solve_tridiag_thomas(d, e, f, b, x);
            for (int k = 0; k < s-2; ++k) {
                pdf_mat[i][k+1] = x[k];
                
            }
            pdf_mat[i][s-1] = 0;
            pdf_mat[i][0] = k * pdf_mat[i][1];
        }
        double sum1 = 0;
        double sum2 = 0;
        for (double p : pdf_mat[len-2]) {
            sum1 += p;
        }
        for (double p : pdf_mat[len-1]) {
            sum2 += p;
        }
        sum1 -= pdf_mat[len-2].front()/2.0;
        sum1 -= pdf_mat[len-2].back()/2.0;
        sum2 -= pdf_mat[len-1].front()/2.0;
        sum2 -= pdf_mat[len-1].back()/2.0;
        sum1 *= m_dx;
        sum2 *= m_dx;
        return (sum1 - sum2) / dt;
    }

    // FIXME to be optimized 
    double solve_volterra_first(double start, double t, const T& stim, 
                                const std::vector<double>& st, 
                                std::vector<double>& p) const {
        p.clear();
        int len = std::round(t/m_dt);
        for (int i = 1; i <= len; ++i) {
            double t1 = start + i * m_dt;
            std::vector<double> itg;
            calc_integral_vector(start, t1, stim, st, itg);
            double sum = 0;
            for (int j = 0; j < i-1; ++j) {
                double t2 = start + j * m_dt;
                double pdf = calc_potential_pdf_without_thres(
                        1, t1, 1, t2, itg, start, stim, st);
                sum +=  pdf * p[j] * m_dt;
            }
            double pdf = calc_potential_pdf_without_thres(
                    1, t1, 1, t1-m_dt, itg, start, stim, st);
            double left = calc_potential_pdf_without_thres(
                    1, t1, m_x_reset, start, itg, start, stim, st);
            p.push_back( (left-sum) / pdf / m_dt );
        }
        return p.back();
    }

    // FIXME to be optimized  profiling
    double solve_volterra_second(double start, double t, const T& stim, 
                                 const std::vector<double>& st,
                                 std::vector<double>& p) const {
        p.clear();
        int len = std::round(t/m_dt);
        for (int i = 1; i <= len; ++i) {
            double t1 = start + i * m_dt;
            std::vector<double> itg;
            calc_integral_vector(start, t1, stim, st, itg);
            double sum = 0;
            for (int j = 0; j < i-1; ++j) {
                double t2 = start + j * m_dt;
                double ins = calc_potential_intensity_without_thres(
                        1, t1, 1, t2, itg, start, stim, st);
                sum +=  2 * ins * p[j] * m_dt;
            }
            double ins = calc_potential_intensity_without_thres(
                    1, t1, 1, t1-m_dt, itg, start, stim, st);
            double left = calc_potential_intensity_without_thres(
                    1, t1, m_x_reset, start, itg, start, stim, st);
            p.push_back( (sum - 2*left) / (1 - 2*ins*m_dt) );
        }
        return p.back();
    }

    void calc_integral_vector(double start, double t, const T& stim, 
                              const std::vector<double>& st, 
                              std::vector<double>& itg) const {
        int len = std::round( (t-start)/m_dt );
        double g = get_conductance();
        for (int i = 1; i <= len; ++i) {
            double u = start + i * m_dt - m_dt/2.0;
            double v = calc_drift(start, u, 0, stim, st) 
                * exp(-g*(t-u)) * m_dt;
            itg.push_back(v);
        }
    }

    double calc_potential_pdf_without_thres(
            double x, double t, double v, double s, 
            const std::vector<double>& itg,
            double start, const T& stim, 
            const std::vector<double>& st) const {
        double var = calc_potential_pdf_variance(t, s, start, stim, st);
        double m = calc_potential_pdf_mean(t, v, s, itg, start, stim, st);
        return 1.0 / sqrt(2*M_PI*var) * exp(- (x-m)*(x-m) / (2*var));
    }

    double calc_potential_intensity_without_thres(
            double x, double t, double v, double s, 
            const std::vector<double>& itg,
            double start, const T& stim, 
            const std::vector<double>& st) const {
        double var = calc_potential_pdf_variance(t, s, start, stim, st);
        double m = calc_potential_pdf_mean(t, v, s, itg, start, stim, st);
        double pdf = 1.0 / sqrt(2*M_PI*var) * exp(- (x-m)*(x-m) / (2*var));
        double g = get_conductance();
        double sig = calc_diffusion(t, 0);
        return 0.5 * pdf * ( g * x - calc_drift(start, t, 0, stim, st)
                - sig*sig / var * (x-m) );
    }

    double calc_potential_pdf_mean(
            double t, double v, double s, const std::vector<double>& itg,
            double start, const T& stim, 
            const std::vector<double>& st) const {
        double sum = 0;
        double g = get_conductance();
        int from = std::round( (s - start) / m_dt );
        int to = std::round( (t - start) / m_dt );
        for (int i = from; i < to; ++i) {
            sum += itg[i];
        }
        return v * exp(-g*(t-s)) + sum;
    }

    double calc_potential_pdf_variance(
            double t, double s,
            double start, const T& stim,
            const std::vector<double>& st) const {
        double sig = calc_diffusion(t, 0);
        double g = get_conductance();
        return sig*sig/2.0/g*(1-exp(-2*g*(t-s)));
    }

    void solve_tridiag_thomas(
            const std::vector<double>& d,
            const std::vector<double>& e,
            const std::vector<double>& f, 
            const std::vector<double>& b, 
            std::vector<double>& x) const  {
        int size = d.size();
        std::vector<double> ep(size-1, 0);
        std::vector<double> bp(size, 0);
        ep[0] = e[0] / d[0];
        for (int i = 1; i < size-1; ++i) {
            ep[i] = e[i] / (d[i] - f[i-1]*ep[i-1]);
        }
        bp[0] = b[0] / d[0];
        for (int i = 1; i < size; ++i) {
            bp[i] = (b[i]-f[i-1]*bp[i-1]) / (d[i]-f[i-1]*ep[i-1]);
        }
        x[size-1] = bp[size-1];
        for (int i = size-2; i >= 0; --i) {
            x[i] = bp[i] - ep[i]*x[i+1];
        }
    }

    void calc_isi_cdf_pdf(const std::vector<std::vector<double>>& cdf,
            std::vector<double>& isi_cdf, std::vector<double>& isi_pdf) const {
        isi_pdf.clear();
        isi_cdf.clear();
        for (auto& i : cdf) {
            double v = i.back();
            isi_cdf.push_back(v);
        }
        for (size_t i = 1; i < isi_cdf.size(); ++i) {
            isi_pdf.push_back((isi_cdf[i-1] - isi_cdf[i]) / m_dt);
        }
    }

    void calc_pdf(const std::vector<std::vector<double>>& cdf,
            std::vector<std::vector<double>>& pdf) const {
        pdf.clear();
        for (auto& i : cdf) {
            std::vector<double> v;
            for (size_t j = 1; j < i.size()-1; ++j) {
                v.push_back((i[j+1] - i[j-1])/m_dx/2.0);
            }
            pdf.push_back(v);
        }
    }

    void calc_cdf(const std::vector<std::vector<double>>& pdf,
            std::vector<std::vector<double>>& cdf) const {
        cdf.clear();
        for (auto& i : pdf) {
            std::vector<double> v;
            v.push_back(0);
            for (size_t j = 1; j < i.size()-1; ++j) {
                v.push_back( v.back() + (i[j] + i[j-1]) / 2.0 * m_dx );
            }
            cdf.push_back(v);
        }
    }

    void print_cdf(const std::vector<std::vector<double>>& cdf, 
                   const std::string outfile) const {
        std::ofstream of(outfile);
        for (auto& i : cdf) {
            for (auto& j : i) {
                of << j << "\t";
            }
            of << "\n";
        }
    }
    void print_pdf(const std::vector<std::vector<double>>& pdf, 
                   const std::string outfile) const {
        std::ofstream of(outfile);
        for (auto& i : pdf) {
            for (auto& j : i) {
                of << j << "\t";
            }
            of << "\n";
        }
    }
    void print_isi_pdf(const std::vector<double>& isi_pdf, 
                       const std::string outfile) const {
        std::ofstream of(outfile);
        for (auto& i : isi_pdf) {
            of << i << "\n";
        }
    }


    void print_cdf_pdf_isipdf_spike_train(
            int j, const T& stim, const std::vector<double>& st) const {
        if (m_lif_method == VOL_SECOND) {
            for (size_t i = 0; i < st.size()-1; ++i) {
                std::vector<double> isi_pdf;
                solve_volterra_second(st[i], /*(st[i+1]-st[i])*/0.2, stim, 
                                      st, isi_pdf);
                print_isi_pdf(isi_pdf, 
                              this->get_folder()+"/vs_isipdf.out_" 
                              + std::to_string(j) + "_" + std::to_string(i));
            }
        } else if (m_lif_method == VOL_FIRST) {
            for (size_t i = 0; i < st.size()-1; ++i) {
                std::vector<double> isi_pdf;
                solve_volterra_first(st[i], 0.2, stim, st, isi_pdf);
                print_isi_pdf(isi_pdf, 
                              this->get_folder()+"/vf_isipdf.out_"
                              + std::to_string(j) + "_" + std::to_string(i));
            }
        } else if (m_lif_method == FK_CDF) {
            for (size_t i = 0; i < st.size()-1; ++i) {
                std::vector<std::vector<double>> cdf_mat, pdf_mat;
                std::vector<double> isi_pdf, isi_cdf;
                solve_time_evolving_cdf(st[i], 0.2, stim, st, cdf_mat);
                calc_isi_cdf_pdf(cdf_mat, isi_cdf, isi_pdf);
                calc_pdf(cdf_mat, pdf_mat);
                print_cdf(cdf_mat, 
                          this->get_folder()+"/fc_cdf.out_"
                          + std::to_string(j) + "_" + std::to_string(i));
                print_pdf(pdf_mat, 
                          this->get_folder()+"/fc_pdf.out_" 
                          + std::to_string(j) + "_" + std::to_string(i));
                print_isi_pdf(isi_pdf, 
                              this->get_folder()+"/fc_isipdf.out_" 
                              + std::to_string(j) + "_" + std::to_string(i));
            }
        } else if (m_lif_method == FK_PDF) {
            for (size_t i = 0; i < st.size()-1; ++i) {
                std::vector<std::vector<double>> cdf_mat, pdf_mat;
                std::vector<double> isi_pdf, isi_cdf;
                solve_time_evolving_pdf(st[i], 0.2, stim, st, pdf_mat);
                calc_cdf(pdf_mat, cdf_mat);
                calc_isi_cdf_pdf(cdf_mat, isi_cdf, isi_pdf);
                print_cdf(cdf_mat, 
                          this->get_folder()+"/fp_cdf.out_" 
                          + std::to_string(j) + "_" + std::to_string(i));
                print_pdf(pdf_mat, 
                          this->get_folder()+"/fp_pdf.out_" 
                          + std::to_string(j) + "_" + std::to_string(i));
                print_isi_pdf(isi_pdf, 
                              this->get_folder()+"/fp_isipdf.out_" 
                              + std::to_string(j) + "_" + std::to_string(i));
            }
        }
    }


    virtual void set_stimuli_from_params() {
        this->m_stimuli.clear();
        for (auto i : this->m_decoding_params) {
            this->m_stimuli.push_back({i});
        }
    }

    double m_dt;
    double m_dx;
    double m_x_reset;

    bool m_keep_inf = true;

    LIFMethod m_lif_method = FK_CDF;

};

} // namespace mass

#endif // LIF_H
