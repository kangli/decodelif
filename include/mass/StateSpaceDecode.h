#ifndef MASS_STATESPACEDECODE_H
#define MASS_STATESPACEDECODE_H

#include <mass/LIFModels.h>
#include <mass/Helper.h>

#include <boost/math/distributions/normal.hpp>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <vector>
#include <set>
#include <cmath>
#include <iostream>
#include <cstring>
#include <memory>
#include <random>

namespace mass {

class StateSpaceDecode {

public:
    /*
     * cs, x_hs, t_hs, ws are used for parallel model when multiple spike trains
     * have different attended stimuli. Vectors for all spike trains.
     *
     * x, t are vectors for all K stimulus components inside mixture 
     * x_h, t_h are vectors for stimulus history, used for beginning boundary
     * likelihood evaluation, to construct the SK_PW stim.
     * eg, x_h = {x1, x2, x3}, t_h = {t1, t2}, then the stimulus is
     *  s(t) = x1, if t < t1;
     *         x2, if t1 <= t < t2;
     *         x3, if t >= t2;
     * t1 and t2 are always boundaries of lattice.
     * Note that the spike train between t1 and t2 is empty:
     *    - x1 -     -  x2 -    - x3 -
     *  .|.|..|...t1.........t2.|..t...|..
     */
    struct Particle {
        int c;                                  //< attended stimulus
        int cp;                                 //< attended stimulus previous step
        std::vector<double> x;                  //< stimulus estimates
        std::vector<double> t;                  //< last updated time of stimulus
        std::vector<double> beta;               //< beta value estimate
        std::vector<double> x_h;                //< history of stimulus
        std::vector<double> t_h;                //< history of time changes
        std::vector<double> all_x;              //< trace of x
        std::vector<std::vector<double>> trans; //< state transition matrix, runifs
        double sigma;
        double w;
        double w_unnorm;
        double logw;
        double apf_g;
        //std::vector<double> ws;
        //std::vector<std::vector<double>> x_hs;  
        //std::vector<std::vector<double>> t_hs;
        //std::vector<int> cs;                    //< attended stimuli
    };

    struct ParticleM {
        ParticleM(int m): x(m), beta(m), x_h(m), all_x(m) {}
        std::vector<double> x;                  //< stimulus estimates
        std::vector<double> beta;               //< beta value estimate
        std::vector<double> t_h;                //< history of time changes
        std::vector<std::vector<double>> x_h;                //< history of stimulus
        std::vector<std::vector<double>> all_x;              //< trace of x
        std::vector<std::vector<double>> trans; //< state transition matrix, runifs
        std::vector<std::vector<double>> prior; //< state prior for each spike train
        double sigma;
        double w;
        double w_unnorm;
        double logw;
        double apf_g;
    };

    enum SimOption {
        SIM_NONE,
        SIM_SPIKE,
        SIM_ATTEND_SPIKE,
        SIM_STIM_SPIKE,
    };

    StateSpaceDecode() : m_d_norm(0, 1), m_d_unif(0, 1) {
        mp_sim_model = new ResponseLIF(0.002, 0.02, 0.4, RK_EXP, 0, SK_INPUT, 0);
        mp_model = new ResponseLIF(0.002, 0.02, 0.4, RK_EXP, 0, SK_PW, 0);
        mp_sim_model->get_stimulus_kernel()->set_properties(&m_stimuli_grid);
        mp_model->set_lif_method(FK_CDF);
    }

    void config(double num_stimuli, double num_spike_train,
            const std::vector<std::vector<double>>& states_tran_p,
            const std::vector<double>& stationary_p,
            const std::vector<double>& stimulus_ou_beta,
            double stimulus_ou_sigma,
            bool nuisance,
            int num_particles,
            bool apf = false, bool ks = false, double discount = 0.95,
            bool geom_mean_apf = false
    ) {
        m_num_stimuli = num_stimuli;
        m_num_spike_trains = num_spike_train;
        m_states_tran_p = states_tran_p;
        m_stationary_p = stationary_p;
        m_stimulus_ou_beta = stimulus_ou_beta;
        m_stimulus_ou_sigma = stimulus_ou_sigma;
        m_ou_nuisanse_known = nuisance;
        m_num_particles = num_particles;
        m_apf = apf;
        m_ks = ks;
        m_geom_mean = geom_mean_apf;
        double tmp = (3*discount -  1) / 2 / discount;
        m_apf_h = sqrt(1-tmp*tmp);
        m_apf_a = sqrt(1-m_apf_h*m_apf_h);
    }

    void set_up(const std::vector<double>& lif_params,
            const std::vector<std::string>& fstimuli,
            const std::string& fattended_stimulus,
            const std::string& fattended_index,
            const std::vector<std::string>& fvoltage,
            const std::vector<std::string>& fspike_train, 
            SimOption sim) {
        mp_sim_model->set_parameters(lif_params);
        mp_model->set_parameters(lif_params);
        if (sim == SIM_NONE) {
            m_stimuli = read_time_series<double>(fstimuli);
            m_attended_stimulus = read_time_series<double>(fattended_stimulus);
            m_attended_index = read_time_series<int>(fattended_index);
            m_spike_train = read_time_series<double>(fspike_train);
        } else if (sim == SIM_SPIKE) {
            m_stimuli = read_time_series<double>(fstimuli);
            m_attended_stimulus = read_time_series<double>(fattended_stimulus);
            m_attended_index = read_time_series<int>(fattended_index);
            simulate_spikes(fvoltage, fspike_train);
        } else if (sim == SIM_ATTEND_SPIKE) {
            m_stimuli = read_time_series<double>(fstimuli);
            calc_attended_stimulus(0, m_spike_train_length);
            print_stimuli(fstimuli, fattended_stimulus, fattended_index);
            simulate_spikes(fvoltage, fspike_train);
        } else if (sim == SIM_STIM_SPIKE) {
            simulate_stimuli(m_spike_train_length);
            calc_attended_stimulus(0, m_spike_train_length);
            print_stimuli(fstimuli, fattended_stimulus, fattended_index);
            simulate_spikes(fvoltage, fspike_train);
        }
    }



    void run_particle_filter() {
        m_x.clear();    m_w.clear();    m_c.clear();
        m_x_lag.clear();    m_w_lag.clear();   
        m_w_FB.clear(); m_sigma.clear(); m_beta.clear();
        m_out_x.clear(); m_out_c.clear(); m_out_ci.clear();
        m_out_sigma.clear(); m_out_beta.clear();
        auto particles = init_particles();
        /*
        if (m_parallel) output_particles_parallel(particles);
        else output_particles(particles, 0, false);
        */
        normalize_particle_weights(particles);
        dump_particles(particles, 0, false);
        int I = std::round((m_end_time - m_start_time) / m_unit_length);
        for (int i = 1; i < I; ++i) {
            double t = m_start_time + (i+0.5) * m_unit_length;
            if (m_apf)
                update_particles_apf(particles, t);
            else 
                update_particles(particles, t);
            normalize_particle_weights(particles);
            dump_particles(particles, i, i==(I-1));
        }
        calc_from_trace();
    }

    void run_particle_filter_marginal() {
        m_x.clear();    m_w.clear();    m_c.clear();
        m_x_lag.clear();    m_w_lag.clear();   
        m_w_FB.clear(); m_sigma.clear(); m_beta.clear();
        m_out_x.clear(); m_out_c.clear(); m_out_ci.clear();
        m_out_sigma.clear(); m_out_beta.clear();
        auto particles = init_particles_marginal();
        /*
        if (m_parallel) output_particles_parallel(particles);
        else output_particles(particles, 0, false);
        */
        normalize_particle_weights(particles);
        dump_particles(particles, 0, false);
        int I = std::round((m_end_time - m_start_time) / m_unit_length);
        for (int i = 1; i < I; ++i) {
            double t = m_start_time + (i+0.5) * m_unit_length;
            if (m_apf)
                update_particles_marginal(particles, t);
            else
                update_particles_marginal_apf(particles, t);
            normalize_particle_weights(particles);
            dump_particles(particles, i, i==(I-1));
        }
        calc_from_trace_marginal();
    }

    std::vector<std::vector<double>>* get_stimuli() {
        return &m_stimuli;
    }

    void calc_from_trace() {
        // filtering
        calc_from_trace_by_input(m_x, m_w, m_out_x, m_out_ci);
        //calc_from_trace_by_c();
        // fixed-lag smoothing
        if (m_smooth_lag > 0)
            calc_from_trace_by_input(m_x_lag, m_w_lag, m_out_x_lag, m_out_ci_lag);
        // FB smoothing
        int sz = m_w.size();
        m_w_FB = std::vector<std::vector<double>>(sz, std::vector<double>(m_num_particles, 0));
        for (int kk = 9; kk < sz; ++kk) {
            m_w_FB[kk] = m_w[kk];
            for (int k = kk-1; k >= kk-9 ; --k) {
                double sigma_mean = 0, sigma_sd = 1;
                if (m_ks) {
                    std::tie(sigma_mean, sigma_sd) = calc_mean_sd_posterior<double>(
                            m_sigma.back(), m_w.back());
                }
                std::vector<std::vector<double>> mat(m_num_particles, std::vector<double>(m_num_particles, 0));
                for (int i = 0; i < m_num_particles; ++i) {
                    for (int j = 0; j < m_num_particles; ++j) {
                        double ci = m_particles[k][i].c;
                        double cj = m_particles[k+1][j].c;
                        double sigmai = m_particles[k][i].sigma;
                        double sigmaj = m_particles[k+1][j].sigma;
                        double dt = m_particles[k+1][j].t[cj] - m_particles[k][i].t[cj];
                        double betai = m_particles[k][i].beta[cj];
                        double betaj = m_particles[k+1][j].beta[cj];
                        double xi = m_particles[k][i].x[cj];
                        double xj = m_particles[k+1][j].x[cj];
                        auto& trans = m_particles[k+1][j].trans;


                        // FIXME omitted to achieve p(xt | xt-1) marginalizing p(xt, thetat | xt-1, thetat-1)
                        /*
                        double prob_c;
                        if (m_num_stimuli == 1) {
                            prob_c = 1;
                        } else if (cj == m_num_stimuli - 1) {
                            prob_c = 1 - trans[ci].back();
                            if (prob_c < 0) prob_c = 0;
                        } else {
                            prob_c = trans[ci][cj] - (cj == 0 ? 0 :trans[ci][cj-1]);
                        }

                        // FIXME store sd of 1
                        double prob_sigma, prob_beta;
                        if (!m_ks) {
                            prob_sigma = dnorm<double>(sigmaj, sigmai, 1) / (1-pnorm<double>(0, sigmai, 1));
                        } else {
                            double tmp_mean = sigmai * m_apf_a + (1-m_apf_a) * sigma_mean;
                            double tmp_sd = m_apf_h * sigma_sd;
                            if (tmp_sd < 1e-100) tmp_sd = 1e-100;
                            prob_sigma = dnorm<double>(sigmaj, tmp_mean, tmp_sd) / (1-pnorm<double>(0, tmp_mean, tmp_sd));
                        }
                        prob_beta = dnorm<double>(betaj, betai, 2*sqrt(dt));
                        */

                        double gmu = (xi - betaj) * exp(-dt) + betaj;
                        double gsd = sqrt(sigmaj*sigmaj/2 * (1-exp(-2*dt)));
                        double prob_x = dnorm<double>(xj, gmu, gsd);

                        //mat[i][j] = prob_c * prob_sigma * prob_beta * prob_x;
                        mat[i][j] = prob_x;

                        if (mat[i][j] < 1e-300) mat[i][j] = 1e-300;
                    }
                }
                std::vector<double> A(m_num_particles);
                for (int j = 0; j < m_num_particles; ++j) {
                    double tmp = 0;
                    for (int i = 0; i < m_num_particles; ++i) {
                        tmp += m_w[k][i] * mat[i][j];
                    }
                    A[j] = tmp;
                    if (A[j] < 1e-300) A[j] = 1e-300;
                }
                for (int i = 0; i < m_num_particles; ++i) {
                    double res = 0;
                    for (int j = 0; j < m_num_particles; ++j) {
                        res += m_w_FB[k+1][j] * mat[i][j] / A[j];
                    }
                    res *= m_w[k][i];
                    m_w_FB[k][i] = res;
                }
                // normalize m_w_FB in k
                double sumw = 0;
                for (double wfb : m_w_FB[k]) {
                    sumw += std::isfinite(wfb) ? wfb : 0;
                }
                if (sumw == 0) sumw = 1e-300;
                for (double& wfb : m_w_FB[k]) wfb /= sumw;
            }
        }
        calc_from_trace_by_input(m_x, m_w_FB, m_out_x_FB, m_out_ci_FB);
    }


    void calc_from_trace_marginal() {
        // filtering
        calc_from_trace_by_input_marginal(m_x, m_w, m_out_x, m_out_ci);
        //calc_from_trace_by_c();
        // fixed-lag smoothing
        if (m_smooth_lag > 0)
            calc_from_trace_by_input_marginal(m_x_lag, m_w_lag, m_out_x_lag, m_out_ci_lag);
        // FB smoothing
        int sz = m_w.size();
        m_w_FB = std::vector<std::vector<double>>(sz, std::vector<double>(m_num_particles, 0));
        m_w_FB[sz-1] = m_w[sz-1];
        for (int k = sz-2; k >= 0 ; --k) {
            double sigma_mean = 0, sigma_sd = 1;
            if (m_ks) {
                std::tie(sigma_mean, sigma_sd) = calc_mean_sd_posterior<double>(
                        m_sigma.back(), m_w.back());
            }
            std::vector<std::vector<double>> mat(m_num_particles, std::vector<double>(m_num_particles, 0));
            for (int i = 0; i < m_num_particles; ++i) {
                for (int j = 0; j < m_num_particles; ++j) {
                    double sigmai = m_particlesm[k][i].sigma;
                    double sigmaj = m_particlesm[k+1][j].sigma;
                    double dt = m_unit_length;

                    /*
                    double prob_sigma = 1;
                    if (!m_ks) {
                        prob_sigma = dnorm<double>(sigmaj, sigmai, 1) / (1-pnorm<double>(0, sigmai, 1));
                    } else {
                        double tmp_mean = sigmai * m_apf_a + (1-m_apf_a) * sigma_mean;
                        double tmp_sd = m_apf_h * sigma_sd;
                        if (tmp_sd < 1e-100) tmp_sd = 1e-100;
                        prob_sigma = dnorm<double>(sigmaj, tmp_mean, tmp_sd) / (1-pnorm<double>(0, tmp_mean, tmp_sd));
                    }
                    */
                    double prob_beta = 1;
                    double prob_x = 1;

                    for (int s = 0; s < m_num_stimuli; ++s) {
                        double betai = m_particlesm[k][i].beta[s];
                        double betaj = m_particlesm[k+1][j].beta[s];
                        double xi = m_particlesm[k][i].x[s];
                        double xj = m_particlesm[k+1][j].x[s];

                        prob_beta *= dnorm<double>(betaj, betai, 2*sqrt(dt));

                        double gmu = (xi - betaj) * exp(-dt) + betaj;
                        double gsd = sqrt(sigmaj*sigmaj/2 * (1-exp(-2*dt)));
                        prob_x *= dnorm<double>(xj, gmu, gsd);
                    }
                    mat[i][j] = prob_x;
                }
            }
            std::vector<double> A(m_num_particles);
            for (int j = 0; j < m_num_particles; ++j) {
                double tmp = 0;
                for (int i = 0; i < m_num_particles; ++i) {
                    tmp += m_w[k][i] * mat[i][j];
                }
                A[j] = tmp;
            }
            for (int i = 0; i < m_num_particles; ++i) {
                double res = 0;
                for (int j = 0; j < m_num_particles; ++j) {
                    res += m_w_FB[k+1][j] * mat[i][j] / A[j];
                }
                res *= m_w[k][i];
                m_w_FB[k][i] = res;
            }
            // normalize m_w_FB in k
            double sumw = 0;
            for (double wfb : m_w_FB[k]) {
                sumw += std::isfinite(wfb) ? wfb : 0;
            }
            if (sumw == 0) sumw = 1e-300;
            for (double& wfb : m_w_FB[k]) wfb /= sumw;
        }
        calc_from_trace_by_input_marginal(m_x, m_w_FB, m_out_x_FB, m_out_ci_FB);
    }

    std::vector<double> calc_RMSD() {
        std::vector<double> rmsd;
        rmsd.push_back(calc_RMSD_by_input(m_out_x));
        rmsd.push_back(calc_RMSD_by_input(m_out_x_lag));
        rmsd.push_back(calc_RMSD_by_input(m_out_x_FB));
        return rmsd;
    }

    double calc_RMSD_by_input(const std::vector<double>& out_x) {
        double s = 0, r = 0;
        int starti = m_start_time / m_stimuli_grid;
        int uniti = m_unit_length / m_stimuli_grid;
        for (size_t i = 0; i < out_x.size(); ++i) {
            for (int j = 0; j < uniti; ++j) {
                int index = starti + uniti*i + j;
                double tmp = out_x[i] - m_attended_stimulus[index];
                s += tmp * tmp;
                r += m_attended_stimulus[index] * m_attended_stimulus[index];
            }
        }
        //double norm = sqrt(r / uniti / out_x.size());
        return sqrt(s / uniti / out_x.size());
    }

    // FIXME not used
    double calc_state_pred_accuracy() {
        int starti = m_start_time / m_unit_length;
        int right = 0;
        for (size_t i = 0; i < m_out_c.size(); ++i) {
            if (m_out_c[i] == m_attended_index[starti+i]) {
                right++;
            }
        }
        return right * 1.0 / m_out_c.size();
    }

    void set_known_nuisance(bool b) {
        m_ou_nuisanse_known = b;
    }

    void set_smoothing_lag(int k) { m_smooth_lag = k; }

    void print_result(
            const std::string& fc, 
            const std::string& fx,
            const std::string& fci,
            const std::string& fsigma, 
            const std::string& fbeta,
            const std::string& fc_trace, 
            const std::string& fx_trace, 
            const std::string& fw_trace,
            const std::string& fsigma_trace,
            const std::string& ftrans_trace,
            const std::string& fx_lag,
            const std::string& fci_lag,
            const std::string& fx_FB,
            const std::string& fci_FB,
            const std::string& feffn,
            bool marginal = false
            ) {
        std::ofstream ofc(fc);
        std::ofstream ofx(fx);
        std::ofstream ofci(fci);
        std::ofstream ofsigma(fsigma);
        std::ofstream ofbeta(fbeta);
        std::ofstream ofx_trace(fx_trace);
        std::ofstream ofsigma_trace(fsigma_trace);
        std::ofstream oftrans_trace(ftrans_trace);
        std::ofstream ofc_trace(fc_trace);
        std::ofstream ofw_trace(fw_trace);
        std::ofstream ofx_lag(fx_lag);
        std::ofstream ofci_lag(fci_lag);
        std::ofstream ofx_FB(fx_FB);
        std::ofstream ofci_FB(fci_FB);
        std::ofstream ofeffn(feffn);
        for (auto i : m_out_c) {
            ofc << i << "\n";
        }
        for (auto i : m_out_x) {
            ofx << i << "\n";
        }
        for (auto i : m_out_ci) {
            ofci << i << "\n";
        }
        for (auto i : m_out_beta) {
            ofbeta << i << "\n";
        }
        for (auto i : m_out_x_lag) {
            ofx_lag << i << "\n";
        }
        for (auto i : m_out_ci_lag) {
            ofci_lag << i << "\n";
        }
        for (auto i : m_out_x_FB) {
            ofx_FB << i << "\n";
        }
        for (auto i : m_out_ci_FB) {
            ofci_FB << i << "\n";
        }
        // for marginal, print all stimuli inside one particle
        if (marginal) {
            for (size_t i = 0; i < m_x.size(); ++i) {
                for (int j = 0; j < m_num_particles; ++j) {
                    for (int k = 0; k < m_num_stimuli; ++k) {
                        int id = j*m_num_stimuli+k;
                        ofx_trace << m_x[i][id] << "\t" << m_x_lag[i][id] << "\t";
                    }
                    ofw_trace << m_w[i][j] << "\t" << m_w_lag[i][j] << "\t" << m_w_FB[i][j] << "\t";
                    ofsigma_trace << m_sigma[i][j] << "\t";
                }
                ofsigma_trace << std::endl; ofx_trace << std::endl; ofc_trace << std::endl; ofw_trace << std::endl;
            }
            for (auto& ps : m_particlesm) {
                for (auto & p : ps) {
                    for (auto& row : p.trans) {
                        for (auto & v : row) {
                            oftrans_trace << v << "\t";
                        }
                    }
                }
                oftrans_trace << "\n";
            }
        } else {
            for (size_t i = 0; i < m_x.size(); ++i) {
                for (int j = 0; j < m_num_particles; ++j) {
                    ofx_trace << m_x[i][j] << "\t" << m_x_lag[i][j] << "\t";
                    ofw_trace << m_w[i][j] << "\t" << m_w_lag[i][j] << "\t" << m_w_FB[i][j] << "\t";
                    ofsigma_trace << m_sigma[i][j] << "\t";
                }
                ofsigma_trace << std::endl; ofx_trace << std::endl; ofc_trace << std::endl; ofw_trace << std::endl;
            }
            for (auto& ps : m_particles) {
                for (auto & p : ps) {
                    for (auto& row : p.trans) {
                        for (auto & v : row) {
                            oftrans_trace << v << "\t";
                        }
                    }
                }
                oftrans_trace << "\n";
            }
        }
        calc_effective_sample_size();
        for (auto i : m_effn) {
            ofeffn << i << "\n";
        }

    }

    void integrate_results_from_parallel(
            const std::vector<std::string>& fx,
            const std::vector<std::string>& fi,
            const std::string& fparallel) {
        int starti = m_start_time / m_unit_length;
        std::vector<std::vector<double>> is = read_time_series<double>(fi);
        std::vector<std::vector<double>> xs = read_time_series<double>(fx);
        int length = xs[0].size();
        std::vector<std::vector<double>> vs(length, 
                std::vector<double>(m_num_stimuli, 0));
        std::vector<std::vector<int>> counts(length,
                std::vector<int>(m_num_stimuli, 0));
        for (int i = 0; i < length; ++i) {
            for (size_t j = 0; j < xs.size(); ++j) {
                int c = is[j][starti+i];
                vs[i][c] += xs[j][i];
                counts[i][c] += 1;
            }
            for (int j = 0; j < m_num_stimuli; ++j) {
                vs[i][j] /= counts[i][j];
            }
        }
        std::ofstream ofparallel(fparallel);
        for (auto& i : vs) {
            for (auto j : i) {
                ofparallel << j << "\t";
            }
            ofparallel << "\n";
        }
    }

    void integrate_results_from_parallel_cluster(
            const std::vector<std::string>& fx,
            const std::string& fparallel,
            const std::string& method) {
        std::vector<std::vector<double>> xs = read_time_series<double>(fx);
        int length = xs[0].size();
        std::vector<std::vector<double>> result(length, std::vector<double>());
        for (int i = 0; i < length; ++i) {
            std::vector<double> v;
            for (size_t j = 0; j < xs.size(); ++j) {
                v.push_back(xs[j][i]);
            }
            std::vector<std::vector<size_t>> c;
            if (method == "kmeans") {
                mass::kmeans<double>(v, 2, c);
            } else if (method == "kmedoids") {
                mass::kmedoids<double>(v, 2, c);
            } else {
                std::cout << "error: unknown cluster method!" << std::endl;
            }
            for (auto& one_c : c) {
                std::vector<double> r;
                for (auto index : one_c) {
                    r.push_back(v[index]);
                }
                result[i].push_back(mass::calc_medoid<double>(r));
                result[i].push_back(mass::calc_median<double>(r));
                result[i].push_back(one_c.size());
            }
        }
        std::ofstream ofparallel(fparallel);
        for (auto& i : result) {
            for (auto j : i) {
                ofparallel << j << "\t";
            }
            ofparallel << "\n";
        }
    }

public:
    /*
    void test_multinormial() {
        std::mt19937 gen(111);
        auto trans = init_states_trans_p(gen);

        std::cout << "trans\n";
        for (auto i : trans) {
            for (auto j : i) std::cout << j << " ";
            std::cout << "\n";
        }

        std::cout << "prop\n";
        for (int i = 0; i < 10; ++i) {
            propagate_states_trans_p(trans, gen);
            for (auto i : trans) {
                for (auto j : i) std::cout << j << " ";
                std::cout << "\n";
            }
        }

        std::cout << "stat\n";
        for (int i = 0; i < 10; ++i) {
            std::cout << sample_from_trans(trans[1], gen) << "\n";
        }
    }
    */


private:

    std::vector<Particle> init_particles() {
        std::random_device rd;
        std::mt19937 gen(rd());
        // gsl random engine
        const gsl_rng_type * gslt;
        gsl_rng* gslr;
        gsl_rng_env_setup();
        gslt = gsl_rng_default;
        gslr = gsl_rng_alloc (gslt);
        std::uniform_real_distribution<> dis(50, 200);
        std::uniform_real_distribution<> dis_sigma(1, 40);
        std::vector<Particle> particles;
        for (int i = 0; i < m_num_particles; ++i) {
            Particle p;
            //std::vector<double> mp(m_num_stimuli-1, 1.0/m_num_stimuli);
            //p.c = sample_multinomial(mp);
            p.trans = init_states_trans_p(gslr);
            p.c = sample_from_trans(p.trans[i%m_num_stimuli], gen);
            p.cp = p.c;
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x.push_back(dis(gen));
                p.t.push_back(m_start_time + 0.5*m_unit_length);
            }
            if (m_ou_nuisanse_known) {
                p.beta = m_stimulus_ou_beta;
                p.sigma = m_stimulus_ou_sigma;
            } else {
                p.beta = p.x;
                p.sigma = dis_sigma(gen);
            }
            double logposterior = 0;
            for (auto& one_spike_train : m_spike_train) {
                logposterior += mp_model->calc_spike_train_loglik(
                        {p.x[p.c]}, one_spike_train, m_start_time, 
                        m_start_time+m_unit_length);
            }
            p.w = exp(logposterior);
            p.logw = logposterior;
            p.x_h.push_back(p.x[p.c]);
            if (m_smooth_lag > 0)
                p.all_x.push_back(p.x[p.c]);
            p.t_h.push_back(m_start_time+m_unit_length);
            particles.push_back(p);
        }
        return particles;
    }

    void update_particles(std::vector<Particle>& particles, double t) {
        resample_particles(particles);
        std::random_device rd;
        std::mt19937 gen(rd());
        // gsl random engine
        const gsl_rng_type * gslt;
        gsl_rng* gslr;
        gsl_rng_env_setup();
        gslt = gsl_rng_default;
        gslr = gsl_rng_alloc (gslt);
        for (auto& p : particles) {
            //p.c = sample_multinomial(m_states_tran_p[p.c]);
            propagate_states_trans_p(p.trans, gslr);
            p.cp = p.c;
            p.c = sample_from_trans(p.trans[p.c], gen);
            if (!m_ou_nuisanse_known) {
                double tmp = p.sigma;
                p.sigma = tmp + m_d_norm(gen);
                while (p.sigma <= 0) 
                    p.sigma = tmp + m_d_norm(gen);
                p.beta[p.c] += m_d_norm(gen) * 2 * sqrt(t - p.t[p.c]);
            }
            p.x[p.c] = sample_next_ou(p.x[p.c], p.beta[p.c],
                    p.sigma, t - p.t[p.c]);
            if (m_smooth_lag > 0)
                p.all_x.push_back(p.x[p.c]);
            p.t[p.c] = t;
            double logposterior = 0;
            std::vector<double> stim = cont_two_vectors<double>(p.t_h, p.x_h);
            stim.push_back(p.x[p.c]);
            for (auto& one_spike_train : m_spike_train) {
                logposterior += mp_model->calc_spike_train_loglik(stim,
                        one_spike_train, t - 0.5*m_unit_length, 
                        t + 0.5*m_unit_length);
            }
            p.w *= exp(logposterior);
            // prevent Overflow
            if (p.w < 1e-305) p.w = 1e-305;
            if (p.w > 1e305) p.w = 1e305;
            p.logw += logposterior;
            p.logw -= log(p.apf_g);
            if (!is_any_spike_train_empty(m_spike_train,
                        t - 0.5*m_unit_length,
                        t + 0.5*m_unit_length)) {
                p.x_h.clear();
                p.t_h.clear();
            }
            p.x_h.push_back(p.x[p.c]);
            p.t_h.push_back(t + 0.5*m_unit_length);
        }
        gsl_rng_free(gslr);
    }

    void update_particles_apf(std::vector<Particle>& particles, double t) {
        std::random_device rd;
        std::mt19937 gen(rd());
        double sigma_mean, sigma_sd;
        // gsl random engine
        const gsl_rng_type * gslt;
        gsl_rng* gslr;
        gsl_rng_env_setup();
        gslt = gsl_rng_default;
        gslr = gsl_rng_alloc (gslt);
        if (m_geom_mean) {
            // geometric mean of weights
            auto geom_w = m_w.back();
            for (auto& v : geom_w) {
                v = pow(v, 1.0/m_num_spike_trains);
            }
            std::tie(sigma_mean, sigma_sd) = calc_mean_sd_posterior<double>(
                    m_sigma.back(), geom_w);
        } else {
            std::tie(sigma_mean, sigma_sd) = calc_mean_sd_posterior<double>(
                    m_sigma.back(), m_w.back());
        }

        // update c
        for (auto& p : particles) {
            // propagate state transition p
            propagate_states_trans_p(p.trans, gslr);
            p.cp = p.c;
            //p.c = sample_multinomial(m_states_tran_p[p.c]);
            p.c = sample_from_trans(p.trans[p.c], gen);
        }
        if (m_apf_expect) {
            // calc E(w|x)
            for (auto& p : particles) {
                std::vector<double> stim = cont_two_vectors<double>(p.t_h, p.x_h);
                stim.push_back(0);
                double posterior = 0;

                // calc E(w|x)
                for (int kk = 0; kk < m_apf_expect_num; ++kk) {
                    stim.back() = sample_next_ou(p.x[p.c], p.beta[p.c], p.sigma, t-p.t[p.c]);
                    double tmppost = 0;
                    for (auto& one_spike_train : m_spike_train) {
                        tmppost += mp_model->calc_spike_train_loglik(stim,
                                one_spike_train, t - 0.5*m_unit_length, 
                                t + 0.5*m_unit_length);
                    }
                    posterior += exp(tmppost) / m_apf_expect_num;
                }

                double tmp = m_geom_mean ? pow(posterior, 1/m_num_spike_trains) : posterior;
                p.w *= tmp;
                // prevent Overflow
                if (p.w < 1e-305) p.w = 1e-305;
                if (p.w > 1e305) p.w = 1e305;
                if (tmp < 1e-305) tmp = 1e-305;
                if (tmp > 1e305) tmp = 1e305;
                p.apf_g = tmp;
            }
        } else {
            // calc E(x) 
            for (auto& p : particles) {
                std::vector<double> stim = cont_two_vectors<double>(p.t_h, p.x_h);
                // expectation of x from OU
                //double betadt = p.beta[p.c]*m_unit_length;
                //stim.push_back((1-m_unit_length)*xx+betadt);
                double xx = p.x[p.c];
                stim.push_back( (xx-p.beta[p.c]) * exp(p.t[p.c]-t) + p.beta[p.c] );
                double logposterior = 0;
                for (auto& one_spike_train : m_spike_train) {
                    logposterior += mp_model->calc_spike_train_loglik(stim,
                            one_spike_train, t - 0.5*m_unit_length, 
                            t + 0.5*m_unit_length);
                }
                double tmp = m_geom_mean ? exp(logposterior/m_num_spike_trains) : exp(logposterior);
                p.w *= tmp;
                // prevent Overflow
                if (p.w < 1e-305) p.w = 1e-305;
                if (p.w > 1e305) p.w = 1e305;
                if (tmp < 1e-305) tmp = 1e-305;
                if (tmp > 1e305) tmp = 1e305;
                p.apf_g = tmp;
            }
        }
        normalize_particle_weights(particles);
        resample_particles(particles);
        for (auto& p : particles) {
            if (!m_ou_nuisanse_known) {
                // update par
                if (!m_ks) {
                    double tmp = p.sigma;
                    p.sigma = tmp + m_d_norm(gen);
                    while (p.sigma <= 0) 
                        p.sigma = tmp + m_d_norm(gen);
                    p.beta[p.c] += m_d_norm(gen) * 2 * sqrt(t - p.t[p.c]);
                } else {
                    // update par using ks
                    double psigma = p.sigma;
                    p.sigma = m_d_norm(gen) * m_apf_h * sigma_sd + p.sigma * m_apf_a + (1-m_apf_a) * sigma_mean;
                    while (p.sigma <= 0) {
                        p.sigma = m_d_norm(gen) * m_apf_h * sigma_sd + psigma * m_apf_a + (1-m_apf_a) * sigma_mean;
                    }
                    p.beta[p.c] += m_d_norm(gen) * 2 * sqrt(t - p.t[p.c]);
                }
            }
            p.x[p.c] = sample_next_ou(p.x[p.c], p.beta[p.c],
                    p.sigma, t - p.t[p.c]);
            if (m_smooth_lag > 0)
                p.all_x.push_back(p.x[p.c]);
            p.t[p.c] = t;
            double logposterior = 0;
            std::vector<double> stim = cont_two_vectors<double>(p.t_h, p.x_h);
            stim.push_back(p.x[p.c]);
            for (auto& one_spike_train : m_spike_train) {
                logposterior += mp_model->calc_spike_train_loglik(stim,
                        one_spike_train, t - 0.5*m_unit_length, 
                        t + 0.5*m_unit_length);
            }
            p.w *= exp(logposterior);
            p.w /= p.apf_g;
            // prevent Overflow
            if (p.w < 1e-305) p.w = 1e-305;
            if (p.w > 1e305) p.w = 1e305;
            p.logw += logposterior;
            if (!is_any_spike_train_empty(m_spike_train,
                        t - 0.5*m_unit_length,
                        t + 0.5*m_unit_length)) {
                p.x_h.clear();
                p.t_h.clear();
            }
            p.x_h.push_back(p.x[p.c]);
            p.t_h.push_back(t + 0.5*m_unit_length);
        }
        gsl_rng_free(gslr);
    }

    // particle filters using marginal posterior of states
    // in marginal pf, m_x_out = {x_t_1, x_t_2, x_t+1_1, x_t+1_2, ...}
    // and m_x = { {x_p_1, x_p_2, x_p+1_1, x_p+1_2, ...}_t, {}_t+1, ...  }
    std::vector<ParticleM> init_particles_marginal() {
        std::random_device rd;
        std::mt19937 gen(rd());
        // gsl random engine
        const gsl_rng_type * gslt;
        gsl_rng* gslr;
        gsl_rng_env_setup();
        gslt = gsl_rng_default;
        gslr = gsl_rng_alloc (gslt);
        std::uniform_real_distribution<> dis(50, 200);
        std::uniform_real_distribution<> dis_sigma(1, 40);
        std::vector<ParticleM> particles;
        std::vector<double> logs(m_num_stimuli);
        for (int i = 0; i < m_num_particles; ++i) {
            ParticleM p(m_num_stimuli);
            p.trans = init_states_trans_p(gslr);
            p.prior = std::vector<std::vector<double>>(m_num_spike_trains, std::vector<double>(m_num_stimuli, 1.0/m_num_stimuli));
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x[j] = dis(gen);
            }
            if (m_ou_nuisanse_known) {
                p.beta = m_stimulus_ou_beta;
                p.sigma = m_stimulus_ou_sigma;
            } else {
                p.beta = p.x;
                p.sigma = dis_sigma(gen);
            }
            double logposterior = 0;
            for (int i_st = 0; i_st < m_num_spike_trains; ++i_st) {
                auto& one_spike_train = m_spike_train[i_st];
                // calc prob for each stimulus using prior and trans
                std::vector<double> probs(m_num_stimuli, 0);
                for (int k = 0; k < m_num_stimuli; ++k) {
                    /*
                    auto row = p.trans[k];
                    for (int j = 1; j < m_num_stimuli - 1; ++j) {
                        row[j] = row[j] - p.trans[k][j-1];
                    }
                    row.push_back(1-p.trans[k].back());
                    */
                    for (int j = 0; j < m_num_stimuli; ++j) {
                        probs[j] += p.prior[i_st][k] * p.trans[k][j];
                    }
                }
                for (int j = 0; j < m_num_stimuli; ++j) {
                    double llk = mp_model->calc_spike_train_loglik(
                            {p.x[j]}, one_spike_train, m_start_time, 
                            m_start_time+m_unit_length);
                    logs[j] = log(probs[j]) + llk;
                }
                logposterior += log_sum_exp(logs);
                // update prior for next time
                double sump = 0;
                std::vector<double> pp = logs;
                for (auto& v : pp) {
                    v = exp(v);
                    sump += v;
                }
                for (int j = 0; j < m_num_stimuli; ++j) {
                    p.prior[i_st][j] = pp[j] / sump;
                }
            }
            p.w = exp(logposterior);
            p.logw = logposterior;
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x_h[j].push_back(p.x[j]);
                if (m_smooth_lag > 0)
                    p.all_x[j].push_back(p.x[j]);
            }
            p.t_h.push_back(m_start_time+m_unit_length);
            particles.push_back(p);
        }
        return particles;
    }


    void update_particles_marginal(std::vector<ParticleM>& particles, double t) {
        resample_particles(particles);
        std::random_device rd;
        std::mt19937 gen(rd());
        std::vector<double> logs(m_num_stimuli);
        // gsl random engine
        const gsl_rng_type * gslt;
        gsl_rng* gslr;
        gsl_rng_env_setup();
        gslt = gsl_rng_default;
        gslr = gsl_rng_alloc (gslt);
        for (auto& p : particles) {
            if (!m_ou_nuisanse_known) {
                propagate_states_trans_p(p.trans, gslr);
                double tmp = p.sigma;
                p.sigma = tmp + m_d_norm(gen);
                while (p.sigma <= 0) 
                    p.sigma = tmp + m_d_norm(gen);
                for (int j = 0; j < m_num_stimuli; ++j) {
                    p.beta[j] += m_d_norm(gen) * 2 * sqrt(m_unit_length);
                }
            }
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x[j] = sample_next_ou(p.x[j], p.beta[j], p.sigma, m_unit_length);
            }
            if (m_smooth_lag > 0) {
                for (int j = 0; j < m_num_stimuli; ++j) {
                    p.all_x[j].push_back(p.x[j]);
                }
            }
            double logposterior = 0;;
            for (int i_st = 0; i_st < m_num_spike_trains; ++i_st) {
                auto& one_spike_train = m_spike_train[i_st];
                // calc prob for each stimulus using prior and trans
                std::vector<double> probs(m_num_stimuli, 0);
                for (int i = 0; i < m_num_stimuli; ++i) {
                    /*
                    auto row = p.trans[i];
                    for (int j = 1; j < m_num_stimuli - 1; ++j) {
                        row[j] = row[j] - p.trans[i][j-1];
                    }
                    row.push_back(1-p.trans[i].back());
                    */
                    for (int j = 0; j < m_num_stimuli; ++j) {
                        probs[j] += p.prior[i_st][i] * p.trans[i][j];
                    }
                }
                for (int j = 0; j < m_num_stimuli; ++j) {
                    std::vector<double> stim = cont_two_vectors<double>(p.t_h, p.x_h[j]);
                    stim.push_back(p.x[j]);
                    double llk = mp_model->calc_spike_train_loglik(stim,
                            one_spike_train, t - 0.5*m_unit_length, 
                            t + 0.5*m_unit_length);
                    logs[j] = log(probs[j]) + llk;
                }
                logposterior += log_sum_exp(logs);
                // update prior for next time
                double sump = 0;
                std::vector<double> pp = logs;
                for (auto& v : pp) {
                    v = exp(v);
                    sump += v;
                }
                for (int j = 0; j < m_num_stimuli; ++j) {
                    p.prior[i_st][j] = pp[j] / sump;
                }
            }
            p.w *= exp(logposterior);
            // prevent Overflow
            if (p.w < 1e-305) p.w = 1e-305;
            if (p.w > 1e305) p.w = 1e305;
            p.logw += logposterior;
            p.logw -= log(p.apf_g);
            if (!is_any_spike_train_empty(m_spike_train,
                        t - 0.5*m_unit_length,
                        t + 0.5*m_unit_length)) {
                for (auto& xh : p.x_h) {
                    xh.clear();
                }
                p.t_h.clear();
            }
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x_h[j].push_back(p.x[j]);
            }
            p.t_h.push_back(t + 0.5*m_unit_length);
        }
        gsl_rng_free(gslr);
    }

    void update_particles_marginal_apf(std::vector<ParticleM>& particles, double t) {
        std::random_device rd;
        std::mt19937 gen(rd());
        double sigma_mean, sigma_sd;
        // gsl random engine
        const gsl_rng_type * gslt;
        gsl_rng* gslr;
        gsl_rng_env_setup();
        gslt = gsl_rng_default;
        gslr = gsl_rng_alloc (gslt);
        if (m_geom_mean) {
            // geometric mean of weights
            auto geom_w = m_w.back();
            for (auto& v : geom_w) {
                v = pow(v, 1.0/m_num_spike_trains);
            }
            std::tie(sigma_mean, sigma_sd) = calc_mean_sd_posterior<double>(
                    m_sigma.back(), geom_w);
        } else {
            std::tie(sigma_mean, sigma_sd) = calc_mean_sd_posterior<double>(
                    m_sigma.back(), m_w.back());
        }
        std::vector<double> logs(m_num_stimuli);
        // calc E(x) 
        for (auto& p : particles) {
            auto trans = p.trans;
            propagate_states_trans_p(trans, gslr);
            // expectation of x from OU
            double logposterior = 0;
            for (int i_st = 0; i_st < m_num_spike_trains; ++i_st) {
                auto& one_spike_train = m_spike_train[i_st];
                // calc prob for each stimulus using prior and trans
                std::vector<double> probs(m_num_stimuli, 0);
                for (int i = 0; i < m_num_stimuli; ++i) {
                    /*
                    auto row = trans[i];
                    for (int j = 1; j < m_num_stimuli - 1; ++j) {
                        row[j] = row[j] - trans[i][j-1];
                    }
                    row.push_back(1-trans[i].back());
                    */
                    for (int j = 0; j < m_num_stimuli; ++j) {
                        probs[j] += p.prior[i_st][i] * trans[i][j];
                    }
                }
                for (int j = 0; j < m_num_stimuli; ++j) {
                    std::vector<double> stim = cont_two_vectors<double>(p.t_h, p.x_h[j]);
                    double xx = p.x[j];
                    //double betadt = p.beta[j]*m_unit_length;
                    //stim.push_back((1-m_unit_length)*xx+betadt);
                    stim.push_back( (xx-p.beta[j]) * exp(-m_unit_length) + p.beta[j] );
                    double llk = mp_model->calc_spike_train_loglik(stim,
                            one_spike_train, t - 0.5*m_unit_length, 
                            t + 0.5*m_unit_length);
                    logs[j] = log(probs[j]) + llk;
                }
                logposterior += log_sum_exp(logs);
            }
            double tmp = m_geom_mean ? exp(logposterior/m_num_spike_trains) : exp(logposterior);
            p.w *= tmp;
            // prevent Overflow
            if (p.w < 1e-305) p.w = 1e-305;
            if (p.w > 1e305) p.w = 1e305;
            if (tmp < 1e-305) tmp = 1e-305;
            if (tmp > 1e305) tmp = 1e305;
            p.apf_g = tmp;
        }
        normalize_particle_weights(particles);
        resample_particles(particles);
        for (auto& p : particles) {
            if (!m_ou_nuisanse_known) {
                // update par
                if (!m_ks) {
                    double tmp = p.sigma;
                    p.sigma = tmp + m_d_norm(gen);
                    while (p.sigma <= 0) 
                        p.sigma = tmp + m_d_norm(gen);
                    for (int j = 0; j < m_num_stimuli; ++j) {
                        p.beta[j] += m_d_norm(gen) * 2 * sqrt(m_unit_length);
                    }
                    propagate_states_trans_p(p.trans, gslr);
                } else {
                    // update par using ks
                    double psigma = p.sigma;
                    p.sigma = m_d_norm(gen) * m_apf_h * sigma_sd + p.sigma * m_apf_a + (1-m_apf_a) * sigma_mean;
                    while (p.sigma <= 0) {
                        p.sigma = m_d_norm(gen) * m_apf_h * sigma_sd + psigma * m_apf_a + (1-m_apf_a) * sigma_mean;
                    }
                    for (int j = 0; j < m_num_stimuli; ++j) {
                        p.beta[j] += m_d_norm(gen) * 2 * sqrt(m_unit_length);
                    }
                    propagate_states_trans_p(p.trans, gslr);
                }
            }
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x[j] = sample_next_ou(p.x[j], p.beta[j], p.sigma, m_unit_length);
            }
            for (int j = 0; j < m_num_stimuli; ++j) {
                if (m_smooth_lag > 0)
                    p.all_x[j].push_back(p.x[j]);
            }
            double logposterior = 0;;
            for (int i_st = 0; i_st < m_num_spike_trains; ++i_st) {
                auto& one_spike_train = m_spike_train[i_st];
                // calc prob for each stimulus using prior and trans
                std::vector<double> probs2(m_num_stimuli, 0);
                for (int i = 0; i < m_num_stimuli; ++i) {
                    /*
                    auto row = p.trans[i];
                    for (int j = 1; j < m_num_stimuli - 1; ++j) {
                        row[j] = row[j] - p.trans[i][j-1];
                    }
                    row.push_back(1-p.trans[i].back());
                    */
                    for (int j = 0; j < m_num_stimuli; ++j) {
                        probs2[j] += p.prior[i_st][i] * p.trans[i][j];
                    }
                }
                for (int j = 0; j < m_num_stimuli; ++j) {
                    std::vector<double> stim = cont_two_vectors<double>(p.t_h, p.x_h[j]);
                    stim.push_back(p.x[j]);
                    double llk = mp_model->calc_spike_train_loglik(stim,
                            one_spike_train, t - 0.5*m_unit_length, 
                            t + 0.5*m_unit_length);
                    logs[j] = log(probs2[j]) + llk;
                }
                logposterior += log_sum_exp(logs);
                // update prior for next time
                double sump = 0;
                std::vector<double> pp = logs;
                for (auto& v : pp) {
                    v = exp(v);
                    sump += v;
                }
                for (int j = 0; j < m_num_stimuli; ++j) {
                    p.prior[i_st][j] = pp[j] / sump;
                }
            }
            p.w *= exp(logposterior);
            p.w /= p.apf_g;
            // prevent Overflow
            if (p.w < 1e-305) p.w = 1e-305;
            if (p.w > 1e305) p.w = 1e305;
            p.logw += logposterior;
            p.logw -= log(p.apf_g);

            if (!is_any_spike_train_empty(m_spike_train,
                        t - 0.5*m_unit_length,
                        t + 0.5*m_unit_length)) {
                for (auto& xh : p.x_h) {
                    xh.clear();
                }
                p.t_h.clear();
            }
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x_h[j].push_back(p.x[j]);
            }
            p.t_h.push_back(t + 0.5*m_unit_length);
        }
        gsl_rng_free(gslr);
    }

    /*
    // FIXME skip outputing beta, sigma, c when using k-lag smoothing
    // TODO confidence interval
    // output NOT USED
    void output_particles(const std::vector<Particle>& particles, int i, bool last_step) {
        // if use k-lag smooth, but i is less than k, skip 
        if (m_smooth_lag > 0 && i < m_smooth_lag) return;
        double x = 0, beta = 0, sigma = 0;
        std::vector<double> c(m_num_stimuli, 0);
        for(auto& p : particles) {
            c[p.c] += p.w;
            x += (m_smooth_lag > 0 ? p.all_x[i-m_smooth_lag] : p.x[p.c]) * p.w;
            beta += p.beta[p.c] * p.w;
            sigma += p.sigma * p.w;
        }
        int max_c = get_max_index(c);
        m_out_c.push_back(max_c);
        m_out_x.push_back(x);
        m_out_sigma.push_back(sigma);
        m_out_beta.push_back(beta);

        // if in the last step, output all remaining x
        if (last_step && m_smooth_lag > 0) {
            for (int ii = i - m_smooth_lag+1; ii <= i; ++ii) {
                x = 0;
                for(auto& p : particles) {
                    x += p.all_x[ii] * p.w;
                }
                m_out_x.push_back(x);
            }
        }
    }
    */

    // before normalization
    double calc_mean(const std::vector<double>& v) {
        int l = v.size();
        double m = 0;
        for (double vv : v) m += vv;
        return m/l;
    }

    double calc_var(const std::vector<double>& v) {
        double m = calc_mean(v);
        int l = v.size();
        double var = 0;
        for (double vv : v) var += (vv - m) * (vv - m);
        return var / (l-1);
    }

    double calc_cov(const std::vector<double>& v1,
                    const std::vector<double>& v2) {
        double m1 = calc_mean(v1),
               m2 = calc_mean(v2);
        int    l  = v1.size();
        double cov = 0;
        for (int i = 0; i < l; ++i) cov += (v1[i] - m1) * (v2[i] - m2);
        return cov / (l-1);
    }

    void calc_from_trace_by_input(
            const std::vector<std::vector<double>>& tr_x,
            const std::vector<std::vector<double>>& tr_w,
            std::vector<double>& out_x,
            std::vector<double>& out_ci) {
        int size  = tr_x.size();
        for (int i = 0; i < size; ++i) {
            double wn = 0;
            for (int j = 0; j < m_num_particles; ++j) {
                wn += tr_w[i][j];
            }
            // normalized weight
            std::vector<double> nw(m_num_particles);
            for (int j = 0; j < m_num_particles; ++j) {
                nw[j] = tr_w[i][j] * 1.0 / wn;
            }
            double x = 0;
            for (int j = 0; j < m_num_particles; ++j) {
                x += tr_x[i][j] * nw[j];
            }
            std::vector<double> hw(m_num_particles), w(m_num_particles);
            for (int j = 0; j < m_num_particles; ++j) {
                hw[j] = tr_w[i][j] * tr_x[i][j];
                w[j]  = tr_w[i][j];
            }
            double varIS  = calc_var(hw);
            double covEta = calc_cov(hw, w);
            double varW   = calc_var(w);
            double c      = calc_mean(w);
            double sem    = sqrt((varIS + x*x*varW - 2*x*covEta) / m_num_particles / c / c);
            out_x.push_back(x);
            out_ci.push_back(1.96*sem);
        }
    }

    void calc_from_trace_by_input_marginal(
            const std::vector<std::vector<double>>& tr_x,
            const std::vector<std::vector<double>>& tr_w,
            std::vector<double>& out_x,
            std::vector<double>& out_ci) {
        int size  = tr_x.size();
        for (int i = 0; i < size; ++i) {
            double wn = 0;
            for (int j = 0; j < m_num_particles; ++j) {
                wn += tr_w[i][j];
            }
            // normalized weight
            std::vector<double> nw(m_num_particles);
            for (int j = 0; j < m_num_particles; ++j) {
                nw[j] = tr_w[i][j] / wn;
            }
            for (int k = 0; k < m_num_stimuli; ++k) {
                double x = 0;
                for (int j = 0; j < m_num_particles; ++j) {
                    x += tr_x[i][j*m_num_stimuli+k] * nw[j];
                }
                std::vector<double> hw(m_num_particles), w(m_num_particles);
                for (int j = 0; j < m_num_particles; ++j) {
                    int jj = j*m_num_stimuli+k;
                    hw[j] = tr_w[i][j] * tr_x[i][jj];
                    w[j]  = tr_w[i][j];
                }
                double varIS  = calc_var(hw);
                double covEta = calc_cov(hw, w);
                double varW   = calc_var(w);
                double c      = calc_mean(w);
                double sem    = sqrt((varIS + x*x*varW - 2*x*covEta) / m_num_particles / c / c);
                out_x.push_back(x);
                out_ci.push_back(1.96*sem);
            }
        }
    }

    void calc_from_trace_by_c() {
        int size = m_x.size();
        for (int i = 0; i < size; ++i) {
            double cn = 0;
            for (int j = 0; j < m_num_particles; ++j) {
                cn += m_c[i][j] * m_w[i][j];
            }
            int cr = 0;
            if (cn > 0.5) cr = 1;
            else cr = 0;
            double xn = 0;
            for (int j = 0; j < m_num_particles; ++j) {
                if (m_c[i][j] == cr)
                    xn += m_x[i][j] * m_w[i][j];
            }
            double wsum = 0;
            for (int j = 0; j < m_num_particles; ++j) {
                if (m_c[i][j] == cr)
                    wsum += m_w[i][j];
            }
            m_out_x_lag.push_back(xn/wsum);
            m_out_ci_lag.push_back(xn/wsum);
        }
    }


#if 0
    std::vector<Particle> init_particles_parallel() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(50, 200);
        std::uniform_real_distribution<> dis_sigma(1, 20);
        std::vector<Particle> particles;
        for (int i = 0; i < m_num_particles; ++i) {
            Particle p;
            p.cs = std::vector<int>(m_num_spike_trains, 0);
            p.x_hs = std::vector<std::vector<double>>(m_num_spike_trains,
                    std::vector<double>());
            p.t_hs = p.x_hs;
            std::vector<double> mp(m_num_stimuli-1, 1.0/m_num_stimuli);
            for (int j = 0; j < m_num_spike_trains; ++j)
                p.cs[j] = sample_multinomial(mp);
            for (int j = 0; j < m_num_stimuli; ++j) {
                p.x.push_back(dis(gen));
                p.t.push_back(m_start_time + 0.5*m_unit_length);
            }
            if (m_ou_nuisanse_known) {
                p.beta = m_stimulus_ou_beta;
                p.sigma = m_stimulus_ou_sigma;
            } else {
                p.beta = p.x;
                p.sigma = dis_sigma(gen);
            }
            p.ws = std::vector<double>(m_num_spike_trains, 0);
            for (int j = 0; j < m_num_spike_trains; ++j) {
                p.ws[j] += mp_model->calc_spike_train_loglik(
                        {p.x[p.cs[j]]}, m_spike_train[j], m_start_time, 
                        m_start_time+m_unit_length);
            }
            double logposterior = 0;
            for (auto& w : p.ws) {
                logposterior += w;
                w = exp(w);
            }
            p.w = exp(logposterior);
            for (int j = 0; j < m_num_spike_trains; ++j) {
                p.x_hs[j].push_back(p.x[p.cs[j]]);
                p.t_hs[j].push_back(m_start_time+m_unit_length);
            }
            particles.push_back(p);
        }
        normalize_particle_weights(particles);
        return particles;
    }

    void update_particles_parallel(std::vector<Particle>& particles, double t) {
        resample_particles(particles);
        for (auto& p : particles) {
            for (int j = 0; j < m_num_spike_trains; ++j)
                p.cs[j] = sample_multinomial(m_states_tran_p[p.cs[j]]);
            auto c_set = get_set_from_vector<int>(p.cs);
            if (!m_ou_nuisanse_known) {
                std::random_device rd;
                std::mt19937 gen(rd());
                std::normal_distribution<> d_sigma(0, 1);
                std::normal_distribution<> d_beta(0, 2);
                p.sigma = fabs(p.sigma + d_sigma(gen));
                for (auto j : c_set)
                    p.beta[j] += d_beta(gen);
            }
            for (auto j : c_set) {
                p.x[j] = sample_next_ou(p.x[j], p.beta[j],
                        p.sigma, t - p.t[j]);
                p.t[j] = t;
            }
            p.ws = std::vector<double>(m_num_spike_trains, 0);
            for (int j = 0; j < m_num_spike_trains; ++j) {
                std::vector<double> stim = cont_two_vectors<double>(
                        p.t_hs[j], p.x_hs[j]);
                stim.push_back(p.x[p.cs[j]]);
                p.ws[j] += mp_model->calc_spike_train_loglik(stim,
                        m_spike_train[j], t - 0.5*m_unit_length, 
                        t + 0.5*m_unit_length);
            }
            double logposterior = 0;
            for (auto& w : p.ws) {
                logposterior += w;
                w = exp(w);
            }
            p.w = exp(logposterior);
            for (int j = 0; j < m_num_spike_trains; ++j) {
                if (!is_spike_train_empty(m_spike_train[j],
                            t - 0.5*m_unit_length,
                            t + 0.5*m_unit_length)) {
                    p.x_hs[j].clear();
                    p.t_hs[j].clear();
                }
                p.x_hs[j].push_back(p.x[p.cs[j]]);
                p.t_hs[j].push_back(t + 0.5*m_unit_length);
            }
        }
        normalize_particle_weights(particles);
    }

    void output_particles_parallel(const std::vector<Particle>& particles) {
        std::vector<double> x(m_num_spike_trains, 0);
        std::vector<double> norm(m_num_spike_trains, 0);
        //std::vector<double> stimulus(m_num_spike_trains, 0);
        for(auto& p : particles) {
            /*
            auto c_set = get_set_from_vector<int>(p.cs);
            for (auto j : c_set) {
                x[j] += p.x[j] * p.ws[j];
                norm[j] += p.ws[j];
            }
            */
            for (int i = 0; i < m_num_spike_trains; ++i) {
                x[i] += p.x[p.cs[i]] * p.ws[p.cs[i]];
                norm[i] += p.ws[p.cs[i]];
            }
        }
        for (int i = 0; i < m_num_spike_trains; ++i) {
            x[i] /= norm[i];
        }
        m_out_xs.push_back(x);
    }
#endif

    template< class T>
    void normalize_particle_weights(std::vector<T>& particles) {
        double sum = 0;
        for (auto& i : particles) {
            sum += i.w;
            i.w_unnorm = i.w;
        }
        for (auto& i : particles) {
            i.w /= sum;
        }
        // overflow
        if (std::isnan(sum) || !std::isfinite(sum)) {
            std::vector<double> logs;
            for (auto& i : particles) {
                logs.push_back(i.logw);
            }
            double logsum = mass::log_sum_exp(logs);
            for (auto& i : particles) {
                i.w = exp(i.logw - logsum);
            }
        }
        for (auto& i : particles) {
            i.logw = log(i.w);
        }
    }

    // systematic resample
    template <class PT>
    void resample_particles( std::vector<PT>& particles) {
        std::vector<PT> new_particles;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::vector<double> grid;
        std::vector<double> cumsum;
        for (int i = 0; i < m_num_particles; ++i) {
            grid.push_back( (i + m_d_unif(gen)) * 1.0 / m_num_particles );
        }
        for (auto& p : particles) {
            double prev = cumsum.size() == 0 ? 0 : cumsum.back();
            cumsum.push_back(prev + p.w);
        }
        double w = 0;
        for (auto i : grid) {
            while (cumsum[w] < i) ++w; 
            new_particles.push_back(particles[w]);
        }
        double pw    = 1.0 / m_num_particles;
        double plogw = log(pw);
        for (auto& p : new_particles) {
            p.w    = pw;
            p.logw = plogw;
        }
        particles = new_particles;
    }

    std::vector<double> simulate_spikes(const std::string& fsimx,
            const std::string& fsims) {
        std::vector<std::vector<double>> stimuli = {m_attended_stimulus};
        std::vector<double> x, st;
        mp_sim_model->set_stimuli(&stimuli);
        mp_sim_model->simulate(0, 0.0001, m_spike_train_length, 
                2000, x, st, fsimx, fsims, false);
        m_spike_train.push_back(st);
        return st;
    }

    std::vector<std::vector<double>> simulate_spikes(
            const std::vector<std::string>& fsimx,
            const std::vector<std::string>& fsims) {
        std::vector<std::vector<double>> ss;
        for (size_t i = 0; i < fsims.size(); ++i) {
            ss.push_back(simulate_spikes(fsimx[i], fsims[i]));
        }
        return ss;
    }

    void print_stimuli(const std::vector<std::string>& fstimuli,
            const std::string& fattended_stimulus,
            const std::string& fattended_index) {
        std::ofstream ofs(fattended_stimulus);
        std::ofstream ofi(fattended_index);
        for (auto i : m_attended_stimulus) {
            ofs << i << std::endl;
        }
        for (auto i : m_attended_index) {
            ofi << i << std::endl;
        }
        for (int i = 0; i < m_num_stimuli; ++i) {
            std::ofstream of(fstimuli[i]);
            for (auto j : m_stimuli[i]) {
                of << j << std::endl;
            }
        }
    }

    // Ornstern-Uhlenbeck
    void simulate_stimuli( double length) {
        int N = length / m_stimuli_grid;
        int skip = 1 / m_stimuli_grid;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<> d(0, m_stimulus_ou_sigma*sqrt(m_stimuli_grid));
        m_stimuli.clear();
        for (int n = 0; n < m_num_stimuli; ++n) {
            double beta = m_stimulus_ou_beta[n];
            m_stimuli.push_back(std::vector<double>(1, beta));
            for (int i = 0; i < N+skip; ++i) {
                double prev = m_stimuli[n].back();
                double next = prev + (beta-prev)*m_stimuli_grid + d(gen);
                if (i >= skip) m_stimuli[n].push_back(next);
                /*
                // check uniformity of residuals using gaussian CDF
                double gmu = prev*exp(-m_stimuli_grid) + mu - mu * exp(-m_stimuli_grid);
                double gsd = sqrt(sigma*sigma/2 * (1-exp(-2*m_stimuli_grid)));
                boost::math::normal_distribution<double> nd(gmu, gsd);
                double resi = boost::math::cdf(nd, next);
                std::cout << resi << std::endl;
                */
            }
        }
    }

    std::vector<double> calc_attended_stimulus(double start, double end) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(0, 1);
        int starti = start / m_stimuli_grid;
        int endi = end / m_stimuli_grid;
        int unit = m_unit_length / m_stimuli_grid;
        int N = (endi - starti) / unit;
        std::vector<double> stimulus;
        // pick first stimulus uniformly
        int i = floor(dis(gen)*m_num_stimuli);
        m_attended_index.push_back(i);
        copy_vector(stimulus, m_stimuli[i], starti, unit);
        // loop
        for (int j = 1; j < N; ++j) {
            i = sample_multinomial(m_states_tran_p[i]);
            copy_vector(stimulus, m_stimuli[i], starti+j*unit, unit);
            m_attended_index.push_back(i);
        }
        m_attended_stimulus = stimulus;
        return stimulus;
    }

    // voltage propagation
    double sample_next_ou(double prev, double beta, double sigma, double dt) {
        double gmu = (prev - beta) * exp(-dt) + beta;
        double gsd = sqrt(sigma*sigma/2 * (1-exp(-2*dt)));
        std::random_device rd;
        std::mt19937 gen(rd());
        double next = m_d_norm(gen) * gsd + gmu;
        return next;
    }

    /*
    // state trans initialize
    //  trans is sorted uniform r.v.s. which decides the random transition:
    //    e.g., 0---v1--v2-------v3---1
    std::vector<std::vector<double>> init_states_trans_p(std::mt19937& gen) {
        if (m_num_stimuli <= 1) {
            return {{1}};
        } else {
            std::vector<std::vector<double>> trans;
            for (int j = 0; j < m_num_stimuli; ++j) {
                // generate uniform random numbers between 0 and 1 
                std::vector<double> unifs(m_num_stimuli-1);
                for (int i = 0; i < m_num_stimuli-1; ++i)
                    unifs[i] = m_d_unif(gen);
                std::sort(unifs.begin(), unifs.end());
                trans.push_back(unifs);
            }
            return trans;
        }
    }
    */

    std::vector<std::vector<double>> init_states_trans_p(gsl_rng* r) {
        std::vector<double> init(m_num_stimuli, 10.0/m_num_stimuli);
        std::vector<double> out(m_num_stimuli);
        std::vector<std::vector<double>> trans;
        for (int j = 0; j < m_num_stimuli; ++j) {
            gsl_ran_dirichlet(r, static_cast<size_t>(m_num_stimuli), init.data(), out.data());
            trans.push_back(out);
        }
        return trans;
    }

    // state trans propagation
    // results in increasing points: 0---p1---p2--p3-----1
    /*
    void propagate_states_trans_p(
            std::vector<std::vector<double>>& trans, std::mt19937& gen) {
        if (m_num_stimuli <= 1) return;
        for (auto& row : trans) {
            for (auto& v : row) {
                double w = v + m_d_norm(gen) * 0.1 / m_num_stimuli;
                while (w <= 0 || w >= 1)
                    w = v + m_d_norm(gen) * 0.1 / m_num_stimuli;
                v = w;
            }
            std::sort(row.begin(), row.end());
        }
    }
    */

    // state trans propagation with dirichlet distribution
    // results in probabilities: sum(p1,p2,...,pn)=1
    void propagate_states_trans_p(
            std::vector<std::vector<double>>& trans, gsl_rng* r) {
        if (m_num_stimuli <= 1) return;
        std::vector<double> result = trans[0], tmp(trans[0].size());
        for (auto& row : trans) {
            std::transform(
                row.begin(), row.end(), tmp.begin(),
                [this] (double v) { 
                    // prevent being stuck during dirichlet sampling
                    if (v < 0.1) v = 0.1;
                    return v * m_dirichlet_const; 
            });
            gsl_ran_dirichlet (r, tmp.size(), tmp.data(), result.data() );
            row = result;
        }
    }

    // sample from trans
    /*
    int sample_from_trans(const std::vector<double>& row, std::mt19937& gen) {
        if (m_num_stimuli <= 1) return 0;
        double v = m_d_unif(gen);
        int sz = row.size();
        for (int i = 0; i < sz; ++i) {
            if (v < row[i]) return i;
        }
        return sz;
    }
    */

    //sample from trans
    int sample_from_trans(const std::vector<double>& row, std::mt19937& gen) {
        std::discrete_distribution<> d(row.begin(), row.end());
        return d(gen);
    }

    /*
    // stationary distribution initialize
    //  trans is sorted uniform r.v.s. which decides the random transition:
    //    e.g., 0---v1--v2-------v3---1
    std::vector<double> init_stationary_p(std::mt19937& gen) {
        if (m_num_stimuli <= 1) {
            return {1};
        } else {
            // generate uniform random numbers between 0 and 1 
            std::vector<double> unifs(m_num_stimuli-1);
            for (int i = 0; i < m_num_stimuli-1; ++i)
                unifs[i] = m_d_unif(gen);
            std::sort(unifs.begin(), unifs.end());
            return unifs;
        }
    }
    */


    // stationary p propagation
    void propagate_stationary_p(
            std::vector<double>& stationary_p, std::mt19937& gen) {
        if (m_num_stimuli <= 1) return;
        for (auto& v : stationary_p) {
            double w = v + m_d_norm(gen) * 0.1 / m_num_stimuli;
            while (w <= 0 || w >= 1)
                w = v + m_d_norm(gen) * 0.1 / m_num_stimuli;
            v = w;
        }
        std::sort(stationary_p.begin(), stationary_p.end());
    }

    // sample from stationary_p
    int sample_from_stationary(const std::vector<double>& row, std::mt19937& gen) {
        if (m_num_stimuli <= 1) return 0;
        double v = m_d_unif(gen);
        int sz = row.size();
        for (int i = 0; i < sz; ++i) {
            if (v < row[i]) return i;
        }
        return sz;
    }



    // calculate propagation prior
    //double calc_prop_prob(double prev, double now, 

    void copy_vector(std::vector<double>& to, const std::vector<double>& from,
            int start, int length) {
        for (int i = start; i < start+length; ++i) {
            to.push_back(from[i]);
        }
    }

    void calc_effective_sample_size() {
        std::vector<double> effn;
        for (size_t i = 0; i < m_w.size(); ++i) {
            double wsum = 0;
            double efn = 0;
            for (int j = 0; j < m_num_particles; ++j) {
                wsum += m_w[i][j];
            }
            for (int j = 0; j < m_num_particles; ++j) {
                double tmp = m_w[i][j] / wsum;
                efn += tmp*tmp;
            }
            effn.push_back(1.0/efn);
        }
        m_effn = effn;
    }

    template <class T>
    std::vector<T> read_time_series(const std::string& file) {
        std::vector<T> ss;
        std::ifstream ifs(file);
        T val;
        while(ifs >> val) {
            ss.push_back(val);
        }
        return ss;
    }

    template <class T>
    std::vector<std::vector<T>> read_time_series(
            const std::vector<std::string>& file) {
        std::vector<std::vector<T>> ss;
        for (auto& i : file) {
            ss.push_back(read_time_series<T>(i));
        }
        return ss;
    }

    bool is_any_spike_train_empty(const std::vector<std::vector<double>>& ss,
            double start, double end) {
        bool r = false;
        for (auto& st : ss) {
            if (is_spike_train_empty(st, start, end)) {
                r = true;
                break;
            }
        }
        return r;
    }

    bool is_spike_train_empty(const std::vector<double>& st, double start,
            double end) {
        bool r = true;
        for (auto i : st) {
            if (i >= start && i <= end) {
                r = false;
                break;
            }
            if (i > end) break;
        }
        return r;
    }

    // dump both fixed-lag-smoothing and filtering
    void dump_particles(std::vector<Particle>& ps, int i, bool last_step) {
        // if use FB store all particles
        m_particles.push_back(ps);
        // filtering
        m_x.push_back(std::vector<double>());
        m_w.push_back(std::vector<double>());
        m_c.push_back(std::vector<int>());
        m_sigma.push_back(std::vector<double>());
        m_beta.push_back(std::vector<double>());
        for (auto& p : ps) {
            m_x.back().push_back(p.x[p.c]);
            m_w.back().push_back(p.w);
            m_c.back().push_back(p.c);
            m_sigma.back().push_back(p.sigma);
            m_beta.back().push_back(p.beta[p.c]);
        }
        // fixed-lag smoothing
        if (m_smooth_lag == 0 || i < m_smooth_lag) return;
        m_x_lag.push_back(std::vector<double>());
        m_w_lag.push_back(std::vector<double>());
        for (auto& p : ps) {
            m_x_lag.back().push_back(m_smooth_lag > 0 ? p.all_x[i-m_smooth_lag] : p.x[p.c]);
            m_w_lag.back().push_back(p.w);
        }
        // if in the last step, output all remaining x
        if (last_step && m_smooth_lag > 0) {
            for (int ii = i - m_smooth_lag+1; ii <= i; ++ii) {
                m_x_lag.push_back(std::vector<double>());
                m_w_lag.push_back(std::vector<double>());
                for(auto& p : ps) {
                    m_x_lag.back().push_back(p.all_x[ii]);
                    m_w_lag.back().push_back(p.w);
                }
            }
        }
    }

    void dump_particles(std::vector<ParticleM>& ps, int i, bool last_step) {
        // if use FB store all particles
        m_particlesm.push_back(ps);
        // filtering
        m_x.push_back(std::vector<double>());
        m_w.push_back(std::vector<double>());
        m_c.push_back(std::vector<int>());
        m_sigma.push_back(std::vector<double>());
        m_beta.push_back(std::vector<double>());
        for (auto& p : ps) {
            auto xx = p.x;
            sort(xx.begin(), xx.end());
            for (int j = 0; j < m_num_stimuli; ++j) {
                m_x.back().push_back(xx[j]);
                m_beta.back().push_back(p.beta[j]);
            }
            m_w.back().push_back(p.w);
            m_sigma.back().push_back(p.sigma);
        }
        // fixed-lag smoothing
        if (m_smooth_lag == 0 || i < m_smooth_lag) return;
        m_x_lag.push_back(std::vector<double>());
        m_w_lag.push_back(std::vector<double>());
        for (auto& p : ps) {
            for (int j = 0; j < m_num_stimuli; ++j) {
                m_x_lag.back().push_back(p.all_x[j][i-m_smooth_lag]);
            }
            sort(m_x_lag.back().end()-m_num_stimuli, m_x_lag.back().end());
            m_w_lag.back().push_back(p.w);
        }
        // if in the last step, output all remaining x
        if (last_step && m_smooth_lag > 0) {
            for (int ii = i - m_smooth_lag+1; ii <= i; ++ii) {
                m_x_lag.push_back(std::vector<double>());
                m_w_lag.push_back(std::vector<double>());
                for(auto& p : ps) {
                    for (int j = 0; j < m_num_stimuli; ++j) {
                        m_x_lag.back().push_back(p.all_x[j][ii]);
                    }
                    sort(m_x_lag.back().end()-m_num_stimuli, m_x_lag.back().end());
                    m_w_lag.back().push_back(p.w);
                }
            }
        }
    }

    ResponseLIF* mp_sim_model;
    ResponseLIF* mp_model;

    // trace
    std::vector<std::vector<double>> m_x_lag;
    std::vector<std::vector<double>> m_x;
    std::vector<std::vector<double>> m_w_lag;
    std::vector<std::vector<double>> m_w;
    std::vector<std::vector<double>> m_w_FB;
    std::vector<std::vector<int>> m_c;
    std::vector<std::vector<double>> m_beta;
    std::vector<std::vector<double>> m_sigma;
    // output
    std::vector<int> m_out_c;
    std::vector<double> m_out_x;
    std::vector<double> m_out_ci;
    std::vector<double> m_out_x_lag;
    std::vector<double> m_out_ci_lag;
    std::vector<double> m_out_x_FB;
    std::vector<double> m_out_ci_FB;
    std::vector<double> m_out_sigma;
    std::vector<double> m_out_beta;

    std::vector<double> m_effn;

    std::vector<std::vector<Particle>> m_particles;
    std::vector<std::vector<ParticleM>> m_particlesm;

    std::vector<std::vector<double>> m_spike_train;
    std::vector<std::vector<double>> m_stimuli;
    std::vector<double> m_attended_stimulus;
    std::vector<int> m_attended_index;
    double m_stimuli_grid = 0.01;
    int m_num_stimuli = 2;
    int m_num_particles = 200;
    int m_num_spike_trains = 1;
    std::vector<std::vector<double>> m_states_tran_p;
    std::vector<double> m_stationary_p;
    std::vector<double> m_stimulus_ou_beta;
    double m_stimulus_ou_sigma = 30;
    double m_unit_length = 0.1;
    double m_start_time = 1;
    double m_end_time = 6;
    double m_spike_train_length = 6;
    bool m_ou_nuisanse_known = false;
    bool m_geom_mean = false;
    std::vector<std::vector<int>> m_out_cs;
    std::vector<std::vector<double>> m_out_xs;
    std::vector<std::vector<double>> m_out_sigmas;
    std::vector<std::vector<double>> m_out_betas;
    bool m_parallel = false;
    int m_smooth_lag = 0;
    bool m_apf = false;         // use auxiliary particle filter
    bool m_ks = false;          // use kernel smoothing to update par
    double m_apf_a = 0.995;
    double m_apf_h = 0.1;
    bool m_apf_expect = false;
    int m_apf_expect_num = 10;

    double m_dirichlet_const = 50.0;

    std::normal_distribution<> m_d_norm;
    std::uniform_real_distribution<> m_d_unif;

}; // StateSpaceDecode

} // namespace mass

#endif // MASS_STATESPACEDECODE_H
