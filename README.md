*__Kang Li__ and Susanne Ditlevsen. (2019). Neural decoding with visual attention using sequential Monte Carlo for leaky integrate-and-fire neurons*

### Requirements
- [openCV](https://www.opencv.org/)
- [Boost](https://www.boost.org/)
- [nlopt](https://nlopt.readthedocs.io/en/latest/)
- [GSL](https://www.gnu.org/software/gsl/)

### How to use
```
mkdir build
cd build/
cmake ..
make decode
cp ../output/* ./ -a
./decode
```
The result files of experiments will be in folders `decode1`, `decode2`, `decode3`, `decodeserial2` and `decodeparallel2`.