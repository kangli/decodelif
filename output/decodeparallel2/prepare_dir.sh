#!/bin/bash
for i in `seq 0 99`;
do
    mkdir rep$i
    cd rep$i
    mkdir pf
    mkdir apfks
    mkdir mpf
    mkdir mapfks
    for j in `seq 0 9`;
    do
        mkdir p$j
        mkdir p$j/pf
        mkdir p$j/apfks
    done
    cd ..
done  
