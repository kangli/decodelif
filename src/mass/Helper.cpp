#include <mass/Helper.h>

namespace mass {

RescaledSpikeTrain combine_rsts( const std::vector<RescaledSpikeTrain>& rsts) {
    RescaledSpikeTrain rst = rsts.front();
    for (size_t i = 1; i < rsts.size(); ++i) {
        double diff = rst.end - rsts[i].start;
        rst.end = rsts[i].end + diff;
        for (size_t j = 0; j < rsts[i].st.size(); ++j) {
            rst.st.push_back( rsts[i].st[j] + diff );
        }
    }
    return rst;
}

// get distance
double calc_dist(const std::vector<double>& v1, const std::vector<double>& v2) {
    double s = 0;
    for (size_t i = 0; i < v1.size(); ++i) {
        double d = v1[i] - v2[i];
        s += d*d;
    }
    return sqrt(s);
}

double calc_dist(double v1, double v2) {
    return sqrt( (v1-v2)*(v1-v2) );
}

// get mean
std::vector<double> calc_mean(const std::vector<std::vector<double>>& v) {
    std::vector<double> m;
    int size = v.size();
    int s2 = v[0].size();
    for (int i = 0; i < s2; ++i) {
        double s = 0;
        for (auto& vv : v) {
            s += vv[i];
        }
        m.push_back(s/size);
    }
    return m;
}

/**
 * \brief Multi-nomial distribution 
 */
int sample_multinomial(const std::vector<double>& p) {
    if (p.size() == 0) return 0;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    double r = dis(gen);
    std::vector<double> cumsum(1, p[0]);
    for (size_t i = 1; i < p.size(); ++i) {
        cumsum.push_back(cumsum.back() + p[i]);
    }
    cumsum.push_back(1);
    int result = 0;
    for (size_t i = 0; i < p.size()+1; ++i) {
        if (r <= cumsum[i]) {
            result = i;
            break;
        }
    }
    return result;
}

} // namespace mass
