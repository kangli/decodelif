#include <mass/StateSpaceDecode.h>

#include <fstream>

using namespace mass;

std::vector<double> lifparam = { 100,  0.4, 1,   50,  -25,   -40,   -15};
//std::vector<double> lifparam = { 100,  0.4, 1,   -50,  -15,   20,   -8};
//std::vector<double> lifparam = { 100,  0.4, 1,   -2,  -0.5,   0,   0};

void run(int num_stimuli, int num_spike_train,
        const std::vector<std::vector<double>>& trans,
        const std::vector<double>& stationary,
        const std::vector<double>& stimulus_ou_beta,
        double stimulus_ou_sigma, const std::string& folder, bool nuisance,
        int loop, StateSpaceDecode::SimOption simoption,
        int num_particles, bool apf = false, std::string fpf = "pf", bool marginal = false,
        bool parallel_same_stimuli = false, bool geom_mean_apf = false) {
    std::vector<double> rmsd, accuracy, rmsd_lag, rmsd_FB;
    std::string folderr = folder + "/" + fpf;
    for (int i = 0; i < loop; ++i) {
        std::cout << i << std::endl;
        StateSpaceDecode ssd;
        ssd.set_smoothing_lag(10);
        ssd.config(num_stimuli, num_spike_train, trans, stationary, stimulus_ou_beta, stimulus_ou_sigma, nuisance, num_particles, apf, apf, 0.95, geom_mean_apf);
        std::vector<std::string> fstimuli, fvoltage, fspike_train;
        for (int j = 0; j < num_stimuli; ++j) {
            fstimuli.push_back(folder + (parallel_same_stimuli ? "/../p0/" : "/") + "s_" 
                    + std::to_string(i) + "_" + std::to_string(j) + ".out");
        }
        for (int j = 0; j < num_spike_train; ++j) {
            fvoltage.push_back(folder + "/" + "simx_" + std::to_string(i) + "_"
                    + std::to_string(j) + ".out");
        }
        for (int j = 0; j < num_spike_train; ++j) {
            fspike_train.push_back(folder + "/" + "sims_" + std::to_string(i) + "_"
                    + std::to_string(j) + ".out");
        }
        ssd.set_up(lifparam,
                fstimuli, 
                folder + "/" + "s_"+std::to_string(i)+".out",
                folder + "/" + "i_"+std::to_string(i)+".out",
                fvoltage,
                fspike_train,
                simoption);
        if (marginal)
            ssd.run_particle_filter_marginal();
        else
            ssd.run_particle_filter();
        //accuracy.push_back(ssd.calc_state_pred_accuracy());
        ssd.print_result(
                folderr + "/c_" + std::to_string(i) + ".out",
                folderr + "/x_" + std::to_string(i) + ".out",
                folderr + "/ci_" + std::to_string(i) + ".out",
                folderr + "/sigma_" + std::to_string(i) + ".out",
                folderr + "/beta_" + std::to_string(i) + ".out",
                folderr + "/ctrace_" + std::to_string(i) + ".out",
                folderr + "/xtrace_" + std::to_string(i) + ".out",
                folderr + "/wtrace_" + std::to_string(i) + ".out",
                folderr + "/sigmatrace_" + std::to_string(i) + ".out",
                folderr + "/transtrace_" + std::to_string(i) + ".out",
                folderr + "/x_lag_" + std::to_string(i) + ".out",
                folderr + "/ci_lag_" + std::to_string(i) + ".out",
                folderr + "/x_FB_" + std::to_string(i) + ".out",
                folderr + "/ci_FB_" + std::to_string(i) + ".out",
                folderr + "/effn_" + std::to_string(i) + ".out",
                marginal
        );
        // run with lag
        auto rmsdone = ssd.calc_RMSD();
        rmsd.push_back(rmsdone[0]);
        rmsd_lag.push_back(rmsdone[1]);
        rmsd_FB.push_back(rmsdone[2]);
    }
    std::ofstream ofrmsd(folderr + "/" + "rmsd.out");
    //std::ofstream ofaccuracy(folder + "/" + "accuracy.out");
    for (size_t i = 0; i < rmsd.size(); ++i) {
        ofrmsd << rmsd[i] << "\t" << rmsd_lag[i] << "\t" << rmsd_FB[i] << std::endl;
    }
}


void run_parallel(int num, int num_particles, const std::string& folder, int rep, bool apf = false, std::string fpf = "pf", bool marginal = false) {
    for (int j = 0; j < rep; ++j) {
        run (2, 1, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, 
                folder+ "/rep" + std::to_string(j) + "/p"+std::to_string(0), false, 1,
                fpf == "pf" ? StateSpaceDecode::SIM_STIM_SPIKE : StateSpaceDecode::SIM_NONE,
                num_particles, apf, fpf, marginal);
        for (int i = 1; i < num; ++i) {
            run (2, 1, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, 
                    folder+ "/rep" + std::to_string(j) + "/p"+std::to_string(i), false, 1,
                    fpf == "pf" ? StateSpaceDecode::SIM_ATTEND_SPIKE : StateSpaceDecode::SIM_NONE,
                    num_particles, apf, fpf, marginal, true);
        }
    }
}

void run_parallel_m(int num_stimuli, int num_spike_train,
        const std::vector<std::vector<double>>& trans,
        const std::vector<double>& stationary,
        const std::vector<double>& stimulus_ou_beta,
        double stimulus_ou_sigma, const std::string& folder, bool nuisance,
        int loop,
        int num_particles, bool apf = false, std::string fpf = "pf",
        bool geom_mean_apf = false) {
    std::vector<double> rmsd, accuracy, rmsd_lag, rmsd_FB;
    std::string folderr = folder + "/" + fpf;
    for (int i = 0; i < loop; ++i) {
        std::cout << i << std::endl;
        StateSpaceDecode ssd;
        ssd.set_smoothing_lag(10);
        ssd.config(num_stimuli, num_spike_train, trans, stationary, stimulus_ou_beta, stimulus_ou_sigma, nuisance, num_particles, apf, apf, 0.95, geom_mean_apf);
        std::vector<std::string> fstimuli, fvoltage, fspike_train;
        for (int j = 0; j < num_stimuli; ++j) {
            fstimuli.push_back(folder + "/p0/" + "s_" + std::to_string(i) + "_"
                    + std::to_string(j) + ".out");
        }
        for (int k = 0; k < num_spike_train; ++k) {
            fvoltage.push_back(folder+"/p"+std::to_string(k) + "/" + "simx_" + std::to_string(i) + "_0.out");
            fspike_train.push_back(folder+"/p"+std::to_string(k) + "/" + "sims_" + std::to_string(i) + "_0.out");
        }
        ssd.set_up(lifparam,
                fstimuli, 
                folder + "/p0/" + "s_"+std::to_string(i)+".out",
                folder + "/p0/" + "i_"+std::to_string(i)+".out",
                fvoltage,
                fspike_train,
                StateSpaceDecode::SIM_NONE);
        ssd.run_particle_filter_marginal();
        //accuracy.push_back(ssd.calc_state_pred_accuracy());
        ssd.print_result(
                folderr + "/c_" + std::to_string(i) + ".out",
                folderr + "/x_" + std::to_string(i) + ".out",
                folderr + "/ci_" + std::to_string(i) + ".out",
                folderr + "/sigma_" + std::to_string(i) + ".out",
                folderr + "/beta_" + std::to_string(i) + ".out",
                folderr + "/ctrace_" + std::to_string(i) + ".out",
                folderr + "/xtrace_" + std::to_string(i) + ".out",
                folderr + "/wtrace_" + std::to_string(i) + ".out",
                folderr + "/sigmatrace_" + std::to_string(i) + ".out",
                folderr + "/transtrace_" + std::to_string(i) + ".out",
                folderr + "/x_lag_" + std::to_string(i) + ".out",
                folderr + "/ci_lag_" + std::to_string(i) + ".out",
                folderr + "/x_FB_" + std::to_string(i) + ".out",
                folderr + "/ci_FB_" + std::to_string(i) + ".out",
                folderr + "/effn_" + std::to_string(i) + ".out",
                true
        );
        // run with lag
        auto rmsdone = ssd.calc_RMSD();
        rmsd.push_back(rmsdone[0]);
        rmsd_lag.push_back(rmsdone[1]);
        rmsd_FB.push_back(rmsdone[2]);
    }
    std::ofstream ofrmsd(folderr + "/" + "rmsd.out");
    //std::ofstream ofaccuracy(folder + "/" + "accuracy.out");
    for (size_t i = 0; i < rmsd.size(); ++i) {
        ofrmsd << rmsd[i] << "\t" << rmsd_lag[i] << "\t" << rmsd_FB[i] << std::endl;
    }
}

void process_parallel(int num, int num_particles, const std::string& folder, const std::string& fpf, int rep) {
    StateSpaceDecode ssd;
    ssd.config(2, 10, {{0.8}, {0.2}}, {}, {65, 75}, 20, false, num_particles);
    for (int j = 0; j < rep; ++j) {
        std::vector<std::string> fx, fi, fx_lag, fx_FB;
        for (int i = 0; i < num; ++i) {
            std::cout << "processing parallel" << j << i << std::endl;
            fx.push_back(folder+ "/rep" + std::to_string(j)+"/p"+std::to_string(i)+"/"+fpf+"/x_0.out");
            fx_lag.push_back(folder+ "/rep" + std::to_string(j)+"/p"+std::to_string(i)+"/"+fpf+"/x_lag_0.out");
            fx_FB.push_back(folder+ "/rep" + std::to_string(j)+"/p"+std::to_string(i)+"/"+fpf+"/x_FB_0.out");
            fi.push_back(folder+ "/rep" + std::to_string(j)+"/p"+std::to_string(i)+"/i_0.out");
        }
        ssd.integrate_results_from_parallel(fx, fi, folder+ "/rep" + std::to_string(j)+"/"+fpf+"/x.out");
        ssd.integrate_results_from_parallel_cluster(fx, folder+ "/rep" + std::to_string(j)+"/"+fpf+"/x_kmeans.out", "kmedoids");
        ssd.integrate_results_from_parallel(fx_lag, fi, folder+ "/rep" + std::to_string(j)+"/"+fpf+"/x_lag.out");
        ssd.integrate_results_from_parallel_cluster(fx_lag, folder+ "/rep" + std::to_string(j)+"/"+fpf+"/x_kmeans_lag.out", "kmedoids");
        //if (j == 1 ) continue;
        ssd.integrate_results_from_parallel(fx_FB, fi, folder+ "/rep" + std::to_string(j)+"/"+fpf+"/x_FB.out");
        ssd.integrate_results_from_parallel_cluster(fx_FB, folder+ "/rep" + std::to_string(j)+"/"+fpf+"/x_kmeans_FB.out", "kmedoids");
    }
}

void run_alldecoding() {
    int K = 500;    // number of particles
    int N = 20;     // number of spike trains in population
    int M = 50;    // number of repetitions
    //////////////// single spike train
    std::cout << "// 1 stimulus using PF" << std::endl;
    run (1, 1, {{1}}, {1}, {70}, 20, "decode1", false, M,
            StateSpaceDecode::SIM_STIM_SPIKE, K, false, "pf", false);

    std::cout << "// 1 stimulus using APFKS (liu & west)" << std::endl;
    run (1, 1, {{1}}, {1}, {70}, 20, "decode1", false, M,
            StateSpaceDecode::SIM_NONE, K, true, "apfks", false);

    std::cout << "// 2 stimuli using PF " << std::endl;
    run (2, 1, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decode2", false, M,
            StateSpaceDecode::SIM_STIM_SPIKE, K, false, "pf", false);

    std::cout << "// 2 stimuli using APFKS" << std::endl;
    run (2, 1, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decode2", false, M,
            StateSpaceDecode::SIM_NONE, K, true, "apfks", false);

    /*
    std::cout << "// 2 stimuli using mPF " << std::endl;
    run (2, 1, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decode2", false, M,
            StateSpaceDecode::SIM_NONE, K, false, "mpf", true);
    
    std::cout << "// 2 stimuli using mAPFKS" << std::endl;
    run (2, 1, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decode2", false, M,
            StateSpaceDecode::SIM_NONE, K, true, "mapfks", true);
            */
    
    std::cout << "// 3 stimuli using PF" << std::endl;
    run (3, 1, {{0.5, 0.2}, {0.3, 0.5}, {0.2, 0.3}}, {1.0/3,1.0/3,1.0/3}, {60, 70, 80}, 20, 
            "decode3", false, M,
            StateSpaceDecode::SIM_STIM_SPIKE, K, false, "pf", false);

    std::cout << "// 3 stimuli using AFPKS" << std::endl;
    run (3, 1, {{0.5, 0.2}, {0.3, 0.5}, {0.2, 0.3}}, {1.0/3,1.0/3,1.0/3}, {60, 70, 80}, 20, 
            "decode3", false, M,
            StateSpaceDecode::SIM_NONE, K, true, "apfks", false);

    std::cout << "// serial 2 stimuli using PF" << std::endl;
    run (2, N, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decodeserial2", false, M,
            StateSpaceDecode::SIM_STIM_SPIKE, K, false, "pf", false);
    
    std::cout << "// serial 2 stimuli using APFKS" << std::endl;
    run (2, N, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decodeserial2", false, M,
            StateSpaceDecode::SIM_NONE, K, true, "apfks", false);
    
    std::cout << "// serial 2 stimuli using APFKS geom" << std::endl;
    run (2, N, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decodeserial2", false, M,
            StateSpaceDecode::SIM_NONE, K, true, "apfksg", false, false, true);

    std::cout << "// parallel 2 stimuli using PF on individual spike train" << std::endl;
    run_parallel(N, K, "decodeparallel2", M, false, "pf", false);

    std::cout << "// parallel 2 stimuli using APFKS on individual spike train" << std::endl;
    run_parallel(N, K, "decodeparallel2", M, true, "apfks", false);


    std::cout << "// parallel 2 stimuli using mPF" << std::endl;
    for (int i = 0; i < M; ++i) 
        run_parallel_m(2, N, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decodeparallel2/rep"+std::to_string(i), false, 1, K, false, "mpf");

    std::cout << "// parallel 2 stimuli using mAPFKS" << std::endl;
    for (int i = 0; i < M; ++i) 
        run_parallel_m(2, N, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decodeparallel2/rep"+std::to_string(i), false, 1, K, true, "mapfks");

    std::cout << "// parallel 2 stimuli using mAPFKS geom" << std::endl;
    for (int i = 0; i < M; ++i) 
        run_parallel_m(2, N, {{0.8}, {0.2}}, {0.5, 0.5}, {65, 75}, 20, "decodeparallel2/rep"+std::to_string(i), false, 1, K, true, "mapfksg", true);
    process_parallel(N, K, "decodeparallel2", "pf", M);
    process_parallel(N, K, "decodeparallel2", "apfks", M);

}
int main() {

    run_alldecoding();
}

